<?php
	class theme{
		var $headtitle="Indian Restaurant in Bali - Indian Food in Bali - Indian Cuisine in Bali";
		var $keyword="indian restaurant in Bali, indian food in Bali, indian cuisine in Bali, indian wedding in Bali, indian group in Bali, indian catering in Bali";
		var $description="Indonesia's largest and most established chain of Indian family restaurants. Welcomes discerning diners to the true flavours of India from our Indian master Chef, Since 1986.";
		var $urlweb="http://bali.queenstandoor.com/";
		var $theme='newqueens/';
		
		function incurl($link_title){
			$this->urlweb.'themes/'.$this->theme.$link_title;
		}
		function linkurl($link_title){
			echo $this->urlweb.'themes/'.$this->theme.$link_title;
		}
		function weburl($weburl,$url){
			$linktab=$_SERVER['REQUEST_URI'];
			$pchtab=explode('/',$linktab);
			if($url==""){ $uri=''; }
			else if($url=="1"){ $uri=$pchtab[1]; }
			else if($url=="2"){ $uri=$pchtab[1].'/'.$pchtab[2]; }
			else if($url=="3"){ $uri=$pchtab[1].'/'.$pchtab[2].'/'.$pchtab[3]; }
			else if($url=="4"){ $uri=$pchtab[1].'/'.$pchtab[2].'/'.$pchtab[3].'/'.$pchtab[4]; }
			else if($url=="5"){ $uri=$pchtab[1].'/'.$pchtab[2].'/'.$pchtab[3].'/'.$pchtab[4].'/'.$pchtab[5]; }
                        else if($url=="6"){ $uri=$pchtab[1].'/'.$pchtab[2].'/'.$pchtab[3].'/'.$pchtab[4].'/'.$pchtab[5].'/'.$pchtab[6]; }
			
			echo $this->urlweb.$weburl.$uri;
		}
		function getlink($weburl,$url){
			$linktab=$_SERVER['REQUEST_URI'];
			$pchtab=explode('/',$linktab);
			if($url==""){ $uri=''; }
			else if($url=="1"){ $uri=$pchtab[1]; }
			else if($url=="2"){ $uri=$pchtab[1].'/'.$pchtab[2]; }
			else if($url=="3"){ $uri=$pchtab[1].'/'.$pchtab[2].'/'.$pchtab[3]; }
			else if($url=="4"){ $uri=$pchtab[1].'/'.$pchtab[2].'/'.$pchtab[3].'/'.$pchtab[4]; }
			else if($url=="5"){ $uri=$pchtab[1].'/'.$pchtab[2].'/'.$pchtab[3].'/'.$pchtab[4].'/'.$pchtab[5]; }
                        else if($url=="6"){ $uri=$pchtab[1].'/'.$pchtab[2].'/'.$pchtab[3].'/'.$pchtab[4].'/'.$pchtab[5].'/'.$pchtab[6]; }
			
			return $this->urlweb.$weburl.$uri;
		}
		function geturl($geturl){
			$linktab=$_SERVER['REQUEST_URI'];
			$pchtab=explode('/',$linktab);
			if($geturl==""){ $geturi=''; }
			else if($geturl=="1"){ $geturi=ucwords(strtolower(str_replace('-',' ',$pchtab[1]))); }
			else if($geturl=="2"){ $geturi=ucwords(strtolower(str_replace('-',' ',$pchtab[2]))); }
			else if($geturl=="3"){ $geturi=ucwords(strtolower(str_replace('-',' ',$pchtab[3]))); }
			else if($geturl=="4"){ $geturi=ucwords(strtolower(str_replace('-',' ',$pchtab[4]))); }
			else if($geturl=="5"){ $geturi=ucwords(strtolower(str_replace('-',' ',$pchtab[5]))); }
                        else if($geturl=="6"){ $geturi=ucwords(strtolower(str_replace('-',' ',$pchtab[6]))); }
			
			return $geturi;
		}
		function oriurl($oriurl){
			$linktab=$_SERVER['REQUEST_URI'];
			$pchtab=explode('/',$linktab);
			if($oriurl==""){ $oriuri=''; }
			else if($oriurl=="1"){ $oriuri=$pchtab[1]; }
			else if($oriurl=="2"){ $oriuri=$pchtab[2]; }
			else if($oriurl=="3"){ $oriuri=$pchtab[3]; }
			else if($oriurl=="4"){ $oriuri=$pchtab[4]; }
			else if($oriurl=="5"){ $oriuri=$pchtab[5]; }
                        else if($oriurl=="6"){ $oriuri=$pchtab[6]; }
			
			return $oriuri;
		}
		function lowerlink($text){
			echo '/'.strtolower(str_replace(' ','-',$text));
		}
		function smalink($text){
			return strtolower(str_replace(' ','-',$text));
		}
	
		function get_header(){
			require_once"./themes/".$this->theme."header.php";
		}
		function get_sidebar(){
			require_once"./themes/".$this->theme."sidebar.php";
		}
		function get_footer(){
			require_once"./themes/".$this->theme."footer.php";
		}
		function injection($data){
			$filter = stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES)));	
			return $filter;	
		}
		function get_content(){
			$linktab=$_SERVER['REQUEST_URI'];
			$pchtab=explode('/',$linktab);
			$url1=$pchtab[1];
			$url2=$pchtab[2];
			$url3=$pchtab[3];
			
			switch($_GET['act']){
				case'':
				require_once "themes/".$this->theme."homepage.php";
				break;
				
				case'content':
				require_once "themes/".$this->theme."single-content.php";
				break;
				
				case $url1:
				require_once "themes/".$this->theme."/single-menu.php";
				break;
			}
		}
	}
?>