<?php
	@session_start();
	require_once "../../../config/connect.php";
	require_once "../../../themes/function/func_theme.php";
	$theme=new theme();
?>
<script type="text/javascript" src="<?php $theme->linkurl('../../autocomplete/jquery.autocomplete.js'); ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php $theme->linkurl('../../autocomplete/jquery.autocomplete.css'); ?>" />
<script type="text/javascript">
	$().ready(function() {
		$("#postal").autocomplete("<?php $theme->linkurl('../../autocomplete/get_course_location.php'); ?>", {
			width: 268,
			matchContains: true,
			//mustMatch: true,
			//minChars: 0,
			//multiple: true,
			//highlight: false,
			//multipleSeparator: ",",
			selectFirst: false
		});
	});
</script>
<?
	
	$sessionID = $_COOKIE['PHPSESSID'];
	$query  = mysql_query("SELECT tb_carts.id_cart, tb_carts.price, tb_carts.qty, tb_carts.specreq, tb_menu.* FROM tb_carts INNER JOIN tb_menu ON tb_carts.id_menu = tb_menu.id_menu WHERE tb_carts.cart_session='".$sessionID."' GROUP BY tb_carts.id_menu, tb_carts.specreq ORDER BY tb_carts.id_cart DESC");
	//$subtotal = mysql_fetch_array(mysql_query("SELECT SUM(price) AS subtotal FROM tb_carts WHERE cart_session='".$sessionID."'"));
	$subtotal=0;
	$taxes = 0;
	$charge = 0;
	$gtotal = 0;
	
	$cekcart=mysql_num_rows($query);
	echo '<div style="max-height:200px; overflow:auto;">';
	echo '<ul>';
	if($cekcart==0){
		echo '<li>Your order is empty</li>';
	}else {
		while($row = mysql_fetch_array($query, MYSQL_ASSOC))
		{
			$subtotal+=$row['price']*$row['qty'];
			$charge = $subtotal*5/100;
			$service = $charge+($charge*10/100);
			$taxes = $subtotal*10/100;
			$gtotal = $subtotal+$service+$taxes;
			$items  = mysql_fetch_array(mysql_query("SELECT COUNT(*) AS totalItems FROM tb_carts WHERE cart_session='".$sessionID."' AND id_menu='".$row['id_menu']."'"));
			
			$title = ($row['menu']);
			$title = substr($title,0,20);
			$title = substr($title,0,strrpos($title," "));
	
			echo '<li id="listcart" style="padding-bottom:10px;">';
			echo '<a id="'.$row['id_cart'].'" href="#" class="deltocart"><span class="glyphicon glyphicon-remove"></span></a> ';
			echo $title.' (x'.$row['qty'].')<span class="pull-right">'.number_format($row['pprice'],2,",",".").'</span>';
			echo '<br /><small>Choice : '.$row['specreq'].'</small> <input name="taste'.$row['id_cart'].'" id="taste'.$row['id_cart'].'" type="hidden" value="'.$row['specreq'].'" />';
			echo '<br /><a id="'.$row['id_cart'].'" href="#" class="plus"><span class="label label-danger"><span class="glyphicon glyphicon-plus"></span></span></a> <a id="'.$row['id_cart'].'" href="#" class="min"><span class="label label-danger pointer"><span class="glyphicon glyphicon-minus"></span></span></a>';
			echo '</li>';
		}
	}
	echo '</ul>';
	echo '</div>';
	echo '<hr />';
	echo '<input name="subtot" id="subtot" type="hidden" value="'.$subtotal.'" />';
	echo '<strong>Sub Total</strong> : <span class="pull-right">'.number_format($subtotal,2,",",".").'</span><br />';
	echo '+ Service : <span class="pull-right">'.number_format($service,2,",",".").'</span><br />';
	echo '+ Taxes : <span class="pull-right">'.number_format($taxes,2,",",".").'</span><br />';
	echo '<strong>Grand Total</strong> : <span class="pull-right">'.number_format($gtotal,2,",",".").'</span><br /><br />';
	/*echo '<input name="postal" class="postal" type="number" placeholder="Postal code" /> <input name="verify" class="verify" type="button" value="verify" />';*/
	echo '<input name="postal" class="postal" id="postal" type="text" placeholder="Current Location.." /> <input name="verify" class="verify" type="button" value="verify" />';
	echo '<span class="pcode"></span>';
	echo '<br />';
	echo '<button type="button" id="checkout" class="btn btn-danger" style="margin-top:10px;" disabled="disabled">Check Out</button>';
?>
<script type="text/javascript">
	$(document).ready(function(){
		$('.verify').click(function(){
			var postal = $('.postal').val();
			$.ajax({
				type: 'POST',
				url: '<?php $theme->linkurl("function/check_postal.php"); ?>',
				data: { postal:postal},
				error: function(){
				   $("#checkout").attr("disabled", true);
			},
				success: function(data) {
					if(data==0){
						alert('We are sorry, but your postal code is not within our delivery zone. Please give us a call to find out if delivery is still available in your area. We will always make our best efforts to deliver to as many destinations as possible.');
				   		$("#checkout").attr("disabled", true);
					}else{
						$("#checkout").attr("disabled", false);
						$(".pcode").html(data);
					}
				}
			});
		});
		$('#checkout').click(function(){
			var subtot = $('#subtot').val();
				if(subtot==0){
					alert("Your order is empty");
					return false;
				}else if(subtot<150000){
					alert("your orders less than Rp. 150.000,-");
					return false;
				}else{
					window.location="<?php $theme->linkurl("../../"); echo 'checkout'; ?>";
				}
		});
		$('.deltocart').click(function(){
			$("#loading").show();
			var target = $(this).attr("id");
			$.ajax({
				type: 'POST',
				url: '<?php $theme->linkurl("function/del_temp_carts.php"); ?>',
				data: { id:target},
				error: function(){
				   alert('Error!');
			},
				success: function(data) {
				   $("#loading").hide();
				   $('#listcart').fadeTo(1000, 0.25);
				   $('#listcart').fadeTo(1000,1);
				}
			});
			$.ajax({
			url: "<?php $theme->linkurl("function/view_temp_carts.php"); ?>",
				cache: false,
				success: function(data){
					$("#viewcart").html(data);
				}
			});
		});
		$('.plus').click(function(){
			$("#loading").show();
			var target = $(this).attr("id");
			var taste = $('#taste'+target).val();
			$.ajax({
				type: 'POST',
				url: '<?php $theme->linkurl("function/plus_temp_carts.php"); ?>',
				data: { id:target,taste:taste},
				error: function(){
				   alert('Error!');
			},
				success: function(data) {
				   $("#loading").hide();
				   $('#listcart').fadeTo(1000, 0.25);
				   $('#listcart').fadeTo(1000,1);
				}
			});
			$.ajax({
			url: "<?php $theme->linkurl("function/view_temp_carts.php"); ?>",
				cache: false,
				success: function(data){
					$("#viewcart").html(data);
				}
			});
		});
		$('.min').click(function(){
			$("#loading").show();
			var target = $(this).attr("id");
			var taste = $('#taste'+target).val();
			$.ajax({
				type: 'POST',
				url: '<?php $theme->linkurl("function/min_temp_carts.php"); ?>',
				data: { id:target,taste:taste},
				error: function(){
				   alert('Error!');
			},
				success: function(data) {
				   $("#loading").hide();
				   $('#listcart').fadeTo(1000, 0.25);
				   $('#listcart').fadeTo(1000,1);
				}
			});
			$.ajax({
			url: "<?php $theme->linkurl("function/view_temp_carts.php"); ?>",
				cache: false,
				success: function(data){
					$("#viewcart").html(data);
				}
			});
		});
	});
</script>