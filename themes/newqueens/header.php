    <?php
	session_start();
	ob_start("ob_gzhandler");
	require_once "./config/connect.php";
	require_once "./auto/numb.php";
	require_once "./themes/function/func_theme.php";
	require_once "./themes/function/func_shopcart.php";
	$theme=new theme();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $theme->headtitle; ?></title>
<meta name="description" content="<?php echo $theme->description; ?>" />
<meta name="keywords" content="<?php echo $theme->keyword; ?>" />
<meta http-equiv="CHARSET" content="ISO-8859-1" />
<meta http-equiv="CONTENT-LANGUAGE" content="English" />
<meta http-equiv="VW96.OBJECT TYPE" content="Document" />
<meta name="RATING" content="General" />
<meta name="ROBOTS" content="index,follow" />
<meta name="REVISIT-AFTER" content="2 days" />
<meta name="author" content="Puneet Malhotra" />
<meta name="copyright" content="queenstandoor" />
<meta name="designer" content="jonny cible" />
<meta name="distribution" content="global" />
<meta name="google-site-verification" content="Fa8gaqIqiGjO_iGEZL1CJ29EB1AIzUbhcGq6rICqLA0" />
<meta name="google-translate-customization" content="ad36f719c2065db-153c959b37732159-g4c06bb6ebafab5f7-17"></meta>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<link href="<?php $theme->linkurl('img/logo.png'); ?>" rel="shortcut icon" />
<link href="<?php $theme->linkurl('css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php $theme->linkurl('css/docs.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php $theme->linkurl('custom.css'); ?>" rel="stylesheet" type="text/css" />

<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>-->
<!--<link href="<?php //$theme->linkurl('css/ui-lightness/jquery-ui-1.10.3.custom.css'); ?>" rel="stylesheet">-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php $theme->linkurl('js/jquery-ui-1.10.3.custom.js'); ?>"></script>

<script type="text/javascript" src="<?php $theme->linkurl('js/jquery-1.7.2.min.js');?>"></script>
<script type="text/javascript" src="<?php $theme->linkurl('js/bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?php $theme->linkurl('../plugin/newsticker/scripts/jquery.bootstrap.newsbox.min.js');?>"></script>
<script type="text/javascript" src="<?php $theme->linkurl('../plugin/newsticker/inewsticker.js'); ?>"></script>

<!-- Tablesorter: required -->
<script type="text/javascript" src="<?php $theme->linkurl('../plugin/tablesorter/js/jquery.tablesorter.js');?>"></script>
<script type="text/javascript" src="<?php $theme->linkurl('../plugin/tablesorter/js/jquery.tablesorter.widgets.js');?>"></script>
<link type="text/css" rel="stylesheet" href="<?php $theme->linkurl('../plugin/tablesorter/addons/pager/jquery.tablesorter.pager.css');?>" />
<script type="text/javascript" src="<?php $theme->linkurl('../plugin/tablesorter/addons/pager/jquery.tablesorter.pager.js');?>"></script>

<link type="text/css" rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/redmond/jquery-ui.css" />
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<link rel="stylesheet" type="text/css" href="<?php $theme->linkurl('../plugin/dropdown/ddsmoothmenu.css'); ?>" />
<script type="text/javascript" src="<?php $theme->linkurl('../plugin/dropdown/ddsmoothmenu.js'); ?>"></script>

<!--<link rel="stylesheet" href="<?php //$theme->linkurl('../plugin/news-ticker/style.css'); ?>" type="text/css" media="screen" />-->
<!--<script src="<?php //$theme->linkurl('../plugin/news-ticker/jquery-latest.pack.js'); ?>" type="text/javascript"></script>-->
<!--<script src="<?php //$theme->linkurl('../plugin/news-ticker/jcarousellite_1.0.1c4.js'); ?>" type="text/javascript"></script>-->

<link rel="stylesheet" type="text/css" href="<?php $theme->linkurl('../plugin/portfolio/css/style.css'); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php $theme->linkurl('../plugin/portfolio/css/prettyPhoto.css'); ?>"/>
<script type="text/javascript" src="<?php $theme->linkurl('../plugin/portfolio/js/jquery.prettyPhoto.js'); ?>"></script> 
<script type="text/javascript" src="<?php $theme->linkurl('../plugin/portfolio/js/jquery.quicksand.js'); ?>"></script> 
<script type="text/javascript" src="<?php $theme->linkurl('../plugin/portfolio/js/jquery.easing.1.3.js'); ?>"></script> 
<script type="text/javascript" src="<?php $theme->linkurl('../plugin/portfolio/js/script.js'); ?>"></script>

<link rel="stylesheet" type="text/css" href="<?php $theme->linkurl('../plugin/flippy/styles.css'); ?>" />
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>-->
<script type="text/javascript" src="<?php $theme->linkurl('../plugin/flippy/jquery.flip.min.js'); ?>"></script>
<script type="text/javascript" src="<?php $theme->linkurl('../plugin/flippy/script.js'); ?>"></script>

<script type="text/javascript" src="<?php $theme->linkurl('../plugin/rating/documentation/chili/jquery.chili-2.0.js'); ?>"></script> 
<script type="text/javascript">try{ChiliBook.recipeFolder="<?php $theme->linkurl('../plugin/rating/documentation/chili/'); ?>"}catch(e){}</script>
<script src='<?php $theme->linkurl('../plugin/rating/jquery.MetaData.js'); ?>' type="text/javascript" language="javascript"></script>
<script src='<?php $theme->linkurl('../plugin/rating/jquery.rating.js'); ?>' type="text/javascript" language="javascript"></script>
<link href='<?php $theme->linkurl('../plugin/rating/jquery.rating.css'); ?>' type="text/css" rel="stylesheet"/>

<link rel='stylesheet' type='text/css' href='<?php $theme->linkurl('../plugin/fullcalendar/fullcalendar/fullcalendar.css'); ?>' />
<link rel='stylesheet' type='text/css' href='<?php $theme->linkurl('../plugin/fullcalendar/fullcalendar/fullcalendar.print.css'); ?>' media='print' />
<script type='text/javascript' src='<?php $theme->linkurl('../plugin/fullcalendar/jquery/jquery-ui-1.8.17.custom.min.js'); ?>'></script>
<script type='text/javascript' src='<?php $theme->linkurl('../plugin/fullcalendar/fullcalendar/fullcalendar.min.js'); ?>'></script>
<style type='text/css'>
	#calendar {
		width: auto;
		margin: 0 auto;
		background-color:#FFFFFF;
	}
	.dropdown-menu a{
		color:#F90;
		cursor:pointer;
	}
	.block {
		display:block;
	}
</style>

<link rel="stylesheet" type="text/css" href="<?php $theme->linkurl('../plugin/follow-sidebar/sidebar.css'); ?>" />
<script type="text/javascript">
$(function() {
	var offset = $("#sidebar").offset();
	var topPadding = -20;
	$(window).scroll(function() {
		if ($(window).scrollTop() > offset.top) {
			$("#sidebar").stop().animate({
				marginTop: $(window).scrollTop() - offset.top + topPadding
			});
		} else {
			$("#sidebar").stop().animate({
				marginTop: 0
			});
		};
	});
});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		// fade in and fade out
		$(function () {
			$(window).scroll(function () {
				if ($(this).scrollTop() > 50) {
					$('#uparrow').fadeIn();
				} else {
					$('#uparrow').fadeOut();
				}
			});
	 
			// scroll body to 0px on click
			$('#uparrow').click(function () {
				$('body,html').animate({
					scrollTop: 0
				}, 800);
				return false;
			});
		});
	 
	});
</script>

<script src="<?php $theme->linkurl('../plugin/carousel/jsCarousel-2.0.0.js'); ?>" type="text/javascript"></script>
<link href="<?php $theme->linkurl('../plugin/carousel/jsCarousel-2.0.0.css'); ?>" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	$(document).ready(function() {
		$('#carouselv').jsCarousel({ onthumbnailclick: function(src) { alert(src); }, autoscroll: true, masked: false, itemstodisplay: 3, orientation: 'v' });
	});
</script>

<link href="<?php $theme->linkurl('../plugin/treeview/css/style.css'); ?>" rel="stylesheet" type="text/css" />

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "name" : "Queens Tandoor Bali",
  "url": "http://bali.queenstandoor.com/",
  "logo": "http://bali.queenstandoor.com/themes/newqueens/img/queens-new.png",
  "contactPoint" : [{ "@type" : "ContactPoint","telephone" : "+62-361-732770","contactType" : "customer service"}],
  "sameAs" : [ "http://www.facebook.com/queensbali","http://www.twitter.com/queensbali","http://plus.google.com/+QueensTandoorBali"]
}
</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1067260556728231');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1067260556728231&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

</head>
<body>
<script type="text/javascript">
$(document).ready(function(){
$('.dropdown-toggle').dropdown();
$('.dropdown-toggle').click(function(){
	$.ajax({
	url: "<?php $theme->linkurl("function/view_temp_carts.php"); ?>",
		cache: false,
		success: function(data){
			$("#yourcart").html(data);
		}
	});
	$('.dropdown-menu').addClass('block');
});
$('.close-toggle').click(function(){
	$('.dropdown-menu').removeClass('block');
});
});
</script>
<div class="topmenu">
	<div class="container">
    	<div class="row">
            <div class="col-md-12 visible-xs right">
            <div class="dropdown left">
            <a role="button" data-toggle="dropdown" class="dropdown-toggle" href="#" onclick="return false;">
            <span class="pull-left"><span class="glyphicon glyphicon-shopping-cart"></span> your cart</span></a>
            <div class="dropdown-menu" role="menu" aria-labelledby="dLabel" style="margin-top:26px; color:#666; padding:10px 0px 15px 0px;">
   			<div style="padding:0px 15px;" class="close-toggle pull-right">
            <a href="#" onclick="return false;" style="color:#d9534f;"><span class="glyphicon glyphicon-remove"></span> close</a>
            </div>
            <div style="clear:right;"></div>
            <div class="container" id="yourcart"></div>
            </div>
            </div>
			<?php
            	$idmember=$_SESSION['member']['nomor'];
				$rowmember=mysql_fetch_array(mysql_query("SELECT nm_gues FROM guestv WHERE nomor='$idmember'"));
				$guestnm = ($rowmember['nm_gues']);
				$guestnm = substr($guestnm,0,20);
				$guestnm = substr($guestnm,0,strrpos($guestnm," "));
				
				if(!empty($idmember)){
					echo $headtitle='Hello <a href="bali.queenstandoor.com/membership">'.$guestnm.'</a> | <a href="index.php?logout=1">Logout</a>';
				}else{
					echo $headtitle='<a href="#myModal" role="button" data-toggle="modal">member login</a> | <a href="http://bali.queenstandoor.com/membership">free sign up</a>';
				}
			?>
            </div>
        	<div class="col-md-7 left">
			<span class="hidden-xs visible-sm visible-md visible-lg pull-left">HOTLINE CALL : <a href="tel://62816942942"><strong>+62816 942 942</strong></a></span>
            <form method="post" autocomplete="off" class="form-inline hidden-xs hidden-sm" role="form" action="<?php $theme->linkurl('../../search'); ?>">
              <div class="form-group">
                <label class="sr-only" for="quick">Search keyword</label>
                <input name="quick" type="text" class="form-control" id="search" placeholder="Search keyword" />
              </div>
              <button type="submit" class="btn btn-warning btn-xs">Search</button>
            </form>
          	</div>
          	<div class="col-md-5 right">
			<?php //require_once"inc/inc_login.php"; ?>
            <span style="margin-top:5px;">
			<?php
				if(!empty($idmember)){
					$headtitle='Hello <a href="http://bali.queenstandoor.com/membership">'.$guestnm.'</a> | <a href="index.php?logout=1">Logout</a>';
				}else{
					$headtitle='<a href="#myModal" role="button" data-toggle="modal">member login</a> | <a href="http://bali.queenstandoor.com/membership">free sign up</a>';
				}
			?>
            <div class="hidden-xs">
            <?php echo $headtitle; ?> <a href="<?php $theme->linkurl('../../gift-voucher'); ?>"><span class="label label-success">Gift Voucher</span></a> <!--<a href="<?php //$theme->linkurl('../../reservation'); ?>"><span class="label label-danger">Reservation &rarr;</span></a>-->
            </div>
            </span>
            </div>
      </div>
    </div>
</div>



<div class="toplogo link">
	<div class="container">
    	<div class="row">
    		<div class="col-md-3">
			<a href="<?php $theme->linkurl('../../'); ?>">
            <img src="<?php $theme->linkurl('img/queens-new.png'); ?>" border="none" />
            </a>
            <span class="hidden-xs visible-sm hidden-md hidden-lg pull-right hotline-xs">
            <small>Bali Head Office : Jl. Raya Seminyak No. 1/73 Bali</small><br />
            <small>P. +62361 732770 | F. +62361 732771<br />
E. <a href="mailto:bali@queenstandoor.com">bali@queenstandoor.com</a></small>
            </span>
            </div>
            <div class="col-md-9">
            	<span class="hidden-xs hidden-sm pull-right hotline">
				<small>Bali Head Office : Jl. Raya Seminyak No. 1/73 Bali</small><br />
				<small>P. +62361 732770 | F. +62361 732771 | E. <a href="mailto:bali@queenstandoor.com">bali@queenstandoor.com</a></small>
                </span>
            	<div class="navigator hidden-xs hidden-sm">
                	<?php require_once "inc/inc_navigator.php"; ?>
                </div>
                <div class="navbar visible-xs visible-sm">
                	<?php require_once "inc/inc_navbar.php"; ?>
                </div>
            </div>
        </div>
    </div>
</div>