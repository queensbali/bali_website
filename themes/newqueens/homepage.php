<script type="text/javascript">
	$(document).ready(function() {
		$('.slidetext').inewsticker({
			speed       : 2500,
			effect      : 'slide',
			dir         : 'ltr',
			font_size   : 13,
			color       : '#000',
			font_family : 'arial',
			delay_after : 1000						
		});
	});	
</script>
<?php
	require_once "./themes/function/func_theme.php";
	$theme=new theme();
?>
<div class="banner">
<?php 
	require_once "./themes/newqueens/inc/inc_banner.php";
	//require_once "./simple_html_dom.php";
	//$html = file_get_html('http://baliindiancuisine.com/recent-recipes');
	//$results = $html->find('div.recipe-info');
?>
</div>
<div class="content">
    <div class="row">
    	<!--<div class="col-md-12">
        	<div class="input-group">
              <span class="input-group-addon" id="basic-addon1">Latest News</span>
              <span class="form-control" aria-describedby="basic-addon1">
              <ul class="slidetext col-md-12" style="margin:0px;">
              	<?php
					//foreach($results as $result){
					//$title = $result->children(0)->plaintext;
					//$description = $result->children(3)->plaintext;
					//$link = $result->children(0)->children(0)->href;
				?>
                <li><a href="<?$link;?>" title="<?$title;?>" target="_blank"><?$title;?> - <font color="#666666"><?$description?></font></a></li>
                <?php //} ?>
              </ul>
              </span>
            </div>
        </div>-->
        <div class="col-md-3">
            <!--<div class="top-special hidden-xs hidden-sm">
            Special Today's
            </div>
            <div class="body-special hidden-xs hidden-sm">
            <?php //require_once "./themes/newqueens/inc/inc_todayspecial.php"; ?>
            </div>-->
            <div class="top-special hidden-xs hidden-sm">
            Our Partner's
            </div>
            <div class="body-special hidden-xs hidden-sm">
            <a href="http://www.exoticbalitravel.com/" target="_blank"><img class="img-responsive img-thumbnail" src="./upload/images/exotic_banner.gif" title="Exotic Bali Travel" alt="Exotic Bali Travel" /></a>
            </div>
            <div class="body-special hidden-xs hidden-sm">
            <a href="http://www.triaumatravel.com/" target="_blank"><img class="img-responsive img-thumbnail" src="./upload/images/triauma_logo.png" width="100%" title="Tria Uma Travel" alt="Tria Uma Travel" /></a>
            </div>
            <div class="body-special hidden-xs hidden-sm">
            <a href="http://ramayanatravelgroup.com/" target="_blank"><img class="img-responsive img-thumbnail" src="./upload/images/ramayana.jpg" title="Ramayana Travel Group" alt="Ramayana Travel Group" /></a>
            </div>
            <!--<div class="body-special hidden-xs hidden-sm">
            <div id="TA_cdswritereviewlg509" class="TA_cdswritereviewlg">
            <ul id="RSbEHmaIwh4y" class="TA_links GzJ73EV">
            <li id="dhuSQ4pAj3u" class="Rw6DOO">Review <a href="http://www.tripadvisor.com/Restaurant_Review-g469404-d1476371-Reviews-Queen_s_Tandoor_Indian_Restaurant-Seminyak_Bali.html">Queen&#39;s Tandoor, Indian Restaurant</a></li>
            </ul>
            </div>
            <script src="http://www.jscache.com/wejs?wtype=cdswritereviewlg&amp;uniq=509&amp;locationId=1476371&amp;lang=en_US&amp;border=false"></script>
            </div>-->
        </div>
        <div class="col-md-9 hometitle">
            <?php
				if(empty($_GET['detail'])){
					include "themes/newqueens/inc/inc_home_content.php";
				}else {
					include "themes/newqueens/inc/inc_home_detail.php";
				}
			?>
            <div class="promo-latest">
            <?php require_once "./themes/newqueens/inc/inc_promolatest.php"; ?>
            </div>
            <!-- Google Code for Remarketing Tag -->
            <!--------------------------------------------------
            Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
            --------------------------------------------------->
            <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 1007971148;
            var google_custom_params = window.google_tag_params;
            var google_remarketing_only = true;
            /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
            <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1007971148/?guid=ON&amp;script=0"/>
            </div>
            </noscript>
        </div>
        <div style="clear:both;"></div>
        <!--<div class="col-md-12">
        	<div class="promo-latest">
            <?php //require_once "./themes/newqueens/inc/inc_promolatest.php"; ?>
            </div>
        </div>-->
    </div> 
    <div style="margin-top:5px;">
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <div class="fb-like" data-href="http://facebook.com/queensbali" data-send="true" data-width="450" data-show-faces="true"></div>
    </div>
</div>