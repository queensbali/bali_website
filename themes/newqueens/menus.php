<script type="text/javascript">
	$(document).ready(function(){
			$('.tree li').each(function(){
					if($(this).children('ul').length > 0){
							$(this).addClass('parent');     
					}
			});
			
			$('.tree li.parent > a').click(function(){
					$(this).parent().toggleClass('active');
					$(this).parent().children('ul').slideToggle('fast');
			});
	});    
</script>
<div class="tree">
<?php
	function menuList($data, $parent=0){
		static $i = 1;
		$tab = str_repeat(" ",$i);
		if($data[$parent]){
			$html = "$tab<ul>";
			$i++;
			foreach($data[$parent] as $v){
				if($v->link==''){
					$link='';
				}else{
					$link='href="http://bali.queenstandoor.com/galery/'.$v->link.'"';
				}
				$child = menuList($data, $v->id);
				$html .= "$tab<li>";
				$html .= '<a '.$link.'>'.$v->title.'</a>';
				if($child){
					$i–;
					$html .= $child;
					$html .= "$tab";
				}
				$html .= '</li>';
			}
			$html .= "$tab</ul>";
			return $html;
		}
		else{
			return false;
		}
	}
	$query = mysql_query("SELECT * FROM tb_catgal WHERE last='0' ORDER BY sort");
	while($r=mysql_fetch_object($query)){
		$data[$r->parent_id][] = $r;
	}
	$menu = menuList($data);
	echo $menu;
?>
</div>