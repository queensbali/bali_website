<script type="text/javascript">
	$(document).ready(function(){
		$("#hcountry").change(function(){
			var nmcountry = $("#hcountry").val();
			$.ajax({
				type: 'POST',
				url: "<?php $theme->linkurl("function/view_country.php"); ?>",
				data: { nmcountry:nmcountry},
				cache: false,
				success: function(data){
					$("#hmobile").val(data);
				}
			});
		});
                $("#delivery").hide();
		$("#preorder").click(function(){
			if($(this).is(":checked")){
				$("#delivery").show();
				$("#title").disabled = true;
			}else {
				$("#delivery").hide();
				$("#title").disabled = true;
			}
		});
	});
</script>
<?php 
	session_start();
	$sessionID = $_COOKIE['PHPSESSID'];
	$option=$_POST['option']; 
	$idmember=$_SESSION['member']['id_billing'];
	
	$selpostal=mysql_fetch_array(mysql_query("SELECT tb_carts.id_cart, tb_postal.* FROM tb_carts INNER JOIN tb_postal ON tb_carts.pcode = tb_postal.pcode WHERE tb_carts.cart_session='$sessionID'"));
?>
<form action="<?php $_SERVER['PHP_SELF']; ?>" method="post" class="form-horizontal" role="form">
  <fieldset>
  <legend>Account Information</legend>
  <div class="form-group">
  	<label for="title" class="col-md-3 control-label">Delivery Time</label>
    <div class="col-sm-9">
      <div class="checkbox">
        <label>
          <input name="preorder" id="preorder" type="checkbox" value="Preorder" required="required"> Preorder
        </label>
      </div>
    </div>
  </div>
  <div class="form-group" id="delivery">
    <div class="col-sm-offset-3 col-sm-5">
    <?php $date=strtotime( '+1 day',strtotime(date("Y-m-d"))); ?>
    <select class="form-control" name="devdate" id="devdate">
      <option value="Today (<?=date("Y-m-d");?>)">Today (<?=date("Y-m-d");?>)</option>
      <option value="Tomorow (<?=date("Y-m-d",$date);?>)">Tomorow (<?=date("Y-m-d",$date);?>)</option>
    </select>
    </div>
    <div class="col-sm-4">
    <select class="form-control" name="devtime" id="devtime">
      <?php for ($time = strtotime("13:00"); $time <= strtotime("22:00"); $time = strtotime("+30 minute", $time)) { ?>
      <option value="<?php echo date("H:i", $time); ?>"><?php echo date("H:i", $time); ?></option>
      <?php } ?>
    </select>
    </div>
  </div>
  <div class="form-group">
    <label for="title" class="col-md-3 control-label">Title</label>
    <div class="col-md-9">
    <input name="type" type="hidden" value="<?php echo $option; ?>" />
    <select class="form-control" name="title" id="title">
      <option value="Mr.">Mr.</option>
      <option value="Ms.">Ms.</option>
      <option value="Mrs.">Mrs.</option>
    </select>
    </div>
  </div>
  <div class="form-group">
    <label for="title" class="col-sm-3 control-label">First / Last Name</label>
    <div class="col-sm-5">
    <input type="text" class="form-control" id="fname" placeholder="First Name" name="fname" required>
    </div>
    <div class="col-sm-4">
    <input type="text" class="form-control" id="lname" placeholder="Last Name" name="lname">
    </div>
  </div>
  <div class="form-group">
    <label for="company" class="col-sm-3 control-label">Company</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="company" name="company" placeholder="Company Name">
    </div>
  </div>
  <div class="form-group">
    <label for="haddress" class="col-sm-3 control-label">Full Address</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="haddress" name="haddress" placeholder="Full Address" required>
    </div>
  </div>
  <div class="form-group">
    <label for="hcity" class="col-sm-3 control-label">City / Area / Postal</label>
    <div class="col-sm-3">
      <select name="hcity" id="hcity" class="form-control">
      	<?php
			$selpos=mysql_query("SELECT * FROM tb_postal WHERE pcode='$selpostal[pcode]'");
			while($rowpos=mysql_fetch_array($selpos)){
		?>
        <option value="<?=$rowpos['regency'].' - '.$rowpos['city'];?>">
        <?=$rowpos['regency'].' - '.$rowpos['city'];?>
        </option>
        <?php } ?>
      </select>
    </div>
    <div class="col-sm-3">
      <input id="harea" name="harea" type="hidden" value="<?=$selpostal['province'];?>" />
      <input type="text" class="form-control" id="harea2" name="harea2" placeholder="Area" value="<?=$selpostal['province'];?>" disabled="disabled" required>
    </div>
    <div class="col-sm-3">
      <input id="hpost" name="hpost" type="hidden" value="<?=$selpostal['pcode'];?>" />
      <input type="text" class="form-control" id="hpost2" name="hpost2" placeholder="Postal Code" value="<?=$selpostal['pcode'];?>" disabled="disabled">
    </div>
  </div>
  <div class="form-group">
    <label for="hcountry" class="col-sm-3 control-label">Country</label>
    <div class="col-sm-9">
    <select class="form-control" id="hcountry" name="hcountry">
    <option selected="selected" value="">Select one country</option>
	<?php
		$selcountry=mysql_query("SELECT * FROM tb_country");
		while($rowcountry=mysql_fetch_array($selcountry)){
	?>
	<option value="<?=$rowcountry['id_country'];?>"><?=$rowcountry['country_name'];?></option>
	<?php } ?>
    </select>
    </div>
  </div>
  <div class="form-group">
    <label for="hphone" class="col-sm-3 control-label">Phone / Fax / Mobile</label>
    <div class="col-sm-3">
      <input type="text" class="form-control" id="hphone" name="hphone" placeholder="Phone Number (+62)">
    </div>
    <div class="col-sm-3">
      <input type="text" class="form-control" id="hfax" name="hfax" placeholder="Fax Number">
    </div>
    <div class="col-sm-3">
      <input type="text" class="form-control" id="hmobile" name="hmobile" placeholder="Mobile Number" required>
    </div>
  </div>
  <div class="form-group">
    <label for="hmail" class="col-sm-3 control-label">Email Address</label>
    <div class="col-sm-9">
      <input type="email" class="form-control" id="hmail" name="hmail" placeholder="info@example.com" required>
    </div>
  </div>
  <?php
  	if($option=='guest'){}
	else {
  ?>
  <div class="form-group">
    <label for="hmail" class="col-sm-3 control-label">Password</label>
    <div class="col-sm-9">
      <input type="password" class="form-control" id="passwd" name="passwd" placeholder="password" required>
    </div>
  </div>
  <?php } ?>
  <div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
      <div class="checkbox">
        <label>
          <input name="subscribe" id="subscribe" type="checkbox" value="YES"> Subscribe to our Mailing List
        </label>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
      <button type="submit" name="bsubmit" class="btn btn-danger pull-right">Continue &rarr;</button>
    </div>
  </div>
  <?php
  	if(isset($_POST['bsubmit'])){
		$title=$_POST['title'];
		$fname=$_POST['fname'];
		$lname=$_POST['lname'];
		$company=$_POST['company'];
		$haddress=$_POST['haddress'];
		$hcity=$_POST['hcity'];
		$harea=$_POST['harea'];
		$hcode=$_POST['hpost'];
		$hcountry=$_POST['hcountry'];
		$hphone=$_POST['hphone'];
		$hfax=$_POST['hfax'];
		$hmobile=$_POST['hmobile'];
		$hmail=$_POST['hmail'];
		$passwd=sha1(md5($_POST['passwd']));

                $delivery=$_POST['devdate'].' | '.$_POST['devtime'];
		
		if(!empty($_POST['subscribe'])){
			$subscribe=$_POST['subscribe'];
		}else{
			$subscribe='NO';
		}
		$datein=date('Y-m-d');
		$dt=date('ym');
		
		if($_POST['type']=='member'){
			$type='MEMBER';
			$idmember=no_auto("tb_carts_member",'MEM'.$dt);
			$idbilling=no_auto("tb_carts_billing",'BIL'.$dt);
			$idcheck=no_auto("tb_carts_checkout",'CRT'.$dt);
			
			$save=mysql_query("INSERT INTO tb_carts_member(id_member, datein, title, fname, lname, company, haddress, hcity, harea, hcode, hcountry, hphone, hfax, hmobile, email, passwd, subscribe, btype) VALUES ('$idmember','$datein','$title','$fname','$lname','$company','$haddress','$hcity','$harea','$hcode','$hcountry','$hphone','$hfax','$hmobile','$hmail','$passwd','$subscribe','$type')");
			
			$save=mysql_query("INSERT INTO tb_carts_billing(id_billing, datein, cart_session, title, fname, lname, company, haddress, hcity, harea, hcode, hcountry, hphone, hfax, hmobile, email, remark, subscribe, btype) VALUES ('$idbilling','$datein','$sessionID','$title','$fname','$lname','$company','$haddress','$hcity','$harea','$hcode','$hcountry','$hphone','$hfax','$hmobile','$hmail','$delivery','$subscribe','$type')");
			
			$cart=mysql_query("INSERT INTO tb_carts_checkout(id_checkout,datein,cart_session,id_billing)VALUES('$idcheck','$datein','$sessionID','$idbilling')");
			if($save){
				echo'<script type="text/javascript">window.location="http://bali.queenstandoor.com/checkout/register/payment";</script>';
			}
		}else{
			$type='GUEST';
			$idbilling=no_auto("tb_carts_billing",'BIL'.$dt);
			$idcheck=no_auto("tb_carts_checkout",'CRT'.$dt);
			
			$save=mysql_query("INSERT INTO tb_carts_billing(id_billing, datein, cart_session, title, fname, lname, company, haddress, hcity, harea, hcode, hcountry, hphone, hfax, hmobile, email, remark, subscribe, btype) VALUES ('$idbilling','$datein','$sessionID','$title','$fname','$lname','$company','$haddress','$hcity','$harea','$hcode','$hcountry','$hphone','$hfax','$hmobile','$hmail','$delivery','$subscribe','$type')");
			$cart=mysql_query("INSERT INTO tb_carts_checkout(id_checkout,datein,cart_session,id_billing)VALUES('$idcheck','$datein','$sessionID','$idbilling')");
			if($save){
				echo'<script type="text/javascript">window.location="http://bali.queenstandoor.com/checkout/register/payment";</script>';
			}
		}
	}
  ?>
  </fieldset>
</form>