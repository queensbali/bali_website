<script type="text/javascript">
	function refreshCaptcha(){
		var img = document.images['captchaimg'];
		img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
	}
</script>
<div class="tabbable"> <!-- Only required for left/right tabs -->
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab1" data-toggle="tab">Subscribe</a></li>
        <li><a href="#tab2" data-toggle="tab">Unsubscribe</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
        <form class="form-horizontal" role="form" onsubmit="return ceksub()" name="form1" method="post">
          <div class="form-group">
            <label for="firstname" class="col-sm-2 control-label">Full Name</label>
            <div class="col-sm-5">
              <input name="firstname" type="text" class="form-control" id="firstname" placeholder="Insert your first name.." required>
            </div>
            <div class="col-sm-5">
              <input name="lastname" type="text" class="form-control" id="lastname" placeholder="Insert your last name..">
            </div>
          </div>
          <div class="form-group">
            <label for="mail" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
              <input name="mail" type="email" class="form-control" id="mail" placeholder="Insert your mail address.." required>
            </div>
          </div>
          <div class="form-group">
            <label for="mobile" class="col-sm-2 control-label">Mobile Number</label>
            <div class="col-sm-10">
              <input name="mobile" type="text" class="form-control" id="mobile" placeholder="Insert your mobile number.." required>
            </div>
          </div>
          <div class="form-group">
            <label for="country" class="col-sm-2 control-label">Country</label>
            <div class="col-sm-10">
            <select class="form-control" id="country" name="country">
            <?php include "./themes/newqueens/inc/inc_country.php"; ?>
            </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
            <img src="<?php $theme->linkurl('../../captcha/captcha.php'); ?>?rand=<?php echo rand(); ?>" id='captchaimg' ><br />
            <small>Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh</small>
            </div>
          </div>
          <div class="form-group">
            <label for="inputSubject" class="col-sm-2 control-label">Verify Code</label>
            <div class="col-sm-10">
              <input name="vcode" type="text" class="form-control" id="inputSubject" placeholder="Insert validation code.." required>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" name="ssubscribe" class="btn btn-warning">Submit Form</button>
              <button type="reset" name="clear" class="btn btn-default">Clear</button>
            </div>
          </div>
          <?php	
			$idsubs=no_auto("tb_subscribe",'SUBS');
			$datein=date('Y-m-d');
			$fname=$theme->injection($_POST['firstname']);
			$lname=$theme->injection($_POST['lastname']);
			$fullname=$fname.' '.$lname;
			$mailto=$theme->injection($_POST['mail']);
			$phone=$theme->injection($_POST['mobile']);
			$country=$theme->injection($_POST['country']);
			$captcha=$_POST['vcode'];
			
			if(isset($_POST['ssubscribe'])){
				if($_SESSION['6_letters_code']!=$captcha){
					echo'<script type="text/javascript">alert("Code validation not valid..");</script>';
				}
				else {
					include $theme->linkurl('../function/')."random_code.php";
					$save=mysql_query("INSERT INTO tb_subscribe(id_subscribe,datein,fullname,mail,phone,country,subscribe,codever,active)VALUES('$idsubs','$datein','$fullname','$mailto','$phone','$country','Yes','$codever','No')");
					
					require_once($theme->incurl('../../').'./themes/plugin/phpmailer/class.phpmailer.php');
					require_once($theme->incurl('../../').'./themes/plugin/phpmailer/class.smtp.php'); 
					$mail = new PHPMailer();		
					//SMTP Class
					$mail->IsSMTP(); //set untuk menggunakan SMTP
					$mail->SMTPDebug  = 1;                     
					//SMTP Setting
					$mail->SMTPAuth   = true;                  
					$mail->SMTPSecure = "tls";                 
					$mail->Host       = "smtp.gmail.com";      
					$mail->Port       = 587; 
					$mail->Username   = "noreply@queenstandoor.com";
					$mail->Password   = "queens1976"; 
					//Email From
					$mail->SetFrom("noreply@queenstandoor.com", "noreply@queenstandoor.com");
					$mail->AddReplyTo("noreply@queenstandoor.com","noreply@queenstandoor.com");
					$mail->Subject="Thank you very much for SUBSCRIBING";
					$mail->AltBody="To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
					//Email Letter
					ob_start();
					require_once($theme->incurl('../../').'./themes/email/subscribe-form.php');
					$html_message = ob_get_contents();
					ob_end_clean();
					//Email HTML Content
					$mail->MsgHTML($html_message);
					$mail->IsHTML(true); 
					//Email To
					$mail->AddAddress($mailto, $fullname);
					$mail->AddCC('puneet@queenstandoor.com', 'Puneet Malhotra');
					$mail->AddCC('bali2@queenstandoor.com', 'Marketing Queens');
					$mail->AddCC('designer@queenstandoor.com', 'Queens Designer');
					$mail->AddBCC('dsg1_qt1_bali@yahoo.com','Jonny Cible');
					if(!$mail->Send()) {
					  echo "Mailer Error: " . $mail->ErrorInfo;
					} else { }
					
					if($save){
						echo'<script type="text/javascript">alert("Congratulations! You have successfully SUBSCRIBED to the QUEENS Newsletter..");</script>';
					}else {
						echo'<script type="text/javascript">alert("Error, Data cannot has been saved..");</script>';
					}
				}
			}
		?>
        </form>
        </div>
        <div class="tab-pane" id="tab2">
        <script type="text/javascript">
			var currentEnabled = null;
				function enableElement(elem) {
				if (currentEnabled) {
					currentEnabled.disabled = true;
				}
				elem.disabled = false;
				currentEnabled = elem;
			}
		</script>
        <form class="form-horizontal" role="form" onsubmit="return ceksub()" name="form1" method="post">
          <div class="form-group">
            <label for="gender" class="col-sm-2 control-label">Option</label>
            <div class="col-sm-10">
              <label>
                <input type="radio" name="option" id="option" value="Phone" required="required" onclick="enableElement(this.form.elements['mobile']);"> Phone
              </label> 
              <label>
                <input type="radio" name="option" id="option" value="Email" onclick="enableElement(this.form.elements['mail']);"> Email
              </label>
            </div>
          </div>
          <div class="form-group">
            <label for="mail" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
              <input name="mail" type="email" class="form-control" id="mail" placeholder="Insert your mail address.." disabled="disabled" required>
            </div>
          </div>
          <div class="form-group">
            <label for="mobile" class="col-sm-2 control-label">Mobile Number</label>
            <div class="col-sm-10">
              <input name="mobile" type="text" class="form-control" id="mobile" placeholder="Insert your mobile number.." disabled="disabled" required>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" name="unsubscribe" class="btn btn-warning">Submit Form</button>
              <button type="reset" name="clear" class="btn btn-default">Clear</button>
            </div>
          </div>
          <?php
		  	if(isset($_POST['unsubscribe'])){
				$umail=$theme->injection($_POST['mail']);
				$umobile=$theme->injection($_POST['mobile']);
				$unsub=mysql_query("UPDATE tb_subscribe SET subscribe='No' WHERE mail='$umail' OR phone='$umobile'");
				
				require_once($theme->incurl('../../').'./themes/plugin/phpmailer/class.phpmailer.php');
				require_once($theme->incurl('../../').'./themes/plugin/phpmailer/class.smtp.php'); 
				$mail = new PHPMailer();		
				//SMTP Class
				$mail->IsSMTP(); //set untuk menggunakan SMTP
				$mail->SMTPDebug  = 1;                     
				//SMTP Setting
				$mail->SMTPAuth   = true;                  
				$mail->SMTPSecure = "tls";                 
				$mail->Host       = "smtp.gmail.com";      
				$mail->Port       = 587; 
				$mail->Username   = "noreply@queenstandoor.com";
				$mail->Password   = "queens1976"; 
				//Email From
				$mail->SetFrom("noreply@queenstandoor.com", "noreply@queenstandoor.com");
				$mail->AddReplyTo("noreply@queenstandoor.com","noreply@queenstandoor.com");
				$mail->Subject="UNSUBSCRIBING";
				$mail->AltBody="To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
				//Email Letter
				ob_start();
				require_once($theme->incurl('../../').'./themes/email/unsubscribe-form.php');
				$html_message = ob_get_contents();
				ob_end_clean();
				//Email HTML Content
				$mail->MsgHTML($html_message);
				$mail->IsHTML(true); 
				//Email To
				$mail->AddAddress($mailto, $fullname);
				$mail->AddCC('puneet@queenstandoor.com', 'Puneet Malhotra');
				$mail->AddCC('designer@queenstandoor.com', 'Queens Designer');
				$mail->AddBCC('dsg1_qt1_bali@yahoo.com','Jonny Cible');
				if(!$mail->Send()) {
				  echo "Mailer Error: " . $mail->ErrorInfo;
				} else { }
				
				if($unsub){
					echo'<script type="text/javascript">alert("Success, Your unsubscribe has been successfully..");</script>';
				}else {
					echo'<script type="text/javascript">alert("Error, Your data not match..");</script>';
				}
			}
		  ?>
        </form>
        </div>
    </div>
</div>