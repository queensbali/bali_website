<?php 
	$idmember=$_SESSION['member']['nomor']; 
	if(!empty($idmember)){
		$tabacc='Account Setting';
		$tabadd='<li class="'.$visibletab.'"><a href="#tab4" data-toggle="tab">Transaction</a></li>';
		$hidetab='hidden-xs hidden-sm hidden-md hidden-lg';
		$visibletab='';
	}else {
		$tabacc='Member Registration';
		$tabadd='';
		$hidetab='';
		$visibletab='hidden-xs hidden-sm hidden-md hidden-lg';
	}
?>
<div class="tabbable"> <!-- Only required for left/right tabs -->
    <ul class="nav nav-tabs">
        <li><a href="#tab1" data-toggle="tab">Welcome</a></li>
        <li class="active"><a href="#tab2" data-toggle="tab"><?php echo $tabacc; ?></a></li>
        <li class="<?php //echo $hidetab; ?>"><a href="#tab3" data-toggle="tab">Partner</a></li>
        <?php echo $tabadd; ?>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" id="tab1">
        <p>
        The Queen's Tandoor VIP Membership Programme allows selected clients to benefit from a range of discounts and rewards points for each transaction.
		Our VIP members also enjoy discounts and promotional rewards at a number of Bali's most popular tourist destinations.<br />
		The Queen's Tandoor VIP Membership Programme requires an annual renewal.<br />
		Membership eligibility is decided exclusively by the Queen's management, complete the registration form to apply for VIP status.
        </p><br />
        </div>
        <div class="tab-pane active" id="tab2">
        <?php
			if(!empty($idmember)){ 
				require_once "./themes/newqueens/inc/inc_memberform_update.php";
			}else {
				require_once "./themes/newqueens/inc/inc_memberform.php";
			}
		?>
        </div>
        <div class="tab-pane <?php //echo $hidetab; ?>" id="tab3">
            <div class="row">
                <div class="col-md-12">
                <?php require_once "./themes/newqueens/inc/inc_partner.php"; ?>
                </div>
                <!--<div class="col-md-3">
                    <div class="top-brown">
                    Widget
                    </div>
                    <div class="body-brown hidden-xs hidden-sm">
                    Testing Widget
                    </div>
                </div>-->
            </div>
        </div>
        <div class="tab-pane <?=$visibletab;?>" id="tab4">
        <?php require_once "./themes/newqueens/inc/inc_member_transaction.php"; ?>
        </div>
    </div>
</div>