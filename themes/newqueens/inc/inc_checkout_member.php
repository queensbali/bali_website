<form action="<?php $_SERVER['PHP_SELF']; ?>" method="post" class="form-horizontal" role="form">
  <fieldset>
  <legend>Billing Information</legend>
  <div class="form-group">
    <label for="title" class="col-md-3 control-label">Title</label>
    <div class="col-md-9">
    <select class="form-control" name="title" id="title">
      <option value="Mr.">Mr.</option>
      <option value="Ms.">Ms.</option>
      <option value="Mrs.">Mrs.</option>
    </select>
    </div>
  </div>
  <div class="form-group">
    <label for="title" class="col-sm-3 control-label">First / Last Name</label>
    <div class="col-sm-5">
    <input type="text" class="form-control" id="fname" placeholder="First Name" name="fname" required>
    </div>
    <div class="col-sm-4">
    <input type="text" class="form-control" id="lname" placeholder="Last Name" name="lname">
    </div>
  </div>
  <div class="form-group">
    <label for="company" class="col-sm-3 control-label">Company</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="company" name="company" placeholder="Company Name" required>
    </div>
  </div>
  <div class="form-group">
    <label for="haddress" class="col-sm-3 control-label">Home Address</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="haddress" name="haddress" placeholder="Home Address" required>
    </div>
  </div>
  <div class="form-group">
    <label for="hcity" class="col-sm-3 control-label">City / Area / Postal</label>
    <div class="col-sm-3">
      <input type="text" class="form-control" id="hcity" name="hcity" placeholder="City" required>
    </div>
    <div class="col-sm-3">
      <input type="text" class="form-control" id="harea" name="harea" placeholder="Area" required>
    </div>
    <div class="col-sm-3">
      <input type="text" class="form-control" id="hpost" name="hpost" placeholder="Postal Code">
    </div>
  </div>
  <div class="form-group">
    <label for="hcountry" class="col-sm-3 control-label">Country</label>
    <div class="col-sm-9">
    <select class="form-control" id="hcountry" name="hcountry">
	<?php include "./themes/newqueens/inc/inc_country.php"; ?>
    </select>
    </div>
  </div>
  <div class="form-group">
    <label for="hphone" class="col-sm-3 control-label">Phone / Fax / Mobile</label>
    <div class="col-sm-3">
      <input type="text" class="form-control" id="hphone" name="hphone" placeholder="Phone Number">
    </div>
    <div class="col-sm-3">
      <input type="text" class="form-control" id="hfax" name="hfax" placeholder="Fax Number">
    </div>
    <div class="col-sm-3">
      <input type="text" class="form-control" id="hmobile" name="hmobile" placeholder="Mobile Number" required>
    </div>
  </div>
  <div class="form-group">
    <label for="hmail" class="col-sm-3 control-label">Email Address</label>
    <div class="col-sm-9">
      <input type="email" class="form-control" id="hmail" name="hmail" placeholder="info@example.com" required>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
      <div class="checkbox">
        <label>
          <input name="subscribe" id="subscribe" type="checkbox" value="YES"> Subscribe to our Mailing List
        </label>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
      <button type="submit" name="bsubmit" class="btn btn-danger pull-right">Continue &rarr;</button>
    </div>
  </div>
  <?php
  	session_start();
	$sessionID = $_COOKIE['PHPSESSID'];
  	if(isset($_POST['bsubmit'])){
		$title=$_POST['title'];
		$fname=$_POST['fname'];
		$lname=$_POST['lname'];
		$company=$_POST['company'];
		$haddress=$_POST['haddress'];
		$hcity=$_POST['hcity'];
		$harea=$_POST['harea'];
		$hcode=$_POST['hpost'];
		$hcountry=$_POST['hcountry'];
		$hphone=$_POST['hphone'];
		$hfax=$_POST['hfax'];
		$hmobile=$_POST['hmobile'];
		$hmail=$_POST['hmail'];
		if(!empty($_POST['subscribe'])){
			$subscribe=$_POST['subscribe'];
		}else{
			$subscribe='NO';
		}
		$datein=date('Y-m-d');
		$dt=date('ym');
		$idbilling=no_auto("tb_carts_billing",'BIL'.$dt);
		$idcheck=no_auto("tb_carts_checkout",'CRT'.$dt);
		
		$save=mysql_query("INSERT INTO tb_carts_billing(id_billing, datein, cart_session, title, fname, lname, company, haddress, hcity, harea, hcode, hcountry, hphone, hfax, hmobile, email, subscribe, btype) VALUES ('$idbilling','$datein','$sessionID','$title','$fname','$lname','$company','$haddress','$hcity','$harea','$hcode','$hcountry','$hphone','$hfax','$hmobile','$hmail','$subscribe','GUEST')");
		$cart=mysql_query("INSERT INTO tb_carts_checkout(id_checkout,datein,cart_session,id_billing)VALUES('$idcheck','$datein','$sessionID','$idbilling')");
		if($save){
			echo'<script type="text/javascript">window.location="http://192.168.1.131/queens/checkout/register/shipping";</script>';
		}
	}
  ?>
  </fieldset>
</form>