<?php
	$geturl=$theme->geturl('2');
	if($geturl=="Awards"){
		$tab1='';
		$tab2='active';
		$tab3='';
	}else if($geturl=="Membership"){
		$tab1='';
		$tab2='';
		$tab3='active';
	}else{
		$tab1='active';
		$tab2='';
		$tab3='';
	}
?>
<div class="tabbable"> <!-- Only required for left/right tabs -->
    <ul class="nav nav-tabs">
        <li onClick="window.location='<?php echo $theme->getlink('','1'); ?>/news'" class="<?=$tab1;?>"><a href="#tab1" data-toggle="tab">Queen's On News</a></li>
        <li onClick="window.location='<?php echo $theme->getlink('','1'); ?>/awards'" class="<?=$tab2;?>"><a href="#tab2" data-toggle="tab">Awards & Accolades</a></li>
        <li onClick="window.location='<?php echo $theme->getlink('','1'); ?>/membership'" class="<?=$tab3;?>"><a href="#tab3" data-toggle="tab">Membership & Association</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane <?=$tab1;?>" id="tab1">
        	<?php require"themes/newqueens/inc/inc_news.php"; ?>
        </div>
        <div class="tab-pane <?=$tab2;?>" id="tab2">
        	<?php require"themes/newqueens/inc/inc_awards.php"; ?>
        </div>
        <div class="tab-pane <?=$tab3;?>" id="tab3">
        	<?php require"themes/newqueens/inc/inc_members.php"; ?>
        </div>
    </div>
</div>