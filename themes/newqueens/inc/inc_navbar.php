<nav class="navbar navbar-inverse" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" style="background:#333;">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand visible-xs" href="#" style="color:#F90;">Menu</a>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
      <?php
	  	$reqnav=$_SERVER['REQUEST_URI'];
		$pchnav=explode('/',$reqnav);
		$navgat=$pchnav[3];
		$repgat=ucwords(str_replace('-',' ',$navgat));
		
	  	$selnav=mysql_query("SELECT * FROM tb_navigator WHERE category='Top' AND publish='Yes' ORDER BY position ASC");
		while($nav=mysql_fetch_array($selnav)){
			if(!empty($nav['link'])){
				$link='../../'.$nav['link'];
				$menu='';
			}else{
				$link='../../content/';
				$menu=strtolower(str_replace(" ","-",$nav['title']));
			}
			if($repgat==$nav['title']){
				$back="#c45900";
			}else {
				$back="";
			}
			
			if($repgat==$nav['title']){
				$aktif='class="active"';
			}else {
				$aktif='';
			}
	  ?>
      <li <?=$aktif;?>>
      <a href="<?php $theme->linkurl($link.''.$menu); ?>" title="<?php echo $nav['menu']; ?>" style="color:#f2dcb7;">
		<?php echo strtoupper($nav['menu']); ?>
      </a>
      </li>
      <?php } ?>
    </ul>
  </div><!-- /.navbar-collapse -->
</nav>
