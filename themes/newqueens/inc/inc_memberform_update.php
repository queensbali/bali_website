<script type="text/javascript">
	function refreshCaptcha(){
		var img = document.images['captchaimg'];
		img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
	}
	$(function() {
		$("#datepicker").datepicker({
		  dateFormat:"yy-mm-dd",
		  changeMonth: true,
		  changeYear: true,
		});
		$("#datepicker2").datepicker({
		  dateFormat:"yy-mm-dd",
		  changeMonth: true,
		  changeYear: true,
		});
	});
</script>
<?php 
	$idmember=$_SESSION['member']['nomor']; 
	$selmem=mysql_fetch_array(mysql_query("SELECT guestv.nomor, guestv.password, guesto.* FROM guestv INNER JOIN guesto ON guestv.kd_gues = guesto.kode WHERE guestv.nomor='$idmember'"));
	$idcustomer=$selmem['kode'];
?>
<button type="submit" name="rsubmit" class="btn btn-danger pull-right" data-toggle="modal" data-target="#myModal">
<span class="glyphicon glyphicon-lock"></span> Change Password
</button>
<div style="clear:right; margin-bottom:10px;"></div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">
        Change Password
        </h4>
      </div>
      <div class="modal-body">
      <script type="text/javascript">
	  	function check(){
			var passwd = '<?php echo trim($selmem['password']); ?>';
			var current = document.getElementById("current").value;
			var newpass = document.getElementById("newpass").value;
			var conpass = document.getElementById("confirm").value;
			
			if(passwd != current){
            	alert('Wrong password..!');
				document.getElementById("current").focus();
				document.getElementById("current").value="";
				return false;
        	}else if(newpass != conpass){
				alert('Password not match..!');
				document.getElementById("newpass").focus();
				document.getElementById("newpass").value="";
				document.getElementById("confirm").value="";
				return false;
			}else {
				return true;
			}
		}
	  </script>
      <form action="<?php $_SERVER['PHP_SELF']; ?>" method="post" class="form-horizontal" role="form" onsubmit="return check()">
      <div class="form-group">
        <label for="current" class="col-sm-4 control-label">Current Password</label>
        <div class="col-sm-8">
          <input type="password" class="form-control" id="current" name="current" placeholder="Current password" required>
        </div>
      </div><div class="form-group">
        <label for="newpass" class="col-sm-4 control-label">New Password</label>
        <div class="col-sm-8">
          <input type="password" class="form-control" id="newpass" name="newpass" placeholder="New password" required>
        </div>
      </div>
      <div class="form-group">
        <label for="confirm" class="col-sm-4 control-label">Confirm Password</label>
        <div class="col-sm-8">
          <input type="password" class="form-control" id="confirm" name="confirm" placeholder="Confirm password" required>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-4 col-sm-8">
          <button type="submit" name="change" class="btn btn-danger">
          <span class="glyphicon glyphicon-lock"></span> Save Password
          </button>
        </div>
      </div>
      <?php
	  	$dateput=date('Y-m-d');
		$old=$_POST['current'];
	  	$current=$_POST['newpass'];
		$passwd=sha1(md5($current));
		if(isset($_POST['change'])){
			$updt=mysql_query("UPDATE guestv SET password='$current' WHERE nomor='$idmember'");
			$sql=mysql_query("INSERT INTO tb_change_password(datein,id_guest,id_vip,old_pass,new_pass)VALUES('$dateput','$idcustomer','$idmember','$old','$passwd')");
			if($updt&&$sql){
				$msg='<p class="bg-success">Success, Password has been change..</p>';
			}else {
				$msg='<p class="bg-danger">Error, Password cannot be change..</p>';
			}
		}
	  ?>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php echo $msg; ?>
<form action="<?php $_SERVER['PHP_SELF']; ?>" method="post" class="form-horizontal" role="form">
  <fieldset>
  <legend>Personal Detail</legend>
  <div class="form-group">
    <label for="title" class="col-sm-2 control-label">Title / Fullname</label>
    <div class="col-sm-2">
    <select class="form-control" name="title" id="title">
      <?php
	  	if(trim($selmem['title'])=='Mr.'){
			$mr='selected="selected"';
			$ms='';
			$mrs='';
		}else if(trim($selmem['title'])=='Ms.'){
			$mr='';
			$ms='selected="selected"';
			$mrs='';
		}else {
			$mr='';
			$ms='';
			$mrs='selected="selected"';
		}
	  ?>
      <option value="Mr." <?=$mr;?>>Mr.</option>
      <option value="Ms." <?=$ms;?>>Ms.</option>
      <option value="Mrs." <?=$mrs;?>>Mrs.</option>
    </select>
    </div>
    <div class="col-sm-4">
    <input type="text" class="form-control" id="fname" placeholder="First Name" name="fname" value="<?=trim($selmem['fname']);?>" required>
    </div>
    <div class="col-sm-4">
    <input type="text" class="form-control" id="lname" placeholder="Last Name" name="lname" value="<?=trim($selmem['lname']);?>" required>
    </div>
  </div>
  <div class="form-group">
  	<label for="gender" class="col-sm-2 control-label">Gender</label>
  	<div class="col-sm-10">
      <?php
	  	if(trim($selmem['sex'])=='Male'){
			$male='checked="checked"';
			$female='';
		}else {
			$male='';
			$female='checked="checked"';
		}
	  ?>
      <label>
        <input type="radio" name="sex" id="gender" value="Male" <?=$male;?> required="required"> Male
      </label> 
      <label>
        <input type="radio" name="sex" id="gender" value="Female" <?=$female;?>> Female
      </label>
    </div>
  </div>
  <div class="form-group">
    <label for="nationality" class="col-sm-2 control-label">Nationality</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nationality" name="nationality" placeholder="Nationality" value="<?=trim($selmem['national']);?>" required>
    </div>
  </div>
  <div class="form-group">
    <label for="religion" class="col-sm-2 control-label">Religion</label>
    <div class="col-sm-10">
    <select class="form-control" name="religion" id="religion">
        <option value="N/A">N/A</option>
		<?php
			$selrel=mysql_query("SELECT * FROM tb_religion WHERE publish='YES'");
			while($rowrel=mysql_fetch_array($selrel)){
				if($rowrel['slug']==trim($selmem['religion'])){
					$selected='selected="selected"';
				}else {
					$selected='';
				}
		?>
        <option value="<?=$rowrel['slug'];?>" <?=$selected;?>><?=$rowrel['title'];?></option>
        <?php } ?>
    </select>
    </div>
  </div>
  <div class="form-group">
    <label for="haddress" class="col-sm-2 control-label">Home Address</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="haddress" name="haddress" placeholder="Home Address" value="<?=trim($selmem['haddress1']);?>" required>
    </div>
  </div>
  <div class="form-group">
    <label for="hcity" class="col-sm-2 control-label">City / Area / Postal</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="hcity" name="hcity" placeholder="City" value="<?=trim($selmem['hcity']);?>" required>
    </div>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="harea" name="harea" placeholder="Area" value="<?=trim($selmem['harea']);?>" required>
    </div>
    <div class="col-sm-2">
      <input type="text" class="form-control" id="hpost" name="hpost" placeholder="Postal Code" value="<?=trim($selmem['hpostcode']);?>">
    </div>
  </div>
  <div class="form-group">
    <label for="hcountry" class="col-sm-2 control-label">Country</label>
    <div class="col-sm-10">
    <select class="form-control" id="hcountry" name="hcountry">
	<?php include "./themes/newqueens/inc/inc_country.php"; ?>
    </select>
    </div>
  </div>
  <div class="form-group">
    <label for="hphone" class="col-sm-2 control-label">Phone / Fax / Mobile</label>
    <div class="col-sm-3">
      <input type="text" class="form-control" id="hphone" name="hphone" placeholder="Phone Number" value="<?=trim($selmem['hphone1']);?>">
    </div>
    <div class="col-sm-3">
      <input type="text" class="form-control" id="hfax" name="hfax" placeholder="Fax Number" value="<?=trim($selmem['hfax1']);?>">
    </div>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="hmobile" name="hmobile" placeholder="Mobile Number" value="<?=trim($selmem['hp1']);?>" required>
    </div>
  </div>
  <div class="form-group">
    <label for="hmail" class="col-sm-2 control-label">Email Address</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="hmail" name="hmail" placeholder="info@example.com" value="<?=trim($selmem['mail1']);?>">
    </div>
  </div>
  <div class="form-group">
    <label for="hweb" class="col-sm-2 control-label">Website</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="hweb" name="hweb" placeholder="http://" value="<?=trim($selmem['web_site']);?>">
    </div>
  </div>
  <legend>Private Detail</legend>
  <div class="form-group">
    <label for="birthplace" class="col-sm-2 control-label">Place / Date of Birth</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="birthplace" name="birthplace" placeholder="Place of Birth" value="<?=trim($selmem['pbirth']);?>">
    </div>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="datepicker" name="idate3" placeholder="Date of Birth" value="<?=trim($selmem['dbirth']);?>">
    </div>
  </div>
  <div class="form-group">
    <label for="idate4" class="col-sm-2 control-label">Marriage Date</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="datepicker2" name="idate4" placeholder="Marriage Date" value="<?=trim($selmem['dwedding']);?>">
    </div>
  </div>
  <div class="form-group">
    <label for="hobby" class="col-sm-2 control-label">Hobby</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="hobby" name="hobby" placeholder="Hobby" value="<?=trim($selmem['hobby1']);?>">
    </div>
  </div>
  <div class="form-group">
    <label for="remark" class="col-sm-2 control-label">Remark</label>
    <div class="col-sm-10">
      <textarea class="form-control" rows="3" id="remark" name="remark" placeholder="Remark"><?=trim($selmem['remark']);?></textarea>
    </div>
  </div>
  <legend>Company Detail</legend>
  <div class="form-group">
    <label for="cname" class="col-sm-2 control-label">Company Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="cname" name="cname" placeholder="Company Name" value="<?=trim($selmem['company']);?>">
    </div>
  </div>
  <div class="form-group">
    <label for="caddress" class="col-sm-2 control-label">Company Address</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="caddress" name="caddress" placeholder="Company Address" value="<?=trim($selmem['caddress1']);?>">
    </div>
  </div>
  <div class="form-group">
    <label for="ccity" class="col-sm-2 control-label">City / Area / Postal</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="ccity" placeholder="City" value="<?=trim($selmem['ccity']);?>">
    </div>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="carea" placeholder="Area" value="<?=trim($selmem['carea']);?>">
    </div>
    <div class="col-sm-2">
      <input type="text" class="form-control" id="cpost" placeholder="Postal Code" value="<?=trim($selmem['cpostcode']);?>">
    </div>
  </div>
  <div class="form-group">
    <label for="ccountry" class="col-sm-2 control-label">Country</label>
    <div class="col-sm-10">
    <select class="form-control" id="ccountry" name="ccountry">
	<?php include "./themes/newqueens/inc/inc_country.php"; ?>
    </select>
    </div>
  </div>
  <div class="form-group">
    <label for="cphone" class="col-sm-2 control-label">Phone / Fax</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="cphone" name="cphone" placeholder="Phone Number" value="<?=trim($selmem['cphone1']);?>">
    </div>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="cfax" name="cfax" placeholder="Fax Number" value="<?=trim($selmem['cfax1']);?>">
    </div>
  </div>
  <div class="form-group">
    <label for="cposition" class="col-sm-2 control-label">Position</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="cposition" name="cposition" placeholder="Position" value="<?=trim($selmem['occupation']);?>">
    </div>
  </div>
  <div class="form-group">
    <label for="cmail" class="col-sm-2 control-label">Email Address</label>
    <div class="col-sm-10">
      <input type="email" class="form-control" id="cmail" name="cmail" placeholder="Email Address" value="<?=trim($selmem['cmail1']);?>" required>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
    <img src="<?php $theme->linkurl('../../captcha/captcha.php'); ?>?rand=<?php echo rand(); ?>" id='captchaimg' ><br />
    <small>Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh</small>
    </div>
  </div>
  <div class="form-group">
    <label for="codevalid" class="col-sm-2 control-label">Verify Code</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="codevalid" name="codevalid" placeholder="Verify Code" required>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
        <label>
          <input name="subscribe" id="subscribe" type="checkbox"> Subscribe to our Mailing List
        </label>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" name="rsubmit" class="btn btn-warning">Update Data</button>
    </div>
  </div>
  </fieldset>
</form>

<?php
	$dt=date("y");
	$kode=no_auto("guesti",'GIBLQTR-'.$dt);
	$datein=date("Y-m-d");
	$timein=date("H:m:i");
	$title=$_POST['title'];
	$fname=$_POST['fname'];
	$lname=$_POST['lname'];
	$sex=$_POST['sex'];
	$nationality=$_POST['nationality'];
	$religion=$_POST['religion'];
	$haddress=$_POST['haddress'];
	$harea=$_POST['harea'];
	$hpost=$_POST['hpost'];
	$hcity=$_POST['hcity'];
	$hcountry=$_POST['hcountry'];
	$hphone=$_POST['hphone'];
	$hfax=$_POST['hfax'];
	$hmobile=$_POST['hmobile'];
	$hmail=$_POST['hmail'];
	$remark=$_POST['remark'];
	$web=$_POST['hweb'];
	$kl_guest='Personal';
	
	$cname=$_POST['cname'];
	$cposition=$_POST['cposition'];
	$caddress=$_POST['caddress'];
	$carea=$_POST['carea'];
	$cpost=$_POST['cpost'];
	$ccity=$_POST['ccity'];
	$ccountry=$_POST['ccountry'];
	$cphone=$_POST['cphone'];
	$cfax=$_POST['cfax'];
	$cmail=$_POST['cmail'];
	$cnamepos=$cname.' | '.$cposition;
	
	$birthplace=$_POST['birthplace'];
	$birthdate=$_POST['idate3'];
	$marriage=$_POST['idate4'];
	$hobby=$_POST['hobby'];
	
	$read=$_POST['read'];
	$subscribe=$_POST['subscribe'];
	$captcha=$_POST['codevalid'];
	
	if(isset($_POST['rsubmit'])){
		if($_SESSION['6_letters_code']!=$captcha){
			echo'<script type="text/javascript">alert("Code validation not valid..");</script>';
		}
		else {
			$selcust=mysql_query("SELECT kode FROM guesti WHERE kode='$idcustomer'");
			$checkcust=mysql_num_rows($selcust);
			if($checkcust==0){
				include "./themes/function/random_code.php";
				$save=mysql_query("INSERT INTO guesti(kode, datein, fname, title, lname, pbirth, dbirth, national, religion, sex, haddress1, hcity, hpostcode, harea, hcountry, hphone1, hfax1, hp1, mail1, aktif, hobby1, dwedding, company, caddress1, ccity, cpostcode, carea, ccountry, cphone1, cfax1, cmail1, dentry, remark, web_site, kl_guest, okveri, mpl_mde, mpl_upd, mdt_upd, mtm_upd, subscribe, codever, gstatus) VALUES ('$kode','$datein','$fname','$title','$lname','$birthplace','$birthdate','$nationality','$religion','$sex','$haddress','$hcity','$hpost','$harea','$hcountry','$hphone','$hfax','$hmobile','$hmail','T','$hobby','$marriage','$cnamepos','$caddress','$ccity','$cpost','$carea','$ccountry','$cphone','$cfax','$cmail','$datein','$remark','$web','$kl_guest','F','BLQTR','BLQTR','$datein','$timein','$subscribe','$codever','0')");
			}else {
				$update=mysql_query("UPDATE guesti SET fname='$fname',title='$title',lname='$lname',pbirth='$birthplace',dbirth='$birthdate',national='$nationality',religion='$religion',sex='$sex',haddress1='$haddress',hcity='$hcity',hpostcode='$hpost',harea='$harea',hcountry='$hcountry',hphone1='$hphone',hfax1='$hfax',hp1='$hmobile',mail1='$hmail',aktif='T',hobby1='$hobby',dwedding='$marriage',company='$cnamepos',caddress1='$caddress',ccity='$ccity',cpostcode='$cpost',carea='$carea',ccountry='$ccountry',cphone1='$cphone',cfax1='$cfax',cmail1='$cmail',web_site='$web',subscribe='$subscribe' WHERE kode='$idcustomer'");
			}
			
			if($save||$update){
				echo'<script type="text/javascript">alert("Success, your data has been saved..");</script>';
			}else{
				echo'<script type="text/javascript">alert("Error, your data cannot been save..");</script>';
			}
		}
	}
?>