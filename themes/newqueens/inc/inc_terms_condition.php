<style type="text/css">
ol {
	margin-left:40px;
}
</style>
<div class="row">
	<div class="col-md-12">
    <p><strong>1.<u>VALIDITY POLICY</u>:</strong></p>
    <ol>
      <li>Valid from 15JAN, 2013 until 15JAN,  2014, unless earlier terminated by either party,</li>
    </ol>
    <p><strong>2.<u>HIGH SEASON POLICY:</u></strong></p>
    <ol>
      <li>Extra SURCHARGE Rp.25,000 net/pax  during PEAK SEASON, for FIT &amp; GROUP in-house guests</li>
      <li>Peak Season period for 2013 are  (7Aug-11Aug, 20Dec-5Jan),</li>
    </ol>
    <p><strong>3.<u>BOOKING POLICY:</u></strong></p>
    <ol>
      <li>Booking should be done by fully  filled BOOKING FORM, send by Fax/Mail/Online,</li>
      <li>Package/Buffet/Set Menu cannot be  combined or joined with any other product/offer/promotion,</li>
      <li>Booking by fully filled BOOKING  FORM, which needed by Fax/Mail/Online,</li>
      <li>Guaranteed Letter from the AGENT in  form of Voucher during dining/meal,</li>
    </ol>
    <p><strong>4.<u>GUEST COUNTS/GUARANTEED PAX</u></strong><u>:</u></p>
    <ol>
      <li>Charges will be according to the <u>guaranteed  number or actual attendance</u>, whichever is greater</li>
      <li>A minimum guarantee number of pax  should be received by <u>24hours of the events</u>, no changes on the number of  pax can be made after this duration</li>
      <li>If the guaranteed <u>guest counts  falls/reduce below 25 %</u> of the proposal guest count, the price per guest  may vary</li>
      <li>Package Code No: V1-V3, NV1-NV6,  VJ1-VJ3, PK1-PK4, BG, HN ONLY can be booked only with minimum 2persons for the  same package &amp; menu,</li>
      <li>Package Code No: BQ1-BQ4, GD1-GD6,  MC1-MC6, HS1-HS6, CKS, CKB, HT, HTW can be booked only with minimum persons for  the same package &amp; menu with target revenue.</li>
    </ol>
    <p><strong>5.<u>BILLING POLICY:</u></strong> <br>
      a. <u>CASH  BASIS:</u></p>
    <ol>
      <li>Payment in net cash amount to  cashier for cash basis contract,</li>
      <li>Advance Bank transfer to BANK A/C  No:  77 00 126 255,<br />
      BANK: BCA-Kantor  Cabang Pembantu-Seminyak-Kuta-Bali, <br />
      BANK NAME: RAMESH  KUNDANMAL S, SWIFT CODE: CENAIDDJA
      </li>
      <li>Free of bank commission or charges  imposed by any bank.</li>
      <li>Copy of each payment slip to be  faxed to Account Dept,</li>
      <li>Payment through Credit Card add 3%  additional,</li>
    </ol>
    <p>b. <u>CREDIT BASIS:</u></p>
    <ol>
      <li>Credit Request Letter proposing  credit facility should be made, attention to Finance Department   with submission of credit facility form and  other supporting documents,</li>
      <li>Process of approval will be fully  under Queen's management,</li>
      <li>Voucher/Credit Facility is only  valid when CREDIT FACILITY AGREEMENT has been approved,</li>
    </ol>
    <p><strong><u>6. PAYMENT POLICY</u></strong></p>
    <ol>
      <li>Payment should be made as per  payment policy during contract.</li>
      <li>Copy of payment slip should be<u> faxed to Sales Dept </u>(62-361-732771) or mail to bali@queenstandoor.com</li>
      <li>Payment by bank should be <u>Free of  bank commission</u> or charges imposed by any bank.</li>
      <li>Final Invoice will be based on <u>guaranteed  booked guests</u> or a<u>ctual attendance</u>, whichever is the greater,</li>
      <li>Payment should be in <u>CASH in  advance </u>OR <u>CREDIT CARD </u>than with additional <u>3% </u>Bank Charges  OR  by <u>BANK REMITTANCE/transfer</u>.</li>
      <li><u>40 % of total estimated</u> budget to be settled prior to signing the Catering  Confirmation Letter/Preliminary Invoice to <u>CONFIRM</u> the bookings.<u></u></li>
      <li>5<u>0%</u> to be settled 2days  before <u>the function start</u> based on the Premliminary Invoice.</li>
      <li>Balance 1<u>0%</u> to be settled  immediately after <u>function finish based </u>on the Final Invoice.</li>
    </ol>
    <p><strong><u>7.DEPOSIT FUNDS on cancellations</u></strong> are handled as follows:-</p>
    <ol>
      <li><u>CANCELLATION  POLICY FOR FIT</u>:(2-10pax)</li>
      <ol>
        <ol>
          <li>Cancellation 1day before booking date = NIL,</li>
          <li>Cancellation same day of event = 40% Charges from Booking  amount.</li>
        </ol>
      </ol>
    </ol>
    <ol>
      <li><u>CANCELLATION  POLICY FOR GROUPS:</u> (10pax &amp; above)</li>
      <ol>
        <ol>
          <li>Cancellation 5days before event date = NIL</li>
          <li>Cancellation 2-4days before event date = 20% Charges of  total booking amount</li>
          <li>Cancellation before 24hrs-48hrs before event date= 40%  Charges of total booking amount</li>
          <li>Cancellation less than 24hrs   = 100% Charges of total booking amount</li>
        </ol>
      </ol>
    </ol>
    <p><strong><u>8. NO SHOW POLICY</u></strong></p>
    <ol>
        <li>AGENT will be charged at full amount  of booking in case of No Show</li>
        <li>No Show charges will be calculated  based on total booking</li>
    </ol>
    <p><strong>9.<u>BUFFET SERVICE POLICY:</u></strong></p>
    <ol>
      <li>Min. 25pax or 2,500,000 total  Invoice, whichever is greater is needed to get Buffet Service,</li>
      <li>If confirm booking less than  25pax,than Set Menu/Table Service/Family Service will be apply.</li>
    </ol>
    <p><strong>10.<u>FREE OF CHARGE (FOC) POLICY</u></strong>:</p>
    <ol>
      <li>Every 30TH Guest will be FOC, total  30guests, 30 will be paying &amp; 1will be free of cost,<br />
        &nbsp;&nbsp;&nbsp;&nbsp;EX:  31=30+1FOC, 62=60+2FOC etc.
      </li>
    </ol>
    <p><strong>11.<u>CHILD POLICY</u></strong>:</p>
    <ol>
      <li>Children below 5years will be FOC,</li>
      <li>5-10YEARS old Child = 50% discount,</li>
      <li>10 years old child &amp; above that  age will be FULL CHARGE/ADULT PRICE</li>
    </ol>
    <p><strong>12.&nbsp;<u>OUTSIDE VENUE POLICY</u></strong> (Rented Venue or Hotel or Villa):</p>
    <ol>
      <li>These Packages &amp; Policies are  valid at Queen's Premises only,</li>
      <li>Outside Party will have different  terms &amp; policies (depending on individual policy)</li>
      <li>Any cost occurred by outside party  will be additional cost paid by Organizer/Host/Agent.</li>
    </ol>
    <p><strong>13.<u>PRIVATE BOOKING</u></strong> <strong><u>POLICY:</u></strong></p>
    <ol>
      <li>QT,Seminyak (All the 3Private Rooms  together=2,500,000, Each Private Room individual=  750.000, Each Gazebo=750,000)</li>
      <li>QOI, Kuta (Private hall on 2nd Floor  = 7,500,000)</li>
      <li>QOI, Benoa-Nusa Dua (Private hall on  2nd Floor = 3,000,000)</li>
      <li>QOI, Ubud (Private hall on 2nd Floor  = 3,000,000)</li>
    </ol>
    <p><strong>14.&nbsp;<u>SERVICE STYLE POLICY</u></strong> For FIT (below10pax):</p>
    <ol>
      <li>Food will be served only after  guests have selected from the list of menu items based on the package ordered.</li>
    </ol>
    <p><strong>15.&nbsp;<u>OUTSIDE CATERING</u></strong>:</p>
    <ol>
      <li>Package can be use for outside  catering with Min. 25pax or 2,500,000 Below this, delivery services will apply.</li>
    </ol>
    <p><strong>16.<u>ADDITIONAL SERVICE POLICIES</u></strong>:</p>
    <ol>
      <li>These packages can also be apply for  take away services with additional Rp.5,100/pax (cost of packaging)</li>
      <li>one time delivery services with  Rp.10,200/time</li>
    </ol>
    <p><strong>17.<u>EQUIPMENT FOR OUTSIDE CATERING (Only Include):</u></strong></p>
    <ol>
      <li>CUTLERIES : Dinner Fork, Dinner Spoon, Dinner Knife, Soup Spoon,  Dessert Spoon, Service Spoon</li>
      <li>CHINNAWARE : Dinner Plate, Dessert Plate, B &amp; B Plate, Bowl, Serving  Bowl</li>
      <li>LINEN : Buffet Table Cloth, Buffet Table Skirting</li>
      <li>BUFFET : Buffet Table</li>
      <li>GLASSES : Whisky Glass, Juice/Soft Drink Glasses</li>
    </ol>
	</div>
</div>