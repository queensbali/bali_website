<div class="row">
	<div class="col-md-12">
    <?php
		session_start();
		$sessionID = $_COOKIE['PHPSESSID'];
		$carts=mysql_query("SELECT * FROM tb_carts WHERE cart_session='$sessionID' AND type='Meal Voucher'");
		$cekcarts=mysql_num_rows($carts);
		if($cekcarts==0){
	?>
    <div style="margin:50px 0;">
    <center>
    <img src="<?php $theme->linkurl("img/"); echo'empty.gif'; ?>" height="200" /><br />
    <a href="<?php $theme->linkurl("../../gift-voucher"); ?>">Click here to continue shopping..</a>
    </center>
    </div>
    <?php } else { ?>
    <div class="table-responsive">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-hover">
      <thead>
      <tr>
        <th width="5%" bgcolor="#FFCC00">&nbsp;</th>
        <th width="52%" bgcolor="#FFCC00">Product Name</th>
        <th width="14%" bgcolor="#FFCC00">Unit Price</th>
        <th width="7%" bgcolor="#FFCC00">QTY</th>
        <th width="14%" bgcolor="#FFCC00">Sub Total</th>
        <th width="8%" bgcolor="#FFCC00">Remove</th>
      </tr>
      </thead>
      <tbody>
      <?php
		$selcart = mysql_query("SELECT * FROM tb_carts WHERE tb_carts.cart_session='".$sessionID."' AND type='Meal Voucher' ORDER BY tb_carts.id_cart DESC");
		$subtot=0;
		$tax=0;
		$service=0;
		$gtotal=0;
		while($vwcart=mysql_fetch_array($selcart)){
			$subtot=($subtot+$vwcart['price']*$vwcart['qty']);
			$tax=$subtot*10/100;
			$service=$subtot*5/100;
			$gtotal=$subtot+$tax+$service;
	  ?>
      <tr>
        <td>
        <img src="<?php $theme->linkurl('../upload/meal-voucher.png'); ?>" height="50">
        </td>
        <td><?php echo $vwcart['type'].' - '.$vwcart['specreq']; ?></td>
        <td>Rp. <span class="pull-right"><?php echo number_format($vwcart['price'],2,",","."); ?></span></td>
        <td>
        <!--<input name="price" type="text" id="price" value="<?php //echo $vwcart['price']; ?>" />-->
        <input name="idcart" id="idcart" class="idcart" type="hidden" value="<?php echo $vwcart['id_cart']; ?>" />
        <input name="qty" type="text" id="qty" size="5" value="<?php echo $vwcart['qty']; ?>" class="qty">
       <!-- <input name="subtotal" type="text" id="subtotal" value="<?php //echo $vwcart['price']*$vwcart['qty']; ?>" />-->
        </td>
        <td>Rp. <span class="pull-right"><?php echo number_format($vwcart['price']*$vwcart['qty'],2,",",".");?></span>
        </td>
        <td align="center">
        <a href="#" class="deleteLink" id="<?php echo $vwcart['id_cart']; ?>" title="Delete cart"><span class="glyphicon glyphicon-remove"></span></a>
        </td>
      </tr>
      <?php } ?>
      </tbody>
    </table>
    </div><hr>
    <div style="margin-bottom:10px;">
    <span class="pull-right">Sub Total : <?php echo 'Rp. '.number_format($subtot,2,",","."); ?></span><br>
    <span class="pull-right">Shipping Fee : <?php echo 'Rp. '.number_format($tax+$service,2,",","."); ?></span><br>
	<span class="pull-right"><strong>Grand Total : <?php echo 'Rp. '.number_format($gtotal,2,",","."); ?></strong></span><br>
    </div>
    <button type="button" class="btn btn-default pull-left" onclick="history.go(-1);">&larr; Continue shopping</button>
    <button type="button" class="btn btn-danger pull-right" style="margin-left:10px;" onclick="window.location='<?php $theme->linkurl('../../checkout/register'); ?>';">Proceed to Checkout &rarr;</button>
    <button type="button" class="btn btn-danger pull-right" style="margin-left:10px;" id="updateButton">Update carts</button>
    <?php } ?>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$(".deleteLink").click(function(){
			var del_id = $(this).attr('id');
			var data = 'id='+del_id;
			var parent = $(this).parent().parent();
			
			if(confirm("Sure you want to delete this ID : "+del_id)){
				$.ajax({
					type: "POST",
					url: "<?php $theme->linkurl('inc/inc_delete_cart.php'); ?>",
					data: data,
					cache: false,
					success: function(msg){ 
						alert(msg);
						parent.fadeOut('slow', function() {$(this).remove();});
						location.reload();
					}
				});
			}
			return false;
		});
		
		$("#updateButton").click(function(){
			var updtid = document.getElementsByClassName('idcart');
			var qty = document.getElementsByClassName('qty');
				
				for(i=0; i<updtid.length; i++) {
					$.ajax({
						type: "POST",
						url: "<?php $theme->linkurl('inc/inc_update_cart.php'); ?>",
						data: 'id[]='+updtid[i].value+'&qty[]='+qty[i].value,
						cache: false,
						success: function(msg){
							location.reload();
						}
					});
				}
		});
	});
</script>