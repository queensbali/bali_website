<strong>AT QUEEN'S TANDOOR WE ARE COMMITTED TO DELIVER MORE THAN WHAT YOU EXPECT.</strong><br><br>

Since your comments and observations are important to us, we'd appreciate it if you could take a moment to fill out this brief survey. Your suggestions will help us to provide the very best in food quality and service.<br><br>

We welcome all of your comments and suggestions. You can also use these forms for other type's of feedback. Tell us what you think about our web site, our products, our organization, or anything else that comes to mind.<br><br>

If your comment or question involves a particular outlet, please enter as much information as possible about that location below.<br><br>

<div style="background-color:#CCC; font-weight:bold; margin-top:10px; padding:5px; color:#FFFFFF;">
Questionnaire Form
</div>
<form method="post" class="form-horizontal" role="form">
<div class="quiz">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped table-bordered">
  <thead>
  <tr>
    <th width="40%">&nbsp;</th>
    <th width="16%"><strong>Excellent</strong></th>
    <th width="15%"><strong>Good</strong></th>
    <th width="14%"><strong>Average</strong></th>
    <th width="15%"><strong>Poor</strong></th>
  </tr>
  </thead>
  <tbody>
  <tr class="danger">
    <td><strong>FOOD</strong></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p>&nbsp;&nbsp;&rarr; Presentation</p></td>
    <td><input type="radio" name="presentation" id="radio" value="Excellent"></td>
    <td><input type="radio" name="presentation" id="radio2" value="Good"></td>
    <td><input type="radio" name="presentation" id="radio3" value="Average"></td>
    <td><input type="radio" name="presentation" id="radio4" value="Poor"></td>
  </tr>
  <tr>
    <td><p>&nbsp;&nbsp;&rarr; Taste</p></td>
    <td><input type="radio" name="taste" id="radio8" value="Excellent"></td>
    <td><input type="radio" name="taste" id="radio7" value="Good"></td>
    <td><input type="radio" name="taste" id="radio6" value="Average"></td>
    <td><input type="radio" name="taste" id="radio5" value="Poor"></td>
  </tr>
  <tr>
    <td><p>&nbsp;&nbsp;&rarr; Temperature</p></td>
    <td><input type="radio" name="temperature_1" id="radio9" value="Excellent"></td>
    <td><input type="radio" name="temperature_1" id="radio13" value="Good"></td>
    <td><input type="radio" name="temperature_1" id="radio17" value="Average"></td>
    <td><input type="radio" name="temperature_1" id="radio21" value="Poor"></td>
  </tr>
  <tr>
    <td><p>&nbsp;&nbsp;&rarr; Portion Size</p></td>
    <td><input type="radio" name="portion" id="radio10" value="Excellent"></td>
    <td><input type="radio" name="portion" id="radio14" value="Good"></td>
    <td><input type="radio" name="portion" id="radio18" value="Average"></td>
    <td><input type="radio" name="portion" id="radio22" value="Poor"></td>
  </tr>
  <tr>
    <td><p>&nbsp;&nbsp;&rarr; Quality</p></td>
    <td><input type="radio" name="quality" id="radio11" value="Excellent"></td>
    <td><input type="radio" name="quality" id="radio15" value="Good"></td>
    <td><input type="radio" name="quality" id="radio19" value="Average"></td>
    <td><input type="radio" name="quality" id="radio23" value="Poor"></td>
  </tr>
  <tr>
    <td><p>&nbsp;&nbsp;&rarr; Menu Selection</p></td>
    <td><input type="radio" name="menu" id="radio12" value="Excellent"></td>
    <td><input type="radio" name="menu" id="radio16" value="Good"></td>
    <td><input type="radio" name="menu" id="radio20" value="Average"></td>
    <td><input type="radio" name="menu" id="radio24" value="Poor"></td>
  </tr>
  <tr class="danger">
    <td><strong>SERVICES</strong></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p>&nbsp;&nbsp;&rarr; Greeting</p></td>
    <td><input type="radio" name="greeting" id="radio25" value="Excellent"></td>
    <td><input type="radio" name="greeting" id="radio26" value="Good"></td>
    <td><input type="radio" name="greeting" id="radio27" value="Average"></td>
    <td><input type="radio" name="greeting" id="radio28" value="Poor"></td>
  </tr>
  <tr>
    <td><p>&nbsp;&nbsp;&rarr; Politeness</p></td>
    <td><input type="radio" name="politeness" id="radio29" value="Excellent"></td>
    <td><input type="radio" name="politeness" id="radio30" value="Good"></td>
    <td><input type="radio" name="politeness" id="radio31" value="Average"></td>
    <td><input type="radio" name="politeness" id="radio32" value="Poor"></td>
  </tr>
  <tr>
    <td><p>&nbsp;&nbsp;&rarr; Promptness</p></td>
    <td><input type="radio" name="promptness" id="radio36" value="Excellent"></td>
    <td><input type="radio" name="promptness" id="radio35" value="Good"></td>
    <td><input type="radio" name="promptness" id="radio34" value="Average"></td>
    <td><input type="radio" name="promptness" id="radio33" value="Poor"></td>
  </tr>
  <tr>
    <td><p>&nbsp;&nbsp;&rarr; Attire</p></td>
    <td><input type="radio" name="attire" id="radio37" value="Excellent"></td>
    <td><input type="radio" name="attire" id="radio38" value="Good"></td>
    <td><input type="radio" name="attire" id="radio39" value="Average"></td>
    <td><input type="radio" name="attire" id="radio40" value="Poor"></td>
  </tr>
  <tr class="danger">
    <td> <strong>ATMOSPHERE</strong></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p>&nbsp;&nbsp;&rarr; Comfortable</p></td>
    <td><input type="radio" name="comfortable" id="radio41" value="Excellent"></td>
    <td><input type="radio" name="comfortable" id="radio42" value="Good"></td>
    <td><input type="radio" name="comfortable" id="radio43" value="Average"></td>
    <td><input type="radio" name="comfortable" id="radio44" value="Poor"></td>
  </tr>
  <tr>
    <td><p>&nbsp;&nbsp;&rarr; Cleanliness</p></td>
    <td><input type="radio" name="cleanliness" id="radio47" value="Excellent"></td>
    <td><input type="radio" name="cleanliness" id="radio48" value="Good"></td>
    <td><input type="radio" name="cleanliness" id="radio46" value="Average"></td>
    <td><input type="radio" name="cleanliness" id="radio45" value="Poor"></td>
  </tr>
  <tr>
    <td><p>&nbsp;&nbsp;&rarr; Music</p></td>
    <td><input type="radio" name="music" id="radio49" value="Excellent"></td>
    <td><input type="radio" name="music" id="radio50" value="Good"></td>
    <td><input type="radio" name="music" id="radio51" value="Average"></td>
    <td><input type="radio" name="music" id="radio52" value="Poor"></td>
  </tr>
  <tr>
    <td><p>&nbsp;&nbsp;&rarr; Temperature</p></td>
    <td><input type="radio" name="temperature_2" id="radio56" value="Excellent"></td>
    <td><input type="radio" name="temperature_2" id="radio55" value="Good"></td>
    <td><input type="radio" name="temperature_2" id="radio54" value="Average"></td>
    <td><input type="radio" name="temperature_2" id="radio53" value="Poor"></td>
  </tr>
  <tr class="danger">
    <td><strong>OVERALL RATING</strong></td>
    <td><input type="radio" name="rating" id="radio57" value="Excellent" /></td>
    <td><input type="radio" name="rating" id="radio58" value="Good" /></td>
    <td><input type="radio" name="rating" id="radio59" value="Average" /></td>
    <td><input type="radio" name="rating" id="radio60" value="Poor" /></td>
  </tr>
  </tbody>
</table>
</div>
<div style="background-color:#CCC; font-weight:bold; margin-top:10px; margin-bottom:10px; padding:5px; color:#FFFFFF;">
Other Information
</div>
<div class="quiz2">
  <div class="form-group">
    <label for="inputName" class="col-sm-4 control-label">1. How did you hear about us?</label>
    <div class="col-sm-8">
      <select name="quis_1" id="quis_1" class="form-control">
      	<option value="Friends / Family / Bussiness Association">Friends / Family / Bussiness Association</option>
      	<option value="Advertisement">Advertisement</option>
      	<option value="Internet">Internet</option>
      	<option value="Other">Other</option>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="inputName" class="col-sm-4 control-label">2. How often do you come to Queen's Tandoor?</label>
    <div class="col-sm-8">
      <select name="quis_2" id="quis_2" class="form-control">
      	<option value="First Visit">First Visit</option>
      	<option value="Weekly">Weekly</option>
      	<option value="Monthly">Monthly</option>
      	<option value="Less Often">Less Often</option>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="inputName" class="col-sm-4 control-label">3. What items did you order?</label>
    <div class="col-sm-8">
      <input type="text" name="quis_3" id="quis_3" class="form-control">
    </div>
  </div>
  <div class="form-group">
    <label for="inputName" class="col-sm-4 control-label">4. What did you like the best about our service?</label>
    <div class="col-sm-8">
      <input type="text" name="quis_4" id="quis_4" class="form-control">
    </div>
  </div>
  <div class="form-group">
    <label for="inputName" class="col-sm-4 control-label">5. If we are not open today, which restaurant will you go to?</label>
    <div class="col-sm-8">
      <input type="text" name="quis_5" id="quis_5" class="form-control">
    </div>
  </div>
  <div class="form-group">
    <label for="inputName" class="col-sm-4 control-label">6. Other comments or suggestions?</label>
    <div class="col-sm-8">
      <textarea name="quis_6" id="quis_6" cols="45" rows="5" class="form-control"></textarea>
    </div>
  </div>
</div>
<div style="background-color:#CCC; font-weight:bold; margin-top:10px; margin-bottom:5px; padding:5px; color:#FFFFFF;">
Information About You
</div>
<div class="quiz3">
<p>To ensure a timely response, please fill out as much information as possible on the form below. Fields noted by an '*' 	are mandatory and must be filled for the form to be processed.</p>
<p>Customer complaints or feedback concerning specific restaurants are forwarded to the owner(s) of the Queen's Tandoor, Indian restaurant. We do not provide your information to third parties.</p>
  <div class="form-group">
    <label for="inputName" class="col-sm-4 control-label">Your Fullname</label>
    <div class="col-sm-8">
      <input type="text" name="fullname" id="fullname" class="form-control" required>
    </div>
  </div>
  <div class="form-group">
    <label for="inputName" class="col-sm-4 control-label">Company</label>
    <div class="col-sm-8">
      <input type="text" name="company" id="company" class="form-control">
    </div>
  </div>
  <div class="form-group">
    <label for="inputName" class="col-sm-4 control-label">E-mail Address</label>
    <div class="col-sm-8">
      <input type="text" name="mail" id="mail" class="form-control" required>
    </div>
  </div>
  <div class="form-group">
    <label for="inputName" class="col-sm-4 control-label">Phone Number (Include Area Code)</label>
    <div class="col-sm-8">
      <input type="text" name="phone" id="phone" class="form-control">
    </div>
  </div>
  <div class="form-group">
    <label for="inputName" class="col-sm-4 control-label">Fax Number (Include Area Code)</label>
    <div class="col-sm-8">
      <input type="text" name="fax" id="fax" class="form-control">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-4 col-sm-8">
      <div class="checkbox">
        <label>
          <input type="checkbox" name="subscribe" value="Yes"> Subscribe to our mailing list
        </label>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-4 col-sm-8">
      <button type="submit" name="subquis" id="subquis" class="btn btn-warning">Submit Form</button>
      <button type="reset" name="clear" class="btn btn-default">Clear</button>
    </div>
  </div>

  <?php
	if(isset($_POST['subquis'])){
		$presentation=$_POST['presentation'];
		$taste=$_POST['taste'];
		$temperature_1=$_POST['temperature_1'];
		$portion=$_POST['portion'];
		$quality=$_POST['quality'];
		$menu=$_POST['menu'];
		$greeting=$_POST['greeting'];
		$politeness=$_POST['politeness'];
		$promptness=$_POST['promptness'];
		$attire=$_POST['attire'];
		$comfortable=$_POST['comfortable'];
		$cleanliness=$_POST['cleanliness'];
		$music=$_POST['music'];
		$temperature_2=$_POST['temperature_2'];
		$rating=$_POST['rating'];
		
		$quis_1=$_POST['quis_1'];
		$quis_2=$_POST['quis_2'];
		$quis_3=$_POST['quis_3'];
		$quis_4=$_POST['quis_4'];
		$quis_5=$_POST['quis_5'];
		$quis_6=$_POST['quis_6'];
		
		$fullname=$_POST['fullname'];
		$company=$_POST['company'];
		$mail=$_POST['mail'];
		$phone=$_POST['phone'];
		$fax=$_POST['fax'];
		$captcha=$_POST['captcha'];
		$subscribe=$_POST['subscribe'];
		$datein=date("Y-m-d");
	
		$dt=date("Y");
		$id=no_auto("tb_questionnaire",'QZ'.$dt);
		$save=mysql_query("INSERT INTO tb_questionnaire(id_quiz,datein,fullname,company,mail,phone,fax,presentation,taste,temperature_1,portion_size,quality,menu,greeting,politeness,promptness,attire,comfortable,cleanliness,music,temperature_2,overal_rating,quiz_1,quiz_2,quiz_3,quiz_4,quiz_5,quiz_6,subscribe,publish)VALUES('$id','$datein','$fullname','$company','$mail','$phone','$fax','$presentation','$taste','$temperature_1','$portion','$quality','$menu','$greeting','$politeness','$promptness','$attire','$comfortable','$cleanliness','$music','$temperature_2','$rating','$quis_1','$quis_2','$quis_3','$quis_4','$quis_5','$quis_6','$subscribe','No')");
		
		require_once($theme->incurl('../../').'./themes/plugin/phpmailer/class.phpmailer.php');
		require_once($theme->incurl('../../').'./themes/plugin/phpmailer/class.smtp.php'); 
		$mail = new PHPMailer();		
		//SMTP Class
		$mail->IsSMTP(); //set untuk menggunakan SMTP
		$mail->SMTPDebug  = 1;                     
		//SMTP Setting
		$mail->SMTPAuth   = true;                  
		$mail->SMTPSecure = "tls";                 
		$mail->Host       = "smtp.gmail.com";      
		$mail->Port       = 587; 
		$mail->Username   = "noreply@queenstandoor.com";
		$mail->Password   = "queens1976"; 
		//Email From
		$mail->SetFrom("noreply@queenstandoor.com", "noreply@queenstandoor.com");
		$mail->AddReplyTo("noreply@queenstandoor.com","noreply@queenstandoor.com");
		$mail->Subject='Queens Questionnaire';
		$mail->AltBody="To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
		//Email Letter
		ob_start();
		require_once($theme->incurl('../../').'./themes/email/questionnaire.php');
		$html_message = ob_get_contents();
		ob_end_clean();
		//Email HTML Content
		$mail->MsgHTML($html_message);
		$mail->IsHTML(true); 
		//Email To
		$mail->AddAddress($mail, $fullname);
		/*$mail->AddCC('puneet@queenstandoor.com', 'Puneet Malhotra');
		$mail->AddCC('bali@queenstandoor.com', 'Queens Bali');*/
		$mail->AddBCC('jonny_hds@yahoo.com','Jonny Cible');
		if(!$mail->Send()) {
		  echo "Mailer Error: " . $mail->ErrorInfo;
		} else { }
		
		if($save){
			echo '<script type="text/javascript">alert("Success, Your comment has been saved..");</script>';
		}else {
			echo '<script type="text/javascript">alert("Error, Please retry again..");</script>';
		}
	}
  ?>
</div>
</form>