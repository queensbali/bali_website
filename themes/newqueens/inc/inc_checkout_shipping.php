<script type="text/javascript">
	$(document).ready(function() {
		$(":checkbox").click(function() {
		 	var fname = $('#rowfname').val();
			var lname = $('#rowlname').val();
			var haddress = $('#rowhaddress').val();
			var hcity = $('#rowhcity').val();
			var harea = $('#rowharea').val();
			var hpost = $('#rowhpost').val();
			var hphone = $('#rowhphone').val();
			var hmail = $('#rowhmail').val();
		 	
			$("#fname").val(fname); $("#lname").val(lname); $("#haddress").val(haddress); $("#hcity").val(hcity);
			$("#harea").val(harea); $("#hpost").val(hpost); $("#hphone").val(hphone); $("#hmail").val(hmail);
		});
	});     
</script>
<form action="<?php $_SERVER['PHP_SELF']; ?>" method="post" class="form-horizontal" role="form">
  <fieldset>
  <legend>Shipping Information</legend>
  <div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
      <div class="checkbox">
        <label><input name="usebill" id="usebill" type="checkbox" value="YES"> Use billing address</label>
        <?php
			session_start();
			$sessionID = $_COOKIE['PHPSESSID'];
			$selbill=mysql_query("SELECT * FROM tb_carts_billing WHERE cart_session='$sessionID'");
			$rowbill=mysql_fetch_array($selbill);
		?>
            <input name="rowfname" type="hidden" id="rowfname" value="<?=$rowbill['fname'];?>" />
            <input name="rowlname" type="hidden" id="rowlname" value="<?=$rowbill['lname'];?>" />
            <input name="rowhaddress" type="hidden" id="rowhaddress" value="<?=$rowbill['haddress'];?>" />
            <input name="rowhcity" type="hidden" id="rowhcity" value="<?=$rowbill['hcity'];?>" />
            <input name="rowharea" type="hidden" id="rowharea" value="<?=$rowbill['harea'];?>" />
            <input name="rowhpost" type="hidden" id="rowhpost" value="<?=$rowbill['hcode'];?>" />
            <input name="rowhphone" type="hidden" id="rowhphone" value="<?=$rowbill['hmobile'];?>" />
            <input name="rowhmail" type="hidden" id="rowhmail" value="<?=$rowbill['email'];?>" />
      </div>
    </div>
  </div>
  <div class="form-group">
    <label for="title" class="col-md-3 control-label">Title</label>
    <div class="col-md-9">
    <select class="form-control required" name="title" id="title">
      <option value="">Select one</option>
      <option value="Mr.">Mr.</option>
      <option value="Ms.">Ms.</option>
      <option value="Mrs.">Mrs.</option>
    </select>
    </div>
  </div>
  <div class="form-group">
    <label for="title" class="col-sm-3 control-label">First / Last Name</label>
    <div class="col-sm-5">
    <input type="text" class="form-control" id="fname" placeholder="First Name" name="fname" required>
    </div>
    <div class="col-sm-4">
    <input type="text" class="form-control" id="lname" placeholder="Last Name" name="lname">
    </div>
  </div>
  <div class="form-group">
    <label for="haddress" class="col-sm-3 control-label">Home Address</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="haddress" name="haddress" placeholder="Home Address" required>
    </div>
  </div>
  <div class="form-group">
    <label for="hcity" class="col-sm-3 control-label">City / Area / Postal</label>
    <div class="col-sm-3">
      <input type="text" class="form-control" id="hcity" name="hcity" placeholder="City" required>
    </div>
    <div class="col-sm-3">
      <input type="text" class="form-control" id="harea" name="harea" placeholder="Area" required>
    </div>
    <div class="col-sm-3">
      <input type="text" class="form-control" id="hpost" name="hpost" placeholder="Postal Code">
    </div>
  </div>
  <div class="form-group">
    <label for="hcountry" class="col-sm-3 control-label">Country</label>
    <div class="col-sm-9">
    <select class="form-control" id="hcountry" name="hcountry">
	<?php include "./themes/newqueens/inc/inc_country.php"; ?>
    </select>
    </div>
  </div>
  <div class="form-group">
    <label for="hphone" class="col-sm-3 control-label">Phone / Mobile</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="hphone" name="hphone" placeholder="Phone Number">
    </div>
  </div>
  <div class="form-group">
    <label for="hmail" class="col-sm-3 control-label">Email Address</label>
    <div class="col-sm-9">
      <input type="email" class="form-control" id="hmail" name="hmail" placeholder="info@example.com" required>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
      <button type="submit" name="bsubmit" class="btn btn-danger pull-right">Continue &rarr;</button>
    </div>
  </div>
  <?php
  	if(isset($_POST['bsubmit'])){
		$title=$_POST['title'];
		$fname=$_POST['fname'];
		$lname=$_POST['lname'];
		$haddress=$_POST['haddress'];
		$hcity=$_POST['hcity'];
		$harea=$_POST['harea'];
		$hcode=$_POST['hpost'];
		$hcountry=$_POST['hcountry'];
		$hphone=$_POST['hphone'];
		$hmail=$_POST['hmail'];
		$datein=date('Y-m-d');
		$dt=date('ym');
		$idshipping=no_auto("tb_carts_shipping",'SHIP'.$dt);
		
		$save=mysql_query("INSERT INTO tb_carts_shipping(id_shipping, datein, cart_session, title, fname, lname, haddress, hcity, harea, hcode, hcountry, hphone, email) VALUES ('$idshipping','$datein','$sessionID','$title','$fname','$lname','$haddress','$hcity','$harea','$hcode','$hcountry','$hphone','$hmail')");
		$cart=mysql_query("UPDATE tb_carts_checkout SET id_shipping='$idshipping' WHERE cart_session='$sessionID'");
		if($save){
			echo'<script type="text/javascript">window.location="http://192.168.1.131/queens/checkout/register/method";</script>';
		}
	}
  ?>
  </fieldset>
</form>