<script language='JavaScript' type='text/javascript'>
	function refreshCaptcha(){
		var img = document.images['captchaimg'];
		img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
	}
</script>
<form class="form-horizontal" role="form" name="form1" method="post">
  <div class="form-group">
    <label for="inputName" class="col-sm-2 control-label">Fullname</label>
    <div class="col-sm-10">
      <input name="fullname" type="text" class="form-control" id="inputName" placeholder="Insert your fullname.." required>
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10">
      <input name="email" type="email" class="form-control" id="inputEmail3" placeholder="Insert your mail address.." required>
    </div>
  </div>
  <div class="form-group">
    <label for="inputSubject" class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      <strong>What kind of comment would you like to send ?</strong><br>
      <label class="radio-inline ">
      	<input type="radio" name="complaint" id="optionsRadios1" value="Compliment"> Compliment
      </label>
      <label class="radio-inline ">
      	<input type="radio" name="complaint" id="optionsRadios1" value="Suggestion" checked> Suggestion
      </label>
    </div>
  </div>
  <div class="form-group">
    <label for="inputMsg" class="col-sm-2 control-label">Message</label>
    <div class="col-sm-10">
    <textarea name="msg" rows="3" class="form-control" id="inputMsg" placeholder="Insert your message.." required></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
    <img src="<?php $theme->linkurl('../../captcha/captcha.php'); ?>?rand=<?php echo rand(); ?>" id='captchaimg' ><br />
    <small>Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh</small>
    </div>
  </div>
  <div class="form-group">
    <label for="inputSubject" class="col-sm-2 control-label">Verify Code</label>
    <div class="col-sm-10">
      <input name="vcode" type="text" class="form-control" id="inputSubject" placeholder="Insert validation code.." required>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
        <label>
          <input type="checkbox" name="subscribe" value="Yes"> Subscribe to our mailing list
        </label>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" name="submit" class="btn btn-warning">Submit Form</button>
      <button type="reset" name="clear" class="btn btn-default">Clear</button>
    </div>
  </div>
  <?php
    if(isset($_POST['submit'])){
        $cdt=date('Y');
		$c_idcom=no_auto("tb_comment",'COM'.$cdt);
		$full=$theme->injection($_POST['fullname']);
		$fullname=$full;
		$mailto=$theme->injection($_POST['email']);
		$complaint=$theme->injection($_POST['complaint']);
		$comment=$_POST['msg'];
		$subscribe=$_POST['subscribe'];
		$datein=date('Y-m-d');
        $vcode=$theme->injection($_POST['vcode']);
        
        if($_SESSION['6_letters_code']!=$vcode){
            echo'<script type="text/javascript">alert("Security code not valid..");</script>';
        }else {
            $save=mysql_query("INSERT INTO tb_comment(id_comment,datein,fullname,mail,category,comment,subscribe,publish,reading)VALUES('$c_idcom','$datein','$fullname','$mailto','$complaint','$comment','$subscribe','No','No')");
            
            require_once($theme->incurl('../../').'./themes/plugin/phpmailer/class.phpmailer.php');
            require_once($theme->incurl('../../').'./themes/plugin/phpmailer/class.smtp.php'); 
            $mail = new PHPMailer();		
            //SMTP Class
            $mail->IsSMTP(); //set untuk menggunakan SMTP
            $mail->SMTPDebug  = 1;                     
            //SMTP Setting
            $mail->SMTPAuth   = true;                  
            $mail->SMTPSecure = "tls";                 
            $mail->Host       = "smtp.gmail.com";      
            $mail->Port       = 587; 
            $mail->Username   = "noreply@queenstandoor.com";
            $mail->Password   = "queens1976"; 
            //Email From
            $mail->SetFrom("noreply@queenstandoor.com", "noreply@queenstandoor.com");
            $mail->AddReplyTo("noreply@queenstandoor.com","noreply@queenstandoor.com");
            $mail->Subject='Feedback - '.$complaint;
            $mail->AltBody="To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
            //Email Letter
            ob_start();
            require_once($theme->incurl('../../').'./themes/email/feedback-form.php');
            $html_message = ob_get_contents();
            ob_end_clean();
            //Email HTML Content
            $mail->MsgHTML($html_message);
            $mail->IsHTML(true); 
            //Email To
            $mail->AddAddress($mailto, $fullname);
            $mail->AddCC('puneet@queenstandoor.com', 'Puneet Malhotra');
            $mail->AddCC('bali@queenstandoor.com', 'Queens Bali');
            $mail->AddCC('bali3@queenstandoor.com', 'Marketing Queens');
            $mail->AddBCC('dsg1_qt1_bali@yahoo.com','Jonny Cible');
            if(!$mail->Send()) {
              echo "Mailer Error: " . $mail->ErrorInfo;
            } else { }
            
            if($save){
                echo'<script type="text/javascript">alert("Success, your comment has been sent..");</script>';
            }else{
                echo'<script type="text/javascript">alert("Error, your comment cannot been sent..");</script>';
            }
        }
    }
  ?>
</form>