<script type="text/javascript">
	$(document).ready(function(){
		$( "#from" ).datepicker({
		  changeMonth: true,
		  changeYear: true,
		  dateFormat:"yy-mm-dd",
		  numberOfMonths: 1,
		  onClose: function( selectedDate ) {
			$( "#to" ).datepicker( "option", "minDate", selectedDate );
		  }
		});
		$( "#to" ).datepicker({
		  changeMonth: true,
		  changeYear: true,
		  dateFormat:"yy-mm-dd",
		  numberOfMonths: 1,
		  onClose: function( selectedDate ) {
			$( "#from" ).datepicker( "option", "maxDate", selectedDate );
		  }
		});
	});
</script>
<style type="text/css">
.bs-modal-lg .modal-dialog {
  width: 85%;
}
.bs-modal-lg .modal-body {
  overflow-y: auto;
}
</style>
<style type="text/css">
.detail {
	margin:0px 0px;
	width:100%;
	border-left:solid 1px #CCC;
}
.detail ul {
	margin:0px;
}
.detail li {
	float:left;
	list-style:none;
	border-right:solid 1px #CCC;
	border-bottom:dashed 1px #CCC;
	padding:2px 10px;
}
.tb_no_t {
	width:10%;
	background-color:#f0f0f0;
}
.tb_guest_t {
	width:35%;
	background-color:#f0f0f0;
}
.tb_waiter_t {
	width:5%;
	background-color:#f0f0f0;
}
.tb_type_t {
	width:10%;
	background-color:#f0f0f0;
}
.tb_total_t {
	width:10%;
	background-color:#f0f0f0;
}
.tb_gtotal_t {
	width:10%;
	background-color:#f0f0f0;
}
.tb_disc_t {
	width:10%;
	background-color:#f0f0f0;
}
.tb_price_t {
	width:10%;
	background-color:#f0f0f0;
}

.tb_no {
	width:10%;
}
.tb_guest {
	width:35%;
}
.tb_waiter {
	width:5%;
}
.tb_type {
	width:10%;
}
.tb_total {
	width:10%;
	text-align:right;
}
.tb_gtotal {
	width:10%;
	text-align:right;
}
.tb_disc {
	width:10%;
}
.tb_price {
	width:10%;
}
</style>
<div class="row" style="margin-bottom:10px;">
	<div class="col-md-12">
    <?php
		$datefull=$theme->geturl('2');
		$pchdate=explode(' ',$datefull);
		$datefrom=$pchdate[0].'-'.$pchdate[1].'-'.$pchdate[2];
		$dateto=$pchdate[3].'-'.$pchdate[4].'-'.$pchdate[5];
	?>
    <form class="form-inline" role="form">
    Filter : 
      <div class="form-group">
        <label class="sr-only" for="exampleInputEmail2">Email address</label>
        <input type="text" name="datefrom" class="form-control" id="from" placeholder="Date from" value="<?=$datefrom;?>" required>
      </div>
      <div class="form-group">
        <label class="sr-only" for="exampleInputEmail2">Email address</label>
        <input type="text" name="dateto" class="form-control" id="to" placeholder="Date to" value="<?=$dateto;?>" required>
      </div>
      <button type="button" id="filter" class="btn btn-success">
      <span class="glyphicon glyphicon-filter"></span> Filter
      </button>
    </form>
    </div>
    <script type="text/javascript">
		$(document).ready(function(){
			$('#filter').click(function(){
				var from = $('#from').val();
				var to = $('#to').val();
				if(from==0){
					alert("Date from is empty..");
					$('#from').focus();
					return false;
				}else if(to==0){
					alert("Date to is empty..");
					$('#to').focus();
					return false;
				}else{
					window.location="<?php $theme->linkurl("../../"); echo strtolower(str_replace(' ','-',$theme->geturl('1'))).'/'; ?>"+from+'-'+to;
				}
			});
		});
	</script>
</div>
<div class="table-responsive">
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="table table-striped table-hover">
  <thead>
  <tr>
    <th height="26" align="center" bgcolor="#FFCC00">Transaction Date</th>
    <th align="center" bgcolor="#FFCC00">Sub Total</th>
    <th align="center" bgcolor="#FFCC00">Discount</th>
    <th align="center" bgcolor="#FFCC00">Dis. Price</th>
    <th align="center" bgcolor="#FFCC00">PPN</th>
    <th align="center" bgcolor="#FFCC00">Service</th>
    <th align="center" bgcolor="#FFCC00">Point</th>
    <th align="center" bgcolor="#FFCC00">Grand Total</th>
    <th align="center" bgcolor="#FFCC00">Option</th>
  </tr>
  </thead>
  <tbody>
  <?php
  	$dfrom=$datefrom;
	$dto=$dateto;
	if(!empty($datefull)){
		$selsh=mysql_query("SELECT jual.*, guestv.nomor, guesto.* FROM jual INNER JOIN guestv ON jual.kd_cust = guestv.kd_gues INNER JOIN guesto ON guestv.kd_gues = guesto.kode WHERE guestv.nomor='$idmember' AND jual.tanggal BETWEEN '$dfrom' AND '$dto'");
	}
	else {
  		$selsh=mysql_query("SELECT jual.*, guestv.nomor, guesto.* FROM jual INNER JOIN guestv ON jual.kd_cust = guestv.kd_gues INNER JOIN guesto ON guestv.kd_gues = guesto.kode WHERE guestv.nomor='$idmember'");
	}
	$selvip=mysql_fetch_array(mysql_query("SELECT poin, ambil, sisa FROM guestv WHERE nomor='$idmember'"));
	while($selshop=mysql_fetch_array($selsh)){
		$i++;
  ?>
  <tr class="memdet">
    <td><?php echo $selshop['tanggal']; ?></td>
    <td><?php echo number_format($selshop['total'],2,",","."); ?></td>
    <td><?php echo $selshop['disc'].'%'; ?></td>
    <td><?php echo number_format($selshop['discp'],2,",","."); ?></td>
    <td><?php echo $selshop['ppn'].'%'; ?></td>
    <td><?php echo $selshop['serv'].'%'; ?></td>
    <td><?php echo $selshop['pointk']; ?></td>
    <td><?php echo number_format($selshop['gtotal'],2,",","."); ?></td>
    <td><a href="#" data-toggle="modal" data-target="#myModal<?=$i;?>"><span class="glyphicon glyphicon-search"></span> Detail view</a></td>
  </tr>
  <!-- Modal -->
    <div class="modal fade bs-modal-lg" id="myModal<?=$i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog bs-modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel"><?php echo $selshop['title'].' '.$selshop['fname'].' '.$selshop['lname']; ?></h4>
            Date : <?php echo $selshop['tanggal']; ?> | Time : <?php echo $selshop['jam']; ?><br />
            Cashier : <?php echo $selshop['nm_opp']; ?><br><br>
            <span class="pull-left">Category : <?php echo $selshop['nm_time']; ?></span>
            <span class="pull-right">Table : <?php echo $selshop['nm_meja'].' - '.$selshop['no_meja']; ?></span><br />
            <span class="pull-left">Transaction No : <?php echo $selshop['no_bukti']; ?></span>
            <span class="pull-right">Staff Name : <?php echo $selshop['nm_wait']; ?></span><br />
          </div>
          <div class="modal-body">
            <div class="detail">
                <ul>
                	<li class="tb_no_t">Code</li>
                	<li class="tb_disc_t">Date</li>
                    <li class="tb_price_t">Time</li>
                    <li class="tb_guest_t">Food/Beverage Name</li>
                    <li class="tb_type_t">Type</li>
                    <li class="tb_waiter_t">Qty</li>
                    <li class="tb_total_t">Price</li>
                    <li class="tb_gtotal_t">Total</li>
                </ul>
                <div style="clear:left;"></div>
                <?php
					$seldet=mysql_query("SELECT jual.no_bukti, dtl_jual.* FROM jual INNER JOIN dtl_jual ON jual.no_bukti = dtl_jual.no_bukti WHERE jual.no_bukti='$selshop[no_bukti]'");
					$gtotal=0;
					while($viewdet=mysql_fetch_array($seldet)){
						$gtotal+=$viewdet['qty']*$viewdet['harga'];
				?>
                <ul>
                    <li class="tb_no"><?php echo $viewdet['kode']; ?></li>
                    <li class="tb_disc"><?php echo $viewdet['mdt_upd']; ?></li>
                    <li class="tb_price"><?php echo $viewdet['jamd']; ?></li>
                    <li class="tb_guest"><?php echo $viewdet['nama']; ?></li>
                    <li class="tb_type"><?php echo $viewdet['golongan']; ?></li>
                    <li class="tb_waiter"><?php echo $viewdet['qty']; ?></li>
                    <li class="tb_total"><?php echo number_format($viewdet['harga'],2,",","."); ?></li>
                    <li class="tb_gtotal"><?php echo number_format($viewdet['qty']*$viewdet['harga'],2,",","."); ?></li>
                </ul>
                <?php } ?>
                <div style="clear:left;"></div>
            </div>
            <?php
				$service=(5*$gtotal)/100;
				$ppn=10*($gtotal+$service)/100;
				$grand=$gtotal-$selshop['discp']+$ppn+$service;
			?>
            <div class="pull-right" style="margin-top:10px; padding-right:12px; text-align:right;">
            	<strong>SUB TOTAL : <?php echo number_format($gtotal,2,",","."); ?></strong><br />
                <strong>DISCOUNT : <?php echo number_format($selshop['discp'],2,",","."); ?></strong><br />
                <strong>TAX / SERVICE : <?php echo number_format($ppn+$service,2,",","."); ?></strong><br />
                <strong>GRAND TOTAL : <?php echo number_format($grand,2,",","."); ?></strong>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
  </tbody>
</table>
</div>
<div style="margin-bottom:20px;">
Reward point : <span class="label label-success"><?php echo $selvip['poin']; ?></span> | 
Redeem reward : <span class="label label-danger"><?php echo $selvip['ambil']; ?></span> | 
Balance : <span class="label label-warning"><?php echo $selvip['sisa']; ?></span>
<button type="reset" class="btn btn-default" style="margin-left:10px;">History Point</button>
</div>