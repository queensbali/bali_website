<script type="text/javascript">
	function refreshCaptcha(){
		var img = document.images['captchaimg'];
		img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
	}
	$(document).ready(function(){
		$("#hcountry").change(function(){
			var hcountry = $("#hcountry").val();
			$.ajax({
				type: 'POST',
				url: "<?php $theme->linkurl("function/view_country.php"); ?>",
				data: { nmcountry:hcountry},
				cache: false,
				success: function(data){
					$("#hmobile").val(data);
				}
			});
		});
		$("#ccountry").change(function(){
			var ccountry = $("#ccountry").val();
			$.ajax({
				type: 'POST',
				url: "<?php $theme->linkurl("function/view_country.php"); ?>",
				data: { nmcountry:ccountry},
				cache: false,
				success: function(data){
					$("#cphone").val(data);
				}
			});
		});
	});
</script>
<form action="<?php $_SERVER['PHP_SELF']; ?>" method="post" class="form-horizontal" role="form">
  <fieldset>
  <legend>Personal Detail</legend>
  <div class="form-group">
    <label for="title" class="col-sm-2 control-label">Title / Fullname</label>
    <div class="col-sm-2">
    <select class="form-control" name="title" id="title">
      <option value="Mr.">Mr.</option>
      <option value="Ms.">Ms.</option>
      <option value="Mrs.">Mrs.</option>
    </select>
    </div>
    <div class="col-sm-4">
    <input type="text" class="form-control" id="fname" placeholder="First Name" name="fname" required>
    </div>
    <div class="col-sm-4">
    <input type="text" class="form-control" id="lname" placeholder="Last Name" name="lname" required>
    </div>
  </div>
  <div class="form-group">
  	<label for="gender" class="col-sm-2 control-label">Gender</label>
  	<div class="col-sm-10">
      <label>
        <input type="radio" name="sex" id="gender" value="Male" required="required"> Male
      </label> 
      <label>
        <input type="radio" name="sex" id="gender" value="Female"> Female
      </label>
    </div>
  </div>
  <div class="form-group">
    <label for="nationality" class="col-sm-2 control-label">Nationality</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nationality" name="nationality" placeholder="Nationality" required>
    </div>
  </div>
  <div class="form-group">
    <label for="religion" class="col-sm-2 control-label">Religion</label>
    <div class="col-sm-10">
    <select class="form-control" name="religion" id="religion">
        <option value="N/A">N/A</option>
		<?php
			$selrel=mysql_query("SELECT * FROM tb_religion WHERE publish='YES'");
			while($rowrel=mysql_fetch_array($selrel)){
		?>
        <option value="<?=$rowrel['slug'];?>"><?=$rowrel['title'];?></option>
        <?php } ?>
    </select>
    </div>
  </div>
  <div class="form-group">
    <label for="haddress" class="col-sm-2 control-label">Home Address</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="haddress" name="haddress" placeholder="Home Address" required>
    </div>
  </div>
  <div class="form-group">
    <label for="hcity" class="col-sm-2 control-label">City / Area / Postal</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="hcity" name="hcity" placeholder="City" required>
    </div>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="harea" name="harea" placeholder="Area" required>
    </div>
    <div class="col-sm-2">
      <input type="text" class="form-control" id="hpost" name="hpost" placeholder="Postal Code">
    </div>
  </div>
  <div class="form-group">
    <label for="hcountry" class="col-sm-2 control-label">Country</label>
    <div class="col-sm-10">
    <select class="form-control" id="hcountry" name="hcountry">
	  <option selected="selected" value="">Select one country</option>
      <?php
        $selcountry=mysql_query("SELECT * FROM tb_country");
        while($rowcountry=mysql_fetch_array($selcountry)){
      ?>
      <option value="<?=$rowcountry['id_country'];?>"><?=$rowcountry['country_name'];?></option>
      <?php } ?>
    </select>
    </div>
  </div>
  <div class="form-group">
    <label for="hphone" class="col-sm-2 control-label">Phone / Fax / Mobile</label>
    <div class="col-sm-3">
      <input type="text" class="form-control" id="hphone" name="hphone" placeholder="Phone Number">
    </div>
    <div class="col-sm-3">
      <input type="text" class="form-control" id="hfax" name="hfax" placeholder="Fax Number">
    </div>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="hmobile" name="hmobile" placeholder="Mobile Number" required>
    </div>
  </div>
  <div class="form-group">
    <label for="hmail" class="col-sm-2 control-label">Email Address</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="hmail" name="hmail" placeholder="info@example.com">
    </div>
  </div>
  <div class="form-group">
    <label for="hweb" class="col-sm-2 control-label">Website</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="hweb" name="hweb" placeholder="http://">
    </div>
  </div>
  <legend>Private Detail</legend>
  <div class="form-group">
    <label for="birthplace" class="col-sm-2 control-label">Place / Date of Birth</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="birthplace" name="birthplace" placeholder="Place of Birth">
    </div>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="datepicker" name="idate3" placeholder="Date of Birth">
    </div>
  </div>
  <div class="form-group">
    <label for="idate4" class="col-sm-2 control-label">Marriage Date</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="datepicker2" name="idate4" placeholder="Marriage Date">
    </div>
  </div>
  <div class="form-group">
    <label for="hobby" class="col-sm-2 control-label">Hobby</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="hobby" name="hobby" placeholder="Hobby">
    </div>
  </div>
  <div class="form-group">
    <label for="remark" class="col-sm-2 control-label">Remark</label>
    <div class="col-sm-10">
      <textarea class="form-control" rows="3" id="remark" name="remark" placeholder="Remark"></textarea>
    </div>
  </div>
  <legend>Company Detail</legend>
  <div class="form-group">
    <label for="cname" class="col-sm-2 control-label">Company Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="cname" name="cname" placeholder="Company Name">
    </div>
  </div>
  <div class="form-group">
    <label for="caddress" class="col-sm-2 control-label">Company Address</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="caddress" name="caddress" placeholder="Company Address">
    </div>
  </div>
  <div class="form-group">
    <label for="ccity" class="col-sm-2 control-label">City / Area / Postal</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="ccity" placeholder="City">
    </div>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="carea" placeholder="Area">
    </div>
    <div class="col-sm-2">
      <input type="text" class="form-control" id="cpost" placeholder="Postal Code">
    </div>
  </div>
  <div class="form-group">
    <label for="ccountry" class="col-sm-2 control-label">Country</label>
    <div class="col-sm-10">
    <select class="form-control" id="ccountry" name="ccountry">
	  <option selected="selected" value="">Select one country</option>
      <?php
        $selcountry=mysql_query("SELECT * FROM tb_country");
        while($rowcountry=mysql_fetch_array($selcountry)){
      ?>
      <option value="<?=$rowcountry['id_country'];?>"><?=$rowcountry['country_name'];?></option>
      <?php } ?>
    </select>
    </div>
  </div>
  <div class="form-group">
    <label for="cphone" class="col-sm-2 control-label">Phone / Fax</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="cphone" name="cphone" placeholder="Phone Number">
    </div>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="cfax" name="cfax" placeholder="Fax Number">
    </div>
  </div>
  <div class="form-group">
    <label for="cposition" class="col-sm-2 control-label">Position</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="cposition" name="cposition" placeholder="Position">
    </div>
  </div>
  <div class="form-group">
    <label for="cmail" class="col-sm-2 control-label">Email Address</label>
    <div class="col-sm-10">
      <input type="email" class="form-control" id="cmail" name="cmail" placeholder="Email Address" required>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
    <img src="<?php $theme->linkurl('../../captcha/captcha.php'); ?>?rand=<?php echo rand(); ?>" id='captchaimg' ><br />
    <small>Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh</small>
    </div>
  </div>
  <div class="form-group">
    <label for="codevalid" class="col-sm-2 control-label">Verify Code</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="codevalid" name="codevalid" placeholder="Verify Code" required>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
        <label>
          <input name="subscribe" id="subscribe" type="checkbox"> Subscribe to our Mailing List
        </label>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" name="rsubmit" class="btn btn-warning">Register Now</button>
      <button type="reset" class="btn btn-default">Clear Form</button>
    </div>
  </div>
  </fieldset>
</form>

<?php
	$dt=date("y");
	$kode=no_auto("guesti",'GIBLQTR-'.$dt);
	$datein=date("Y-m-d");
	$timein=date("H:m:i");
	$title=$_POST['title'];
	$fname=$_POST['fname'];
	$lname=$_POST['lname'];
	$sex=$_POST['sex'];
	$nationality=$_POST['nationality'];
	$religion=$_POST['religion'];
	$haddress=$_POST['haddress'];
	$harea=$_POST['harea'];
	$hpost=$_POST['hpost'];
	$hcity=$_POST['hcity'];
	$hcountry=$_POST['hcountry'];
	$hphone=$_POST['hphone'];
	$hfax=$_POST['hfax'];
	$hmobile=$_POST['hmobile'];
	$hmail=$_POST['hmail'];
	$remark=$_POST['remark'];
	$web=$_POST['hweb'];
	$kl_guest='Personal';
	
	$cname=$_POST['cname'];
	$cposition=$_POST['cposition'];
	$caddress=$_POST['caddress'];
	$carea=$_POST['carea'];
	$cpost=$_POST['cpost'];
	$ccity=$_POST['ccity'];
	$ccountry=$_POST['ccountry'];
	$cphone=$_POST['cphone'];
	$cfax=$_POST['cfax'];
	$cmail=$_POST['cmail'];
	$cnamepos=$cname.' | '.$cposition;
	
	$birthplace=$_POST['birthplace'];
	$birthdate=$_POST['idate3'];
	$marriage=$_POST['idate4'];
	$hobby=$_POST['hobby'];
	
	$read=$_POST['read'];
	$subscribe=$_POST['subscribe'];
	$captcha=$_POST['codevalid'];
	
	if(isset($_POST['rsubmit'])){
		if($_SESSION['6_letters_code']!=$captcha){
			echo'<script type="text/javascript">alert("Code validation not valid..");</script>';
		}
		else {
			include "./themes/function/random_code.php";
			$save=mysql_query("INSERT INTO guesti(kode, datein, fname, title, lname, pbirth, dbirth, national, religion, sex, haddress1, hcity, hpostcode, harea, hcountry, hphone1, hfax1, hp1, mail1, aktif, hobby1, dwedding, company, caddress1, ccity, cpostcode, carea, ccountry, cphone1, cfax1, cmail1, dentry, remark, web_site, kl_guest, okveri, mpl_mde, mpl_upd, mdt_upd, mtm_upd, subscribe, codever, gstatus) VALUES ('$kode','$datein','$fname','$title','$lname','$birthplace','$birthdate','$nationality','$religion','$sex','$haddress','$hcity','$hpost','$harea','$hcountry','$hphone','$hfax','$hmobile','$hmail','T','$hobby','$marriage','$cnamepos','$caddress','$ccity','$cpost','$carea','$ccountry','$cphone','$cfax','$cmail','$datein','$remark','$web','$kl_guest','F','BLQTR','BLQTR','$datein','$timein','$subscribe','$codever','0')");
			
			require_once('./themes/plugin/phpmailer/class.phpmailer.php');
			require_once('./themes/plugin/phpmailer/class.smtp.php'); 
			$mail = new PHPMailer();		
			//SMTP Class
			$mail->IsSMTP(); //set untuk menggunakan SMTP
			$mail->SMTPDebug  = 1;                     
			//SMTP Setting
			$mail->SMTPAuth   = true;                  
			$mail->SMTPSecure = "tls";                 
			$mail->Host       = "smtp.gmail.com";      
			$mail->Port       = 587; 
			$mail->Username   = "noreply@queenstandoor.com";
			$mail->Password   = "queens1976"; 
			//Email From
			$mail->SetFrom("noreply@queenstandoor.com", "noreply@queenstandoor.com");
			$mail->AddReplyTo("noreply@queenstandoor.com","noreply@queenstandoor.com");
			$mail->Subject="MEMBERSHIP APPLICATION";
			$mail->AltBody="To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
			//Email Letter
			ob_start();
			require_once('./themes/email/membership-form.php');
			$html_message = ob_get_contents();
			ob_end_clean();
			//Email HTML Content
			$mail->MsgHTML($html_message);
			$mail->IsHTML(true); 
			//Email To
			$mail->AddAddress($mailto, $fullname);
			$mail->AddCC('puneet@queenstandoor.com', 'Puneet Malhotra');
			$mail->AddCC('bali@queenstandoor.com', 'Queens Bali');
			$mail->AddBCC('dsg1_qt1_bali@yahoo.com','Jonny Cible');
			if(!$mail->Send()) {
			  echo "Mailer Error: " . $mail->ErrorInfo;
			} else { }
			
			if($save){
				echo'<script type="text/javascript">alert("Success, your data has been saved..");</script>';
			}else{
				echo'<script type="text/javascript">alert("Error, your data cannot been save..");</script>';
			}
		}
	}
?>