<style type="text/css">
.databs{
	margin-top:0px;
	margin-bottom:10px;
	margin-left:30px;
	font-size:12px;
}
h4 {
	margin:0px;
	padding:0px;
}
</style>
<?php
	session_start();
	$sessionID = $_COOKIE['PHPSESSID'];
	$linkurl=$theme->geturl('3');
	if(empty($linkurl)||$linkurl=='Account'){
		$billing='class="active"';
		$payment='';
		$bilspan='';
		$payspan='';
	}else if($linkurl=='Payment'){
		$billing='';
		$payment='class="active"';
		$bilspan='<span class="glyphicon glyphicon-ok"></span>';
		$payspan='';
	}else {
		$billing='';
		$payment='';
		$bilspan='<span class="glyphicon glyphicon-ok"></span>';
		$payspan='<span class="glyphicon glyphicon-ok"></span>';
	}
?>
<div class="row">
	<div class="col-md-3">
    <ul class="nav nav-pills nav-stacked">
    	<li <?=$billing;?>><a href="#"><?=$bilspan;?> Your Details</a>
        	<ul class="databs">
            	<?php $selbil=mysql_fetch_array(mysql_query("SELECT * FROM tb_carts_billing WHERE cart_session='$sessionID'")); ?>
                <li><?=$selbil['title'].' '.$selbil['fname'].' '.$selbil['lname'];?></li>
                <li><?=$selbil['haddress'].' '.$selbil['hcity'].' '.$selbil['harea'].' '.$selbil['hcountry'];?></li>
                <li><?=$selbil['hmobile'];?></li>
                <li><?=$selbil['email'];?></li>
            </ul>
        </li>
        <li <?=$payment;?>><a href="#"><?=$payspan;?> Payment Method</a></li>
    </ul>
    </div>
    <div class="col-md-9">
    <ol class="breadcrumb">
      <li <?=$billing;?>><?=$bilspan;?> Your Details</li>
      <li <?=$payment;?>><?=$payspan;?> Payment Method</li>
    </ol>
    <?php
		$link=$theme->geturl('3');
		if(empty($link)){
			include $theme->incurl('../inc/').'inc_checkout_login.php';
		}else if($link=='Account'){
			include $theme->incurl('../inc/').'inc_checkout_billing.php';
		}else if($link=='Payment'){
			include $theme->incurl('../inc/').'inc_checkout_payment.php';
		}else if($link=='Finish'){
			include $theme->incurl('../inc/').'inc_checkout_finish.php';
		}
	?>
    </div>
</div>