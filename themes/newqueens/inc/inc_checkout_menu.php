<div class="row">
	<div class="col-md-12">
    <?php
		session_start();
		$sessionID = $_COOKIE['PHPSESSID'];
		$carts=mysql_query("SELECT * FROM tb_carts WHERE cart_session='$sessionID' AND type='Delivery'");
		$cekcarts=mysql_num_rows($carts);
		if($cekcarts==0){
	?>
    <div style="margin:50px 0;">
    <center>
    <img src="<?php $theme->linkurl("img/"); echo'empty.gif'; ?>" height="200" /><br />
    <a href="<?php $theme->linkurl("../../delivery"); ?>">Click here to continue shopping..</a>
    </center>
    </div>
    <?php } else { ?>
    <div class="table-responsive">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-hover">
      <thead>
      <tr>
        <th width="12%" bgcolor="#FFCC00">&nbsp;</th>
        <th width="45%" bgcolor="#FFCC00">Product Name</th>
        <th width="14%" bgcolor="#FFCC00">Unit Price</th>
        <th width="7%" bgcolor="#FFCC00">QTY</th>
        <th width="14%" bgcolor="#FFCC00">Sub Total</th>
        <th width="8%" bgcolor="#FFCC00">Remove</th>
      </tr>
      </thead>
      <tbody>
      <?php
	  	session_start();
		$sessionID = $_COOKIE['PHPSESSID'];
		$querymenu = mysql_query("SELECT tb_carts.id_cart, tb_menu.* FROM tb_carts INNER JOIN tb_menu ON tb_carts.id_menu = tb_menu.id_menu WHERE tb_carts.cart_session='".$sessionID."' GROUP BY tb_carts.id_menu ORDER BY tb_carts.id_cart DESC");
		$cekmenu=mysql_num_rows($querymenu);
		if(!empty($cekmenu)){
			$selcart = mysql_query("SELECT tb_carts.*, tb_menu.* FROM tb_carts INNER JOIN tb_menu ON tb_carts.id_menu = tb_menu.id_menu WHERE tb_carts.cart_session='".$sessionID."' GROUP BY tb_carts.id_menu ORDER BY tb_carts.id_cart DESC");
		}else {
			$selcart = mysql_query("SELECT * FROM tb_carts WHERE tb_carts.cart_session='".$sessionID."' ORDER BY tb_carts.id_cart DESC");
		}
		$subtot=0;
		$tax=0;
		$service=0;
		$gtotal=0;
		while($vwcart=mysql_fetch_array($selcart)){
			$items  = mysql_fetch_array(mysql_query("SELECT COUNT(*) AS totalItems FROM tb_carts WHERE cart_session='".$sessionID."' AND id_menu='".$vwcart['id_menu']."'"));
			$subtot=($subtot+$vwcart['price']*$vwcart['qty']);
			$charge=$subtot*5/100;
			$service=$charge+($charge*10/100);
			$tax=$subtot*10/100;
			$gtotal=$subtot+$tax+$service;
	  ?>
      <tr>
        <td>
        <?php
			if(empty($vwcart['thumbs'])){
				$thumbs='no-photo.gif';
				$large='no-photo.gif';
			}else{
				$thumbs=$vwcart['thumbs'];
				$large=$vwcart['large'];
			}
		?>
        <img src="<?php $theme->linkurl('../../upload/menu/'); echo $thumbs; ?>" height="50" class="img-thumbnail">
        </td>
        <td valign="middle">
		<strong><?php echo $vwcart['menu']; ?></strong><br />
        <font color="#666666">
		<?php echo $vwcart['description']; ?>
        </font>
        </td>
        <td>Rp. <span class="pull-right"><?php echo number_format($vwcart['price'],2,",","."); ?></span></td>
        <td>
        <input name="qty<?php echo $vwcart['id_menu']; ?>" type="text" class="qty" id="qty<?php echo $vwcart['id_menu']; ?>" size="5" value="<?php echo $vwcart['qty']; ?>">
        </td>
        <td>Rp. <span class="pull-right"><?php echo number_format($vwcart['price']*$vwcart['qty'],2,",",".");?></span>
        </td>
        <td align="center">
        <a href="#" class="updateLink" id="<?php echo $vwcart['id_menu']; ?>" title="Update cart"><span class="glyphicon glyphicon-edit"></span></a>
        <a href="#" class="deleteLink" id="<?php echo $vwcart['id_cart']; ?>" title="Delete cart"><span class="glyphicon glyphicon-remove"></span></a>
        </td>
      </tr>
      <?php } ?>
      </tbody>
    </table>
    </div><hr>
    <div style="margin-bottom:10px;">
    <span class="pull-right">Sub Total : <?php echo 'Rp. '.number_format($subtot,2,",","."); ?></span><br>
    <span class="pull-right">Tax : <?php echo 'Rp. '.number_format($tax,2,",","."); ?></span><br>
    <span class="pull-right">Service : <?php echo 'Rp. '.number_format($service,2,",","."); ?></span><br>
	<span class="pull-right"><strong>Grand Total : <?php echo 'Rp. '.number_format($gtotal,2,",","."); ?></strong></span><br>
    </div>
    <button type="button" class="btn btn-default pull-left" onClick="history.go(-1); return false;">Continue shopping</button>
    <button type="button" class="btn btn-danger pull-right" style="margin-left:10px;" onclick="window.location='<?php $theme->linkurl('../../checkout/register'); ?>';">Proceed to Checkout &rarr;</button>
    <?php } ?>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {	
		$(".updateLink").click(function(){
			var target = $(this).attr("id");
			var cqty = document.getElementById("qty"+target).value;
			
			$.ajax({
				type: 'POST',
				url: '<?php $theme->linkurl("inc/inc_update_cart_by_one.php"); ?>',
				data: { id:target,cqty:cqty },
				error: function(){
				   alert('Error!');
				},
				success: function(data) {
				   location.reload();
				}
			});
		});
		$(".deleteLink").click(function(){
			var del_id = $(this).attr('id');
			var data = 'id='+del_id;
			var parent = $(this).parent().parent();
			
			if(confirm("Sure you want to delete this ID : "+del_id)){
				$.ajax({
					type: "POST",
					url: "<?php $theme->linkurl('inc/inc_delete_cart.php'); ?>",
					data: data,
					cache: false,
					success: function(msg){ 
						alert(msg);
						parent.fadeOut('slow', function() {$(this).remove();});
						location.reload();
					}
				});
			}
			return false;
		});
	});
</script>