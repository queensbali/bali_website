<?php
	$geturl=$theme->geturl('2');
	if($geturl=="Questionnaire"){
		$tab1='';
		$tab2='active';
		$tab3='';
	}else if($geturl=="View Posted Comments"){
		$tab1='';
		$tab2='';
		$tab3='active';
	}else{
		$tab1='active';
		$tab2='';
		$tab3='';
	}
?>
<div class="tabbable"> <!-- Only required for left/right tabs -->
    <ul class="nav nav-tabs">
        <li onClick="window.location='<?php echo $theme->getlink('','1'); ?>/post-comment'" class="<?=$tab1;?>"><a href="#tab1" data-toggle="tab">Post Comment</a></li>
        <li onClick="window.location='<?php echo $theme->getlink('','1'); ?>/questionnaire'" class="<?=$tab2;?>"><a href="#tab2" data-toggle="tab">Questionnaire</a></li>
        <li onClick="window.location='<?php echo $theme->getlink('','1'); ?>/view-posted-comments'" class="<?=$tab3;?>"><a href="#tab3" data-toggle="tab">View Posted Comments</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane <?=$tab1;?>" id="tab1">
        	<?php include "./themes/newqueens/inc/inc_feedback_form.php"; ?>
        </div>
        <div class="tab-pane <?=$tab2;?>" id="tab2">
        	<?php include "./themes/newqueens/inc/inc_questionnaire.php"; ?>
        </div>
        <div class="tab-pane <?=$tab3;?>" id="tab3">
        	<?php include "./themes/newqueens/inc/inc_feedback_view.php"; ?>
        </div>
    </div>
</div>

<!--<a class="twitter-timeline" href="https://twitter.com/queensbali" data-widget-id="420503507372744705">Tweets by @queensbali</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>-->