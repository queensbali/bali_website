<script type="text/javascript">
	$(function() {
		$("#datepicker").datepicker({
		  dateFormat:"yy-mm-dd",
		  changeMonth: true,
		  changeYear: true,
                  minDate: '0'
		});
	});
	function refreshCaptcha(){
		var img = document.images['captchaimg'];
		img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
	}
	$(document).ready(function(){
		$("#country").change(function(){
			var nmcountry = $("#country").val();
			$.ajax({
				type: 'POST',
				url: "<?php $theme->linkurl("function/view_country.php"); ?>",
				data: { nmcountry:nmcountry},
				cache: false,
				success: function(data){
					$("#mobile").val(data);
				}
			});
		});
	});
</script>
<div class="row">
	<div class="col-md-9">
        <form role="form" class="form-horizontal" method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
          <fieldset>
          <legend>Information Detail</legend>
          <section>
          <div class="form-group">
            <label for="outlet" class="col-sm-4 control-label">Outlet / Branch</label>
            <div class="col-sm-8">
            <select name="outlet" class="form-control" id="outlet">
              <?php
			  	$selout=mysql_query("SELECT * FROM tb_outlet ORDER BY id_outlet ASC");
				while($vwout=mysql_fetch_array($selout)){
			  ?>
              <option value="<?=$vwout['id_outlet'];?>"><?=$vwout['name'].' - '.$vwout['state'];?></option>
              <?php } ?>
            </select>
            </div>
          </div>
          <div class="form-group">
            <label for="datein" class="col-sm-4 control-label">Date for Visit</label>
            <div class="col-sm-4">
            <input type="text" name="idate" class="form-control" id="datepicker" placeholder="Date for visit" required>
            </div>
            <div class="col-sm-2">
            <select name="hour" id="hour" class="form-control" required>
			<?php for($i = 11; $i <= 23; $i++) {?>
                <option <?php echo $cekhr; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
            <?php } ?>
            </select>
            </div>
            <div class="col-sm-2">
            <select name="minute" id="minute" class="form-control" required>
		<?php
                for($u = 0; $u <= 59; $u++) {
                    if($u<10){
                        $a='0'.$u;
                    }else {
                        $a=$u;
                    }
            	?>
                <option <?php echo $cekmin; ?> value="<?php echo $a; ?>"><?php echo $a; ?></option>
            <?php } ?>
            </select>
            </div>
          </div>
          <div class="form-group">
            <label for="amount" class="col-sm-4 control-label">Total People</label>
            <div class="col-sm-4">
            <input name="adult" type="number" class="form-control" id="adult" placeholder="Adult" min="0" required>
            </div>
            <div class="col-sm-4">
            <input name="child" type="number" class="form-control" id="child" placeholder="Child" min="0">
            </div>
          </div>
          <div class="form-group">
            <label for="amount" class="col-sm-4 control-label">Special Request</label>
            <div class="col-sm-8">
            <textarea name="request" cols="" rows="" class="form-control"></textarea>
            </div>
          </div>
          <!--<div class="form-group">
            <label for="company" class="col-sm-4 control-label">Payment Mode</label>
            <div class="col-sm-8">
            <label class="radio-inline ">
              <input type="radio" name="pmode" id="optionsRadios1" value="Credit"> Credit
            </label>
            <label class="radio-inline ">
              <input type="radio" name="pmode" id="optionsRadios1" value="Cash" checked> 
              Cash
            </label>
            <label class="radio-inline ">
              <input type="radio" name="pmode" id="optionsRadios1" value="Bank Transfer"> 
              Bank Transfer
            </label>
            </div>
          </div>-->
          </section>
          
          <legend>Personal Details</legend>
          <section>
          <div class="form-group">
            <label for="datein" class="col-sm-4 control-label">Title</label>
            <div class="col-sm-8">
            <select name="title" class="form-control" id="title" required>
              <option value="Mr.">Mr.</option>
              <option value="Ms.">Ms.</option>
              <option value="Mrs.">Mrs.</option>
            </select>
            </div>
          </div>
          <div class="form-group">
            <label for="fullname" class="col-sm-4 control-label">Fullname</label>
            <div class="col-sm-4">
            <input name="fname" type="text" class="form-control" id="fname" placeholder="Insert your first name.." required>
            </div>
            <div class="col-sm-4">
            <input name="lname" type="text" class="form-control" id="lname" placeholder="Insert your last name..">
            </div>
          </div>
          <div class="form-group">
            <label for="address" class="col-sm-4 control-label">Home Address</label>
            <div class="col-sm-8">
            <input name="address" type="text" class="form-control" id="address" placeholder="Insert your home address.." required>
            </div>
          </div>
          <div class="form-group">
            <label for="city" class="col-sm-4 control-label">City</label>
            <div class="col-sm-4">
            <input name="city" type="text" class="form-control" id="city" placeholder="Insert your city.." required>
            </div>
            <div class="col-sm-4">
            <input name="pcode" type="text" class="form-control" id="pcode" placeholder="Insert your postal code..">
            </div>
          </div>
          <div class="form-group">
            <label for="datein" class="col-sm-4 control-label">Country</label>
            <div class="col-sm-8">
            <select name="country" class="form-control" id="country" required>
              <option selected="selected" value="">Select one country</option>
			  <?php //include "themes/newqueens/inc/inc_country.php"; ?>
              <?php
			  	$selcountry=mysql_query("SELECT * FROM tb_country");
				while($rowcountry=mysql_fetch_array($selcountry)){
			  ?>
              <option value="<?=$rowcountry['id_country'];?>"><?=$rowcountry['country_name'];?></option>
              <?php } ?>
            </select>
            </div>
          </div>
          <div class="form-group">
            <label for="address" class="col-sm-4 control-label">Phone Number</label>
            <div class="col-sm-8">
            <input name="phone" type="text" class="form-control" id="phone" placeholder="Insert your phone number..">
            </div>
          </div>
          <div class="form-group">
            <label for="address" class="col-sm-4 control-label">Mobile Number</label>
            <div class="col-sm-8">
            <input name="mobile" type="text" class="form-control" id="mobile" placeholder="Insert your mobile number.. (+62)" required>
            </div>
          </div>
          <div class="form-group">
            <label for="address" class="col-sm-4 control-label">Email Address</label>
            <div class="col-sm-8">
            <input name="email" type="email" class="form-control" id="email" placeholder="Insert your email address.." required>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
            <img src="<?php $theme->linkurl('../../captcha/captcha.php'); ?>?rand=<?php echo rand(); ?>" id='captchaimg' ><br />
            <small>Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh</small>
            </div>
          </div>
          <div class="form-group">
            <label for="inputSubject" class="col-sm-4 control-label">Verify Code</label>
            <div class="col-sm-8">
              <input name="vcode" type="text" class="form-control" id="inputSubject" placeholder="Insert validation code.." required>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="subscribe" value="Yes"> Subscribe to our mailing list
                </label>
              </div>
            </div>
          </div>
          <!--<div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="term" value="Yes"> I have read terms & condition
                </label>
              </div>
            </div>
          </div>-->
          <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
              <button type="submit" name="submitres" class="btn btn-warning">Submit Form</button>
              <button type="reset" class="btn btn-default">Clear Form</button>
            </div>
          </div>
          </section>
          </fieldset>
          <?php
		  	$id_outlet=$_POST['outlet'];
			$subject="Online Reservation";
			$datevisit=$_POST['idate'];
			$time=$_POST['hour'].':'.$_POST['minute'];
			$adult=$_POST['adult'];
			$child=$_POST['child'];
			$specrec=$_POST['request'];
			$payment='Cash';
			
			$title=$_POST['title'];
			$fname=$_POST['fname'];
			$lname=$_POST['lname'];
			$fullname=$title.' '.$fname.' '.$lname;
			$haddress=$_POST['address'];
			$city=$_POST['city'];
			$postal=$_POST['pcode'];
			$country=$_POST['country'];
			$phone=$_POST['phone'];
			$mobile=$_POST['mobile'];
			$mailto=$_POST['email'];
			$subscribe=$_POST['subscribe'];
			$ipaddress=$_SERVER['REMOTE_ADDR'];
			$datein=date('Y-m-d');
			$captcha=$_POST['vcode'];
			$msg='QUEENS BALI - Thank you for your reservation enquiry. Your enquiry has been sent to our sales & marketing team and will be processed and responded to within 24 hours.';
			
			if(isset($_POST['submitres'])){
				if($_SESSION['6_letters_code']!=$captcha){
					echo'<script type="text/javascript">alert("Code validation not valid..");</script>';
				}
				else {
					$selcab=mysql_fetch_array(mysql_query("SELECT * FROM tb_outlet WHERE id_outlet='$id_outlet'"));
					include "./themes/function/random_code.php";
					$dt=date('Y');
					$id_reserv=no_auto("tb_reservation",'RSV'.$dt);
					$save=mysql_query("INSERT INTO tb_reservation(id_reservation, datein, id_outlet, fullname, address, city, postal, country, phone, mobile, mail, datevisit, timevisit, timeend, adult, child, subject, request, payment, subscribe, ip_address, codever, status)VALUES('$id_reserv','$datein','$id_outlet','$fullname','$haddress','$city','$postal','$country','$phone','$mobile','$mailto','$datevisit','$time','$time','$adult','$child','$subject','$specrec','$payment','$subscribe','$ipaddress','$codever','New Order')");
					
					require_once($theme->incurl('../../').'./themes/plugin/mycoolsms/MyCoolSMS.class.php');
					$oMyCoolSMS = new MyCoolSMS();
					$oResponse = $oMyCoolSMS->sendSms($mobile, $msg);
					$oResponse = $oMyCoolSMS->sendSms('+6281558584669', 'NEW RESERVATION - '.$fullname.' | '.$mailto.' | '.$mobile.' | QUEENS - '.$selcab['state'].' | '.$datevisit.' '.$time.' | Adult:'.$adult.',Child:'.$child.' | Request:'.$specrec);
					
					require_once($theme->incurl('../../').'./themes/plugin/phpmailer/class.phpmailer.php');
					require_once($theme->incurl('../../').'./themes/plugin/phpmailer/class.smtp.php'); 
					$mail = new PHPMailer();		
					//SMTP Class
					$mail->IsSMTP(); //set untuk menggunakan SMTP
					$mail->SMTPDebug  = 1;                     
					//SMTP Setting
					$mail->SMTPAuth   = true;                  
					$mail->SMTPSecure = "tls";                 
					$mail->Host       = "smtp.gmail.com";      
					$mail->Port       = 587; 
					$mail->Username   = "noreply@queenstandoor.com";
					$mail->Password   = "queens1976"; 
					//Email From
					$mail->SetFrom("noreply@queenstandoor.com", "noreply@queenstandoor.com");
					$mail->AddReplyTo("noreply@queenstandoor.com","noreply@queenstandoor.com");
					$mail->Subject="Thank You for Your Reservation";
					$mail->AltBody="To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
					//Email Letter
					ob_start();
					require_once($theme->incurl('../../').'./themes/email/reservasi-form.php');
					$html_message = ob_get_contents();
					ob_end_clean();
					//Email HTML Content
					$mail->MsgHTML($html_message);
					$mail->IsHTML(true); 
					//Email To
					$mail->AddAddress($mailto, $fname.' '.$lname);
					$mail->AddCC('puneet@queenstandoor.com', 'Puneet Malhotra');
					$mail->AddCC('bali3@queenstandoor.com', 'Marketing Queens');
					$mail->AddBCC('dsg1_qt1_bali@yahoo.com','Jonny Cible');
					if(!$mail->Send()) {
					  echo "Mailer Error: " . $mail->ErrorInfo;
					} else { }
					
					if($save){
						echo'<script type="text/javascript">alert("Thank Your For Reservation, we will replay your inquiry soon as possible");</script>';
					}else {
						echo'<script type="text/javascript">alert("Error, Reservation not complete..");</script>';
					}
					
					/*if($oResponse->success) {
						print_r($oResponse);
					} else {
						print_r($oResponse);
					}*/
				}
			}
		  ?>
        </form>
    </div>
    <div class="col-md-3 hidden-xs hidden-sm">
    	<?php
			$selmn=mysql_query("SELECT tb_menu.*, tb_menu_cat.title AS cattitle, tb_menu_type.title AS typetitle FROM tb_menu INNER JOIN tb_menu_cat ON tb_menu.id_category = tb_menu_cat.id_category INNER JOIN tb_menu_type ON tb_menu_cat.id_type = tb_menu_type.id_type ORDER BY RAND() LIMIT 0,3");
			while($vwmenu=mysql_fetch_array($selmn)){
		?>
        <div class="media">
          <?php
			if(empty($vwmenu['thumbs'])){
				$thumbs='no-photo.gif';
				$large='no-photo.gif';
			}else{
				$thumbs=$vwmenu['thumbs'];
				$large=$vwmenu['large'];
			}
		  ?>
          <a class="pull-left" href="<?php $theme->linkurl('../../our-menu/'); echo $theme->smalink($vwmenu['type']).'/'.$theme->smalink($vwmenu['category']); ?>">
            <img class="media-object img-thumbnail" src="<?php $theme->linkurl('../../upload/menu/'); echo $thumbs; ?>" alt="<?=$vwmenu['description'];?>" width="100">
          </a>
          <div class="media-body">
            <h4 class="media-heading">
			<a href="<?php $theme->linkurl("../../our-menu/"); echo strtolower(str_replace(' ','-',$vwmenu['typetitle']).'/'.str_replace(' ','-',$vwmenu['cattitle'])); ?>" style="color:#F90;"><?=$vwmenu['menu'];?></a></h4>
            <small>
            <?php
				$content = ($vwmenu['description']);
				$content = substr($content,0,50);
				$content = substr($content,0,strrpos($content," "));
				echo $content.' [ . . . ]';
			?>
            </small>
          </div>
        </div>
        <?php } ?>
    </div>
</div>

<!--<div class="tabbable columns">
    <ul id="myTab" class="nav nav-tabs">
        <li class="active"><a id="n.1" href="#declarations1" data-toggle="tab"> Declarations </a></li>
        <li class=""><a id="n.2" href="#hradetails1" data-toggle="tab">HRA Details</a></li>
        <li class=""> <a id="n.3" href="#preEmpSalary1" data-toggle="tab">Previous Employment Salary</a></li>
        <li><a id="n.4" href="#otherIncome1" data-toggle="tab">Other Income</a></li>
    </ul>
    <form method="post">
    <div id="myTabContent" class="tab-content">
        <div class="tab-pane fade active in" id="declarations1">
           <p>The first content</p>
           <input type="button" onclick="document.getElementById('n.2').click()" value="Next" />
        </div>
        <div class="tab-pane fade" id="hradetails1">
            <p>The 2nd content</p>
            <input type="button" onclick="document.getElementById('n.1').click()" value="Previous" />
            <input type="button" onclick="document.getElementById('n.3').click()" value="Next" />
        </div>
        <div class="tab-pane fade" id="preEmpSalary1">
            <p>The 3rd content</p>
            <input type="button" onclick="document.getElementById('n.2').click()" value="Previous" />
            <input type="button" onclick="document.getElementById('n.4').click()" value="Next" />
        </div>
        <div class="tab-pane fade" id="otherIncome1">
            <p>The 4th content</p>
            <input type="submit" value="submit" />
        </div>
    </div>
    </form>
</div>-->