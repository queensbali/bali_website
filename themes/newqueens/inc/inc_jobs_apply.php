<script type="text/javascript">
	$(function() {
		$("#datepicker").datepicker({
		  dateFormat:"yy-mm-dd",
		  changeMonth: true,
		  changeYear: true,
		});
	});
	function refreshCaptcha(){
		var img = document.images['captchaimg'];
		img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
	}
</script>
<!--<ol class="breadcrumb">
  <li><a href="#" onClick="history.go(-1); return false;">Home</a></li>
  <li class="active"><?php //echo $theme->geturl('1'); ?></li>
</ol>-->
<div class="row">
	<div class="col-md-12">
        <form role="form" class="form-horizontal" method="post" action="<?php $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
          <div class="form-group">
        	<label for="fname" class="col-sm-2 control-label">Fullname</label>
            <div class="col-sm-5">
            <input name="fname" type="text" class="form-control" id="fullname" placeholder="Insert your first name.." required>
            </div>
            <div class="col-sm-5">
            <input name="lname" type="text" class="form-control" id="fullname" placeholder="Insert your last name..">
            </div>
          </div>
          <div class="form-group">
        	<label for="address" class="col-sm-2 control-label">Address</label>
            <div class="col-sm-10">
            <input name="address" type="text" class="form-control" id="address" placeholder="Insert your full address.." required>
            </div>
          </div>
          <div class="form-group">
        	<label for="city" class="col-sm-2 control-label">City/Province/Postal</label>
            <div class="col-sm-4">
            <input name="city" type="text" class="form-control" id="city" placeholder="Insert city.." required>
            </div>
            <div class="col-sm-4">
            <input name="province" type="text" class="form-control" id="province" placeholder="Insert province.." required>
            </div>
            <div class="col-sm-2">
            <input name="postal" type="text" class="form-control" id="postal" placeholder="Insert postal code..">
            </div>
          </div>
          <div class="form-group">
        	<label for="phone" class="col-sm-2 control-label">Phone/Mobile</label>
            <div class="col-sm-5">
            <input name="phone" type="text" class="form-control" id="phone" placeholder="Insert phone number..">
            </div>
            <div class="col-sm-5">
            <input name="mobile" type="text" class="form-control" id="mobile" placeholder="Insert mobile number.." required>
            </div>
          </div>
          <div class="form-group">
        	<label for="email" class="col-sm-2 control-label">Email Address</label>
            <div class="col-sm-10">
            <input name="email" type="text" class="form-control" id="email" placeholder="Insert email address.." required>
            </div>
          </div>
          <div class="form-group">
        	<label for="position" class="col-sm-2 control-label">Position</label>
            <div class="col-sm-10">
            <input name="position" type="text" class="form-control" id="position" placeholder="Insert your position.." required>
            </div>
          </div>
          <!--<div class="form-group">
        	<label for="attach" class="col-sm-2 control-label">Attach CV</label>
            <div class="col-sm-10">
            <input name="attach" type="file" />
            </div>
          </div>-->
          <div class="form-group">
        	<label for="note" class="col-sm-2 control-label">Note</label>
            <div class="col-sm-10">
            <textarea name="note" cols="" rows="" class="form-control" id="note" placeholder="Insert note.."></textarea>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
            <img src="<?php $theme->linkurl('../../captcha/captcha.php'); ?>?rand=<?php echo rand(); ?>" id='captchaimg' ><br />
            <small>Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh</small>
            </div>
          </div>
          <div class="form-group">
            <label for="inputSubject" class="col-sm-2 control-label">Verify Code</label>
            <div class="col-sm-10">
              <input name="vcode" type="text" class="form-control" id="inputSubject" placeholder="Insert validation code.." required>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" name="submitjob" class="btn btn-warning">Submit Form</button>
              <button type="reset" class="btn btn-default">Clear Form</button>
            </div>
          </div>
        </form>
    </div>
</div>
<?php
	$datein=date("Y-m-d");
	$fname=$_POST['fname'];
	$lname=$_POST['lname'];
	$address=$_POST['address'];
	$city=$_POST['city'];
	$province=$_POST['province'];
	$postal=$_POST['postal'];
	$phone=$_POST['phone'];
	$mobile=$_POST['mobile'];
	$email=$_POST['email'];
	$position=$_POST['position'];
	$note=$_POST['note'];
	$vcode=$_POST['vcode'];
	$att_name=$_FILES['attach']['name'];
	$att_tmp=$_FILES['attach']['tmp_name'];
	$att_type=$_FILES['attach']['type'];
	
	if(isset($_POST['submitjob'])){
		if($_SESSION['6_letters_code']!=$vcode){
			echo'<script type="text/javascript">alert("Code validation not valid..");</script>';
		}else {
			$pdfurl=$theme->incurl('jobpdf/').$att_name;
			$dt=date('Y');
			$idjobs=no_auto("tb_job_apply",'APP'.$dt);
			$save=mysql_query("INSERT INTO tb_job_apply(id_jobstaff, datein, fname, lname, address, city, province, postal, position, attach, phone, mobile, email, note, view) VALUES ('$idjobs','$datein','$fname','$lname','$address','$city','$province','$postal','$position','$att_name','$phone','$mobile','$email','$note','0')");
			//$move=move_uploaded_file($att_tmp, $pdfurl);
			
			require_once($theme->incurl('../../').'./themes/plugin/phpmailer/class.phpmailer.php');
			require_once($theme->incurl('../../').'./themes/plugin/phpmailer/class.smtp.php'); 
			$mail = new PHPMailer();		
			//SMTP Class
			$mail->IsSMTP(); //set untuk menggunakan SMTP
			$mail->SMTPDebug  = 1;                     
			//SMTP Setting
			$mail->SMTPAuth   = true;                  
			$mail->SMTPSecure = "tls";                 
			$mail->Host       = "smtp.gmail.com";      
			$mail->Port       = 587; 
			$mail->Username   = "noreply@queenstandoor.com";
			$mail->Password   = "queens1976"; 
			//Email From
			$mail->SetFrom("noreply@queenstandoor.com", "noreply@queenstandoor.com");
			$mail->AddReplyTo("noreply@queenstandoor.com","noreply@queenstandoor.com");
			$mail->Subject="Job Application";
			$mail->AltBody="To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
			//Email Letter
			ob_start();
			require_once($theme->incurl('../../').'./themes/email/job-apply.php');
			$html_message = ob_get_contents();
			ob_end_clean();
			//Email HTML Content
			$mail->MsgHTML($html_message);
			$mail->IsHTML(true); 
			$mail->AddAttachment($pdfurl);
			//Email To
			$mail->AddAddress('hrd1.bali@queenstandoor.com','Queens HRD');
			$mail->AddCC('puneet@queenstandoor.com', 'Puneet Malhotra');
			$mail->AddCC('sec1.bali@queenstandoor.com', 'Secretary Queens');
			$mail->AddBCC('dsg1_qt1_bali@yahoo.com','Jonny Cible');
			if(!$mail->Send()) {
			  echo "Mailer Error: " . $mail->ErrorInfo;
			} else { }
			
			if($save){
				echo '<script type="text/javascript">alert("Suucess, Your application has been sent,..");</script>';
			}else {
				echo '<script type="text/javascript">alert("Error, Your application cannot be send,..");</script>';
			}
		}
	}
?>