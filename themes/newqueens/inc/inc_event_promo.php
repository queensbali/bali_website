<script type='text/javascript'>
	$(document).ready(function() {
		var calendar = $('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			selectable: false,
			selectHelper: true,
			select: function(start, end, allDay) {
				var title = prompt('Event Title:');
				if (title) {
					calendar.fullCalendar('renderEvent',
						{
							title: title,
							start: start,
							end: end,
							allDay: allDay
						},
						true // make the event "stick"
					);
				}
				calendar.fullCalendar('unselect');
			},
			editable: false,
			<!--timeFormat: 'HH:mm ',--> // uppercase H for 24-hour clock
			agenda: 'HH:mm', // 5:00 - 6:30
			events: "<?php $theme->linkurl("function/check_event.php"); ?>",
			eventColor: 'green'
		});		
	});
</script>

<div class="row">
    <div class="col-md-12">
    <div id="calendar"></div>
    </div>
</div>