<style type="text/css">
.bs-modal-lg .modal-dialog {
  width: 85%;
}
.bs-modal-lg .modal-body {
  overflow-y: auto;
}
</style>
<script language="JavaScript" type="text/javascript">
	function refreshCaptcha(){
		var img = document.images['captchaimg'];
		img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
	}
</script>
<form role="form" class="form-horizontal" action="<?php $_SERVER['PHP_SELF']; ?>" method="post" name="credit">
  <div class="bs-callout bs-callout-warning">
    <h4>Company Details</h4>
  </div>
  <div class="form-group">
	<label for="amount" class="col-sm-3 control-label">Company</label>
    <div class="col-sm-9">
    <input name="company" type="text" id="company" class="form-control" placeholder="Company" value="<?php echo $_COOKIE['company']; ?>" required>
    </div>
  </div>
  <div class="form-group">
	<label for="amount" class="col-sm-3 control-label">Address</label>
    <div class="col-sm-9">
    <input name="caddress" type="text" id="caddress" class="form-control" placeholder="Company Address" value="<?php echo $_COOKIE['caddress']; ?>" required>
    </div>
  </div>
  <div class="form-group">
	<label for="amount" class="col-sm-3 control-label">Nationality</label>
    <div class="col-sm-9">
    <select name="country" class="form-control" id="country" placeholder="Country" required>
	  <?php include "themes/newqueens/inc/inc_country.php"; ?>
    </select>
    </div>
  </div>
  <div class="form-group">
	<label for="amount" class="col-sm-3 control-label">City / State / Code</label>
    <div class="col-sm-3">
    <input type="text" name="city" id="city" class="form-control" placeholder="City" value="<?php echo $_COOKIE['city']; ?>" required>
    </div>
    <div class="col-sm-3">
    <input type="text" name="state" id="state" class="form-control" placeholder="State" value="<?php echo $_COOKIE['state']; ?>" required>
    </div>
    <div class="col-sm-3">
    <input type="text" name="postal" id="postal" class="form-control" placeholder="Postal Code" value="<?php echo $_COOKIE['postal']; ?>">
    </div>
  </div>
  <div class="form-group">
	<label for="amount" class="col-sm-3 control-label">Phone / Fax</label>
    <div class="col-sm-5">
    <input type="text" name="cphone" id="cphone" class="form-control" placeholder="Phone Number" value="<?php echo $_COOKIE['cphone']; ?>" required>
    </div>
    <div class="col-sm-4">
    <input type="text" name="cfax" id="cfax" class="form-control" placeholder="Fax Number" value="<?php echo $_COOKIE['cfax']; ?>">
    </div>
  </div>
  <div class="form-group">
	<label for="amount" class="col-sm-3 control-label">Email</label>
    <div class="col-sm-9">
    <input name="cmail" type="email" id="cmail" class="form-control" placeholder="Email Address" value="<?php echo $_COOKIE['cmail']; ?>" required>
    </div>
  </div>
  <div class="bs-callout bs-callout-warning">
    <h4>Account Details</h4>
  </div>
  <div class="form-group">
	<label for="amount" class="col-sm-3 control-label">Title</label>
    <div class="col-sm-9">
    <select name="atitle" id="atitle" class="form-control">
      <option value="Mr.">Mr.</option>
      <option value="Mrs.">Mrs.</option>
      <option value="Ms.">Ms.</option>
    </select>
    </div>
  </div>
  <div class="form-group">
	<label for="amount" class="col-sm-3 control-label">Account Payable</label>
    <div class="col-sm-9">
    <input type="text" name="aname" id="aname" class="form-control" placeholder="Name of Account Payable" value="<?php echo $_COOKIE['aname']; ?>" required>
    </div>
  </div>
  <div class="form-group">
	<label for="amount" class="col-sm-3 control-label">Company Registration</label>
    <div class="col-sm-9">
    <input type="text" name="aregistration" id="aregistration" class="form-control" placeholder="Number of Company Registration" value="<?php echo $_COOKIE['aregistration']; ?>" required>
    </div>
  </div>
  <div class="form-group">
    <label for="company" class="col-sm-3 control-label">Type</label>
    <div class="col-sm-9">
    <label class="radio-inline ">
      <input type="radio" name="type" id="radio" value="Corporation" required="required"> Corporation
    </label>
    <label class="radio-inline ">
      <input type="radio" name="type" id="radio2" value="Partnership"> Partnership
    </label>
    <label class="radio-inline ">
      <input type="radio" name="type" id="radio3" value="Individual"> Individual
    </label>
    </div>
  </div>
  <div class="bs-callout bs-callout-warning">
    <h4>Account Details</h4>
  </div>
  <div class="tabbable"> <!-- Only required for left/right tabs -->
    <ul class="nav nav-tabs">
        <li class="active"><a href="#own1" data-toggle="tab">Ownership I Details</a></li>
        <li><a href="#own2" data-toggle="tab">Ownership II Details</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="own1">
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Title</label>
            <div class="col-sm-9">
            <select name="otitle1" id="otitle" class="form-control">
              <option value="Mr.">Mr.</option>
              <option value="Mrs.">Mrs.</option>
              <option value="Ms.">Ms.</option>
            </select>
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Fullname</label>
            <div class="col-sm-9">
            <input type="text" name="oname1" id="oname1" class="form-control" placeholder="Fullname" value="<?php echo $_COOKIE['oname1']; ?>" required>
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Address</label>
            <div class="col-sm-9">
            <input name="oaddress1" type="text" id="oaddress1" class="form-control" placeholder="Full Address" value="<?php echo $_COOKIE['oaddress1']; ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Phone / Mobile</label>
            <div class="col-sm-9">
            <input type="text" name="ophone1" id="ophone1" class="form-control" placeholder="Phone / Mobile Number" value="<?php echo $_COOKIE['ophone1']; ?>" required>
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Email Address</label>
            <div class="col-sm-9">
            <input type="email" name="omail1" id="omail1" class="form-control" placeholder="Email Address" value="<?php echo $_COOKIE['omail1']; ?>" required>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="own2">
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Title</label>
            <div class="col-sm-9">
            <select name="otitle2" id="otitle2" class="form-control">
              <option value="Mr.">Mr.</option>
              <option value="Mrs.">Mrs.</option>
              <option value="Ms.">Ms.</option>
            </select>
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Fullname</label>
            <div class="col-sm-9">
            <input type="text" name="oname2" id="oname2" class="form-control" placeholder="Fullname" value="<?php echo $_COOKIE['oname2']; ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Address</label>
            <div class="col-sm-9">
            <input name="oaddress2" type="text" id="oaddress2" class="form-control" placeholder="Full Address" value="<?php echo $_COOKIE['oaddress2']; ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Phone / Mobile</label>
            <div class="col-sm-9">

            <input type="text" name="ophone2" id="ophone2" class="form-control" placeholder="Phone / Mobile Number" value="<?php echo $_COOKIE['ophone2']; ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Email Address</label>
            <div class="col-sm-9">
            <input type="email" name="omail2" id="omail2" class="form-control" placeholder="Email Address" value="<?php echo $_COOKIE['omail2']; ?>">
            </div>
          </div>
        </div>
    </div>
  </div>
    
  <div class="tabbable"><!-- Only required for left/right tabs -->
    <ul class="nav nav-tabs">
        <li class="active"><a href="#gm1" data-toggle="tab">General / Operational Manager</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="gm1">
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Title</label>
            <div class="col-sm-9">
            <select name="mtitle" id="mtitle" class="form-control">
              <option value="Mr.">Mr.</option>
              <option value="Mrs.">Mrs.</option>
              <option value="Ms.">Ms.</option>
            </select>
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Fullname</label>
            <div class="col-sm-9">
            <input type="text" name="mname" id="mname" class="form-control" placeholder="Fullname" value="<?php echo $_COOKIE['mname']; ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Address</label>
            <div class="col-sm-9">
            <input name="maddress" type="text" id="maddress" class="form-control" placeholder="Full Address" value="<?php echo $_COOKIE['maddress']; ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Phone / Mobile</label>
            <div class="col-sm-9">
            <input type="text" name="mphone" id="mphone" class="form-control" placeholder="Phone / Mobile Number" value="<?php echo $_COOKIE['mphone']; ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Email Address</label>
            <div class="col-sm-9">
            <input type="email" name="mmail" id="mmail" class="form-control" placeholder="Email Address" value="<?php echo $_COOKIE['mmail']; ?>">
            </div>
          </div>
        </div>
    </div>
  </div>
        
  <div class="tabbable"> <!-- Only required for left/right tabs -->
    <ul class="nav nav-tabs">
        <li class="active"><a href="#acc1" data-toggle="tab">Acc I References</a></li>
        <li><a href="#acc2" data-toggle="tab">Acc II References</a></li>
        <li><a href="#acc3" data-toggle="tab">Acc III References</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="acc1">
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Fullname</label>
            <div class="col-sm-9">
            <input type="text" name="accname1" id="accname1" class="form-control" placeholder="Fullname" value="<?php echo $_COOKIE['accname1']; ?>" required>
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Contact Name</label>
            <div class="col-sm-9">
            <input type="text" name="acccontact1" id="acccontact1" class="form-control" placeholder="Contact Name" value="<?php echo $_COOKIE['acccontact1']; ?>" required>
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Address</label>
            <div class="col-sm-9">
            <input name="accaddress1" type="text" id="accaddress1" class="form-control" placeholder="Full Address" value="<?php echo $_COOKIE['accaddress1']; ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Phone / Mobile</label>
            <div class="col-sm-9">
            <input type="text" name="accphone1" id="accphone1" class="form-control" placeholder="Phone / Mobile Number" value="<?php echo $_COOKIE['accphone1']; ?>" required>
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Email Address</label>
            <div class="col-sm-9">
            <input type="email" name="accmail1" id="accmail1" class="form-control" placeholder="Email Address" value="<?php echo $_COOKIE['accmail1']; ?>" required>

            </div>
          </div>
        </div>
        <div class="tab-pane" id="acc2">
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Fullname</label>
            <div class="col-sm-9">
            <input type="text" name="accname2" id="accname2" class="form-control" placeholder="Fullname" value="<?php echo $_COOKIE['accname2']; ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Contact Name</label>
            <div class="col-sm-9">
            <input type="text" name="acccontact2" id="acccontact2" class="form-control" placeholder="Contact Name" value="<?php echo $_COOKIE['acccontact2']; ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Address</label>
            <div class="col-sm-9">
            <input name="accaddress2" type="text" id="accaddress2" class="form-control" placeholder="Full Address" value="<?php echo $_COOKIE['accadress2']; ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Phone / Mobile</label>
            <div class="col-sm-9">
            <input type="text" name="accphone2" id="accphone2" class="form-control" placeholder="Phone / Mobile Number" value="<?php echo $_COOKIE['accphone2']; ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Email Address</label>
            <div class="col-sm-9">
            <input type="email" name="accmail2" id="accmail2" class="form-control" placeholder="Email Address" value="<?php echo $_COOKIE['accmail2']; ?>">
            </div>
          </div>
        </div>
        <div class="tab-pane" id="acc3">
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Fullname</label>
            <div class="col-sm-9">
            <input type="text" name="accname3" id="accname3" class="form-control" placeholder="Fullname" value="<?php echo $_COOKIE['accname3']; ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Contact Name</label>
            <div class="col-sm-9">
            <input type="text" name="acccontact3" id="acccontact3" class="form-control" placeholder="Contact Name" value="<?php echo $_COOKIE['acccontact3']; ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Address</label>
            <div class="col-sm-9">
            <input name="accaddress3" type="text" id="accaddress3" class="form-control" placeholder="Full Address" value="<?php echo $_COOKIE['accaddress3']; ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Phone / Mobile</label>
            <div class="col-sm-9">
            <input type="text" name="accphone3" id="accphone3" class="form-control" placeholder="Phone / Mobile Number" value="<?php echo $_COOKIE['accphone3']; ?>">
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-3 control-label">Email Address</label>
            <div class="col-sm-9">
            <input type="email" name="accmail3" id="accmail3" class="form-control" placeholder="Email Address" value="<?php echo $_COOKIE['accmail3']; ?>">
            </div>
          </div>
       </div>
    </div>
  </div>
  <div class="bs-callout bs-callout-warning">
  <h4>Bank References</h4>
  </div>
  <div class="form-group">
    <label for="company" class="col-sm-3 control-label">Name</label>
    <div class="col-sm-9">
    <input type="text" name="bname" id="bname" class="form-control" placeholder="Name" value="<?php echo $_COOKIE['bname']; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="company" class="col-sm-3 control-label">Address</label>
    <div class="col-sm-9">
    <input name="baddress" type="text" id="baddress" class="form-control" placeholder="Address" value="<?php echo $_COOKIE['baddress']; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="company" class="col-sm-3 control-label">Office</label>
    <div class="col-sm-9">
    <input type="text" name="boffice" id="boffice" class="form-control" placeholder="Office" value="<?php echo $_COOKIE['boffice']; ?>">

    </div>
  </div>
  <div class="form-group">
    <label for="company" class="col-sm-3 control-label">Account Number</label>
    <div class="col-sm-9">
    <input type="text" name="baccno" id="baccno" class="form-control" placeholder="Account Number" value="<?php echo $_COOKIE['baccno']; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="company" class="col-sm-3 control-label">Phone Number</label>
    <div class="col-sm-9">
    <input type="text" name="bphone" id="bphone" class="form-control" placeholder="Phone Number" value="<?php echo $_COOKIE['bphone']; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="company" class="col-sm-3 control-label">Payment Mode</label>
    <div class="col-sm-9">
    <label class="radio-inline ">
      <input type="radio" name="type" id="radio" value="Cash" required="required"> Cash
    </label>
    <label class="radio-inline ">
      <input type="radio" name="type" id="radio2" value="Credit"> Credit
    </label>
    <label class="radio-inline ">
      <input type="radio" name="type" id="radio3" value="Bank Transfers"> Bank Transfer
    </label>
    </div>
  </div>
  <div class="bs-callout bs-callout-warning">
  <h4>Terms & Condition</h4>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
    <img src="<?php $theme->linkurl('../../captcha/captcha.php'); ?>?rand=<?php echo rand(); ?>" id='captchaimg' ><br />
    <small>Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh</small>
    </div>
  </div>
  <div class="form-group">
    <label for="inputSubject" class="col-sm-3 control-label">Verify Code</label>
    <div class="col-sm-9">
      <input name="vcode" type="text" class="form-control" id="inputSubject" placeholder="Insert validation code.." required>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
      <div class="checkbox">
        <label>
          <input type="checkbox" name="subscribe" value="Yes"> Subscribe to our mailing list
        </label>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
      <div class="checkbox">
        <label>
          <input type="checkbox" name="term" value="Yes" required="required"> I have read <a href="#" data-toggle="modal" data-target="#myModal">terms & condition</a>
        </label>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
      <button type="submit" name="submitlocal" class="btn btn-warning">Submit Form</button>
      <button type="reset" class="btn btn-default">Clear Form</button>
    </div>
  </div>
</form>
Your IP : <font color="#FF0000"><?php echo $_SERVER['REMOTE_ADDR']; ?></font>
<!-- Modal -->
<div class="modal fade bs-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Terms & Condition</h4>
      </div>
      <div class="modal-body">
      <?php include $theme->incurl('inc/').'inc_terms_condition.php'; ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php
	if(isset($_POST['submitlocal'])){
		$datein=date('Y-m-d');
		$company=$_POST['company']; setcookie('company',$company, time()+300);
		$caddress=$_POST['caddress']; setcookie('caddress', $caddress, time()+300);
		$country=$_POST['country'];
		$city=$_POST['city']; setcookie('city', $city, time()+300);
		$state=$_POST['state']; setcookie('state', $state, time()+300);
		$postal=$_POST['postal']; setcookie('postal', $postal, time()+300);
		$cphone=$_POST['cphone']; setcookie('cphone', $cphone, time()+300);
		$cfax=$_POST['cfax']; setcookie('cfax', $cfax, time()+300);
		$cmail=$_POST['cmail']; setcookie('cmail', $cmail, time()+300);
		$atitle=$_POST['atitle']; setcookie('atitle', $atitle, time()+300);
		$aname=$_POST['aname']; setcookie('aname', $aname, time()+300);
		$aregistration=$_POST['aregistration']; setcookie('aregistration', $aregistration, time()+300);
		$asitano=$_POST['asita']; setcookie('asita', $asitano, time()+300);
		$apatano=$_POST['apata']; setcookie('apata', $apatano, time()+300);
		$type=$_POST['type'];
		
		$otitle1=$_POST['otitle1']; setcookie('otitle1', $otitle1, time()+300);
		$oname1=$_POST['oname1']; setcookie('oname1', $oname1, time()+300);
		$oaddress1=$_POST['oaddress1']; setcookie('oaddress1', $oaddress1, time()+300);
		$ophone1=$_POST['ophone1']; setcookie('ophone1', $ophone1, time()+300);
		$omail1=$_POST['omail1']; setcookie('omail1', $omail1, time()+300);
		$otitle2=$_POST['otitle2']; setcookie('otitle2', $otitle2, time()+300);
		$oname2=$_POST['oname2']; setcookie('oname2', $oname2, time()+300);
		$oaddress2=$_POST['oaddress2']; setcookie('oaddress2', $oaddress2, time()+300);
		$ophone2=$_POST['ophone2']; setcookie('ophone2', $ophone2, time()+300);
		$omail2=$_POST['omail2']; setcookie('omail2', $omail2, time()+300);
		
		$mtitle=$_POST['mtitle']; setcookie('mtitle', $mtitle, time()+300);
		$mname=$_POST['mname']; setcookie('mname', $mname, time()+300);
		$maddress=$_POST['maddress']; setcookie('maddress', $maddress, time()+300);
		$mphone=$_POST['mphone']; setcookie('mphone', $mphone, time()+300);
		$mmail=$_POST['mmail']; setcookie('mmail', $mmail, time()+300);
		
		$accname1=$_POST['accname1']; setcookie('accname1', $accname1, time()+300);
		$acccontact1=$_POST['acccontact1']; setcookie('acccontact1', $acccontact1, time()+300);
		$accaddress1=$_POST['accaddress1']; setcookie('accaddress1', $accaddress1, time()+300);
		$accphone1=$_POST['accphone1']; setcookie('accphone1', $accphone1, time()+300);
		$accmail1=$_POST['accmail1']; setcookie('accmail1', $accmail1, time()+300);
		$accname2=$_POST['accname2']; setcookie('accname2', $accname2, time()+300);
		$acccontact2=$_POST['acccontact2']; setcookie('acccontact2', $acccontact2, time()+300);
		$accaddress2=$_POST['accaddress2']; setcookie('accaddress2', $accaddress2, time()+300);
		$accphone2=$_POST['accphone2']; setcookie('accphone2', $accphone2, time()+300);
		$accmail2=$_POST['accmail2']; setcookie('accmail2', $accmail2, time()+300);
		$accname3=$_POST['accname3']; setcookie('accname3', $accname3, time()+300);
		$acccontact3=$_POST['acccontact3']; setcookie('acccontact3', $acccontact3, time()+300);
		$accaddress3=$_POST['accaddress3']; setcookie('accaddress3', $accaddress3, time()+300);
		$accphone3=$_POST['accphone3']; setcookie('accphone3', $accphone3, time()+300);
		$accmail3=$_POST['accmail3']; setcookie('accmail3', $accmail3, time()+300);
		
		$bname=$_POST['bname']; setcookie('bname', $bname, time()+300);
		$baddress=$_POST['baddress']; setcookie('baddress', $baddress, time()+300);
		$boffice=$_POST['boffice']; setcookie('boffice', $boffice, time()+300);
		$baccno=$_POST['baccno']; setcookie('baccno', $baccno, time()+300);
		$bphone=$_POST['bphone']; setcookie('bphone', $bphone, time()+300);
		$ipaddress=$_SERVER['REMOTE_ADDR'];
		$captcha=$_POST['vcode'];

		if($_SESSION['6_letters_code']!=$captcha){
			echo'<script type="text/javascript">alert("Code validation not valid..");</script>';
			header("Location:".$theme->link('../../credit-form-local')."");
		}else {
			include "./themes/function/random_code.php";
			$dt=date('my');
			$idcredit=no_auto("tb_credit",'CL'.$dt);
			$save=mysql_query("INSERT INTO tb_credit(id_credit, datein, tcredit, company, caddress, country, city, state, postal, cphone, cfax, cmail, atitle, aname, aregistration, type, otitle1, oname1, oaddress1, ophone1, omail1,  otitle2, oname2, oaddress2, ophone2, omail2, mtitle, mname, maddress, mphone, mmail, accname1, acccontact1, accaddress1, accphone1, accmail1, accname2, acccontact2, accaddress2, accphone2, accmail2, accname3, acccontact3, accaddress3, accphone3, accmail3, bname, baddress, boffice, baccno, bphone, ipaddress, codever, aktif) VALUES ('$idcredit','$datein','HOTEL/VILLA/DELIVERY/CATERING/LOCAL','$company','$caddress','$country','$city','$state','$postal','$cphone','$cfax','$cmail','$atitle','$aname','$aregistration','$type','$otitle1','$oname1','$oaddress1','$ophone1','$omail1','$otitle2','$oname2','$oaddress2','$ophone2','$omail2','$mtitle','$mname','$maddress','$mphone','$mmail','$accname1','$acccontact1','$accaddress1','$accphone1','$accmail1','$accname2','$acccontact2','$accaddress2','$accphone2','$accmail2','$accname3','$acccontact3','$accaddress3','$accphone3','$accmail3','$bname','$baddress','$boffice','$baccno','$bphone','$ipaddress','$codever','No')");
			
			require_once('./themes/plugin/phpmailer/class.phpmailer.php');
			require_once('./themes/plugin/phpmailer/class.smtp.php');  
			$mail = new PHPMailer();		
			//SMTP Class
			$mail->IsSMTP(); //set untuk menggunakan SMTP
			$mail->SMTPDebug  = 1;                     
			//SMTP Setting
			$mail->SMTPAuth   = true;                  
			$mail->SMTPSecure = "tls";                 
			$mail->Host       = "smtp.gmail.com";      
			$mail->Port       = 587; 
			$mail->Username   = "noreply@queenstandoor.com";
			$mail->Password   = "queens1976"; 
			//Email From
			$mail->SetFrom("noreply@queenstandoor.com", "noreply@queenstandoor.com");
			$mail->AddReplyTo("noreply@queenstandoor.com","noreply@queenstandoor.com");
			$mail->Subject="Thank for Credit Request Form";
			$mail->AltBody="To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
			//Email Letter
			ob_start();
			require_once('./themes/email/credit-form-letter.php');
			$html_message = ob_get_contents();
			ob_end_clean();
			//Email HTML Content
			$mail->MsgHTML($html_message);
			$mail->IsHTML(true); 
			//Email To
			$mail->AddAddress($cmail, $company);
			/*$mail->AddCC('puneet@queenstandoor.com', 'Puneet Malhotra');*/
			$mail->AddBCC('jonny_hds@yahoo.com','Jonny Cible');
			if(!$mail->Send()) {
			  echo "Mailer Error: " . $mail->ErrorInfo;
			} else { }   
			
			$mail2 = new PHPMailer();
			//SMTP Class
			$mail2->IsSMTP(); //set untuk menggunakan SMTP
			$mail2->SMTPDebug  = 1;                     
			//SMTP Setting
			$mail2->SMTPAuth   = true;                  
			$mail2->SMTPSecure = "tls";                 
			$mail2->Host       = "smtp.gmail.com";      
			$mail2->Port       = 587; 
			$mail2->Username   = "noreply@queenstandoor.com";
			$mail2->Password   = "queens1976"; 
			//Email From
			$mail2->SetFrom("noreply@queenstandoor.com", "noreply@queenstandoor.com");
			$mail2->AddReplyTo("noreply@queenstandoor.com","noreply@queenstandoor.com");
			$mail2->Subject="Your Detail Data Account";
			$mail2->AltBody="To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test 	//Email Letter
			ob_start();
			require_once('./themes/email/credit-form-local.php');
			$html_message2 = ob_get_contents();
			ob_end_clean();
			//Email HTML Content
			$mail2->MsgHTML($html_message2);
			$mail2->IsHTML(true); 
			//Email To
			$mail2->AddAddress($cmail, $company);
			$mail2->AddCC('bali2@queenstandoor.com', 'Marketing Queens');
			$mail2->AddCC('ca1.bali@queenstandoor.com', 'Financial Controller');
			$mail2->AddCC('ar1.bali@queenstandoor.com', 'Account Receivable');
			$mail2->AddCC('puneet@queenstandoor.com', 'Puneet Malhotra');
			$mail2->AddBCC('jonny_hds@yahoo.com','Jonny Cible');
			
			if(!$mail2->Send()) {
			  echo "Mailer Error: " . $mail2->ErrorInfo;
			} else {}   
			
			if($save){
				echo'<script type="text/javascript">alert("Thank You!, Your request has been received..");</script>';
				foreach($_COOKIE as $key => $value) {
					 setcookie($key,$value,time()-10000,"/","bali.queenstandoor.com");
				}
			}else {
				echo'<script type="text/javascript">alert("Error, Request not complete..");</script>';
			}
		}
	}
?>