<?php
	require_once "./themes/function/func_theme.php";
	$theme=new theme();
	
	$linktab=$_SERVER['REQUEST_URI'];
	$pchtab=explode('/',$linktab);
	$tcont=ucwords(str_replace('-',' ',$pchtab[1]));
	$tmenu=ucwords(str_replace('-',' ',$pchtab[2]));
	$tab=ucwords(str_replace('-',' ',$pchtab[3]));
	$det=ucwords(str_replace('-',' ',$pchtab[4]));
?>
<div class="row">
	<div class="col-md-3 sideleft hidden-xs hidden-sm">
       	<div id="sidebar">
		<?php require_once "sidebar-page.php"; ?>
        </div>
	</div>
    <div class="col-md-9">
        <div class="hidden-xs" style="margin-bottom:10px;">
        <?php require_once "themes/newqueens/banner-promo.php"; ?>
        </div>
        <div class="tabbable"> <!-- Only required for left/right tabs -->
            <ul class="nav nav-tabs">
            <?php
                $selmenu=mysql_fetch_array(mysql_query("SELECT id_menu FROM tb_navigator WHERE title='$tmenu'"));
                $idmenu=$selmenu['id_menu'];
                
                $seltab=mysql_query("SELECT * FROM tb_contentsub WHERE id_menu='$idmenu' AND publish='Yes' ORDER BY tabpos ASC");
                while($viewtab=mysql_fetch_array($seltab)){
					$no++;
                    if($tab==''){
						$active=$viewtab['default'];
					}
					else if($tab==$viewtab['tabtitle']){
                        $active='active';
                    }
                    else {
                        $active='';
                    }
            ?>
            <li class='<?=$active; ?>'><a href="#tab<?=$no;?>" data-toggle="tab"><?php echo $viewtab['tabtitle']; ?></a></li>
            <?php } ?>
            </ul>
            <div class="tab-content">
                <?php
					$selcont=mysql_query("SELECT * FROM tb_contentsub WHERE id_menu='$idmenu' AND publish='Yes' ORDER BY tabpos ASC");
					while($viewcont=mysql_fetch_array($selcont)){
						$noo++;
						if($tab==''){
							$empt=$viewcont['default'];
						}
						else if($tab==$viewcont['tabtitle']){
							$active='active';
						}
						else {
							$active='';
						}
				?>
                <div class="tab-pane <?=$active;?> <?=$empt;?>" id="tab<?=$noo;?>" style="margin-top:10px;">
                <article-<?=$noo;?>>
				<?php
					if(!empty($viewcont['filename'])){
						include "".$theme->incurl('').'inc/'.$viewcont['filename']."";
					}else {
						echo $viewcont['content'];
					}
				?>
                </article>
                </div>
                <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>-->
				<!--<script src="<?php //$theme->linkurl('../plugin/readmore/readmore.js'); ?>"></script>
                <script type="text/javascript">
                    $('article-<?php //echo $noo;?>').readmore({maxHeight: 140});
                </script>-->
                <?php } ?>
            </div>
        </div>
	</div>
</div>