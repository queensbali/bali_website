<?php

//first include the My-Cool-SMS Starter Class
require_once('./MyCoolSMS.class.php');

//instantiate a MyCoolSMS object
$oMyCoolSMS = new MyCoolSMS();

//invoke the sendSms method
$oResponse = $oMyCoolSMS->sendSms('+12309876543', 'Have a nice day!');

/*
	$oResponse is now and object and you can use it like:
	$oResponse->success
	$oResponse->smsid
	$oResponse->balance
	etc.
*/

//handle the response
if($oResponse->success) {
    //Great, it worked!
    print_r($oResponse);
} else {
    //Oops, Something went wrong...
    print_r($oResponse);
}

?>