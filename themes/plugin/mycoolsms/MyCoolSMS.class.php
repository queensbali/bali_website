<?php

class MyCoolSMS {

    function __construct() {
    
        $this->username = 'puneet'; //your username here...
        $this->password = 'queens1986'; //your password here...
        $this->endpoint = 'http://www.my-cool-sms.com/api-socket.php';
    }

    public function sendSms($number, $message, $senderid='+6281249249249') {
		
        return($this->request(json_encode(array(
            'username' => $this->username,
            'password' => $this->password,
            'function' => 'sendSms',
            'number' => $number,
            'senderid' => $senderid,
            'message' => $message
        ))));
		
		//extend this function as you please...
        
    }
    
	// you can handle other api functions within this class too
	
    public function getDeliveryReport() {}
    
    public function getBalance() {}
    
    //add more api functions as you please...
    
    private function request($oRequest) {
    	
		//if your server doesn't support curl you can use fsockopen instead
        $oCurl = curl_init($this->endpoint);
        curl_setopt($oCurl, CURLOPT_POST, 1);
        curl_setopt($oCurl, CURLOPT_POSTFIELDS, $oRequest);
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);
        
        if(curl_errno($oCurl) == 0) {
			//the connection to the api endpoint was succesful
			
			//let's store the response in a variable...
            $oResponse = json_decode(curl_exec($oCurl));
			
            if(!is_object($oResponse)) {
				//hmm.. it seems that the response by the endpoint was invalid (i.e. no JSON object)
                $oResponse = $this->getError('001', 'Bad Response');
            }
			
        } else {
			
			//couldn't establish a connection...
            $oResponse = $this->getError(curl_error($oCurl));
			
        }		
    
        curl_close($oCurl);
        
        return $oResponse;
    
    }
	
    private function getError($error = '000', $description = NULL) {

        return json_encode(array(
            'success' => false,
            'error' => $error,
            'description' => $description,
        ));
        
    }

}

?>