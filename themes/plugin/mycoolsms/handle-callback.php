<?php
//you can save this file on your server and use it as your API endpoint for callbacks

$oCallback = json_decode(file_get_contents("php://input"));

/*
	$oCallback is now and object and you can use it like:
	$oResponse->smsid
	$oResponse->status
	etc.
*/

//handle the callback
if(is_object($oCallback)) {
    //Great, it worked!
    print_r($oCallback);
} else {
    //Oops, Something went wrong... (i.e. the URL was not invoked by My-Cool-SMS but i.e. a browser or crawler.) Simply ignore the request.
}

?>