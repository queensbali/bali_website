<?php require_once $theme->incurl('../../')."config/connect.php"; ?>
<table width="100%" border="0" cellspacing="0" cellpadding="70" bgcolor="#FF9900">
  <tr>
    <td>
    <table width="700" border="0" cellspacing="0" cellpadding="0" style="color:#FFF; margin-bottom:10px; font-size:12px;" align="center">
      <tr>
        <td align="left" valign="top">
        Home Delivery, <?php echo date('F Y'); ?>
        </td>
        <td align="right" valign="top">
        <a href="http://twitter.com/queensbali" target="_blank" style="color:#FFF;">follow on Twitter</a> | <a href="http://facebook.com/queensbali" target="_blank" style="color:#FFF;">friend on Facebook</a> 
        </td>
      </tr>
    </table>
    <table width="700" border="0" cellspacing="0" cellpadding="50" bgcolor="#FFFFFF" align="center">
      <tr>
        <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
          <tr>
          	<td width="100%" align="center">
            <img src="http://accounts.queenstandoor.com/images/queens-head.jpg" width="300" height="121"><hr>
            </td>
          </tr>
          <tr>
            <td align="right">
            Date : <strong><?php echo date('l, d F Y');?></strong>
            </td>
          </tr>
        </table>
        <h3>PAYMENT FOR DELIVERY SUCCESS</h3>
        <div><strong><em>Warm greetings from Queen&rsquo;s Chain of Indian Restaurants.<br />
          JAKARTA - BALI - AL-KHOBAR - RIYADH</em></strong>
        </div><br>
        <b>Fullname</b> : <?php echo $billing['title'].' '.$billing['fname'].' '.$billing['lname']; ?><br />
        <b>Email</b> : <?php echo $billing['email']; ?><br />
        <b>Phone Number</b> : <?php echo $billing['hmobile']; ?><br />
        <b>Address</b> : <?php echo $billing['haddress']; ?><br />
        <b>City</b> : <?php echo $billing['hcity']; ?><br />
        <br />
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <thead>
          <tr>
            <th bgcolor="#CCCCCC">#</th>
            <th bgcolor="#CCCCCC">Order Name</th>
            <th bgcolor="#CCCCCC">Price</th>
            <th bgcolor="#CCCCCC">Qty</th>
            <th bgcolor="#CCCCCC">Tax</th>
            <th bgcolor="#CCCCCC">Service</th>
            <th bgcolor="#CCCCCC">Total</th>
            <th bgcolor="#CCCCCC">Special Req</th>
          </tr>
          </thead>
          <tbody>
          <?php
		  	$sql=mysql_query("SELECT tb_carts.id_cart, tb_menu.menu, tb_carts.price, tb_carts.qty, tb_carts.tax, tb_carts.service, tb_carts.total, tb_carts.specreq FROM tb_carts INNER JOIN tb_menu ON tb_carts.id_menu = tb_menu.id_menu WHERE tb_carts.cart_session='$billing[cart_session]'");
			$gtotal=0;
			while($row=mysql_fetch_array($sql)){
				$gtotal+=$row['total'];
				$no++;
		  ?>
          <tr>
            <td><?=$no;?></td>
            <td><?=$row['menu'];?></td>
            <td align="right"><?=number_format($row['price'],2,",",".");;?></td>
            <td align="center"><?=number_format($row['qty'],2,",",".");;?></td>
            <td align="center"><?=$row['tax'].'%';?></td>
            <td align="center"><?=$row['service'].'%';?></td>
            <td align="right"><?=number_format($row['total'],2,",",".");?></td>
            <td align="center"><?=$row['specreq'];?></td>
          </tr>
          <?php } ?>
          <tr>
            <td colspan="6" align="right" bgcolor="#F0F0F0">
            <strong>GRAND TOTAL</strong> : </td>
            <td align="right" bgcolor="#F0F0F0"><?=number_format($gtotal,2,",",".");?></td>
            <td bgcolor="#F0F0F0">&nbsp;</td>
          </tr>
          </tbody>
        </table>
        <br />
        <b>Status</b> : Success<br />
        <b>Transaction ID</b> : <?=$trx_id;?>
        <hr>
        With Regards<br />
        Management of Queen's Chain of Indian Restaurants in Bali
        <br><br>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:12px;">
          <tr>
            <td align="center">
            <hr />
            Follow us @: queensbali: <img src="http://accounts.queenstandoor.com/images/icon-media.jpg" alt="" width="158" height="22" align="absmiddle"> |  http://bali.queenstandoor.com |  HOTLINE: +62 81 249 249 249 </td>
          </tr>
          <tr>
            <td align="center"><strong>QUEEN'S TANDOOR</strong>, Jl. Raya <strong>Seminyak</strong> No. 1/73A, Kuta |  T/F: (62-361) 732770/732771  |  e: bali@queenstandoor.com</td>
          </tr>
          <tr>
            <td align="center"><strong>QUEEN'S OF INDIA</strong>, Bali Dynasty Resort, Jl. Kartika Plaza, Tuban - <strong>Kuta</strong> | T/F: (62-361) 765988/761099  |  e: bali2@queenstandoor.com</td>
          </tr>
          <tr>
            <td align="center"><strong>QUEEN'S OF INDIA</strong>, Jl. Pratama No. 65B, Tanjung Benoa - <strong>Nusa Dua</strong> |  T/F: (62-361) 771344/774 648  |  e: bali3@queenstandoor.com</td>
          </tr>
          <tr>
            <td align="center"><strong>QUEEN'S OF INDIA</strong>, Jl. Suweta No. 1, Opp. Palace ( Puri Saren Ubud ), <strong>Ubud</strong> | T/F: ( 62-361 ) 977399/977400 | e: bali4@queenstandoor.com</td>
          </tr>
        </table>
		</td>
	  </tr>
	</table>
    <table width="700" border="0" cellspacing="0" cellpadding="0" align="center" style="margin-top:10px; color:#FFF;">
      <tr>
        <td align="center">
        Copyright &copy; 2010 - 2012 <a href="http://bali.queenstandoor.com/" target="_blank" title="Bali Indian Restaurant" style="color:#FFF;">bali.queenstandoor.com</a>. All Rights Reserved</td>
      </tr>
    </table>
	</td>
  </tr>
</table>