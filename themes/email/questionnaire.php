<table width="100%" border="0" cellspacing="0" cellpadding="70" bgcolor="#FF9900">
  <tr>
    <td>
    <table width="700" border="0" cellspacing="0" cellpadding="0" style="color:#FFF; margin-bottom:10px; font-size:12px;" align="center">
      <tr>
        <td align="left" valign="top">
        Questionnaire Form, <?php echo date('F Y'); ?>
        </td>
        <td align="right" valign="top">
        <a href="http://twitter.com/queensbali" target="_blank" style="color:#FFF;">follow on Twitter</a> | <a href="http://facebook.com/queensbali" target="_blank" style="color:#FFF;">friend on Facebook</a> 
        </td>
      </tr>
    </table>
    <table width="700" border="0" cellspacing="0" cellpadding="50" bgcolor="#FFFFFF" align="center">
      <tr>
        <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
          <tr>
          	<td width="100%" align="center">
            <img src="http://accounts.queenstandoor.com/images/queens-head.jpg" width="300" height="121"><hr>
            </td>
          </tr>
          <tr>
            <td align="right">
            Date : <strong><?php echo date('l, d F Y');?></strong>
            </td>
          </tr>
        </table>
        QUESTIONNAIRE<br>
        <div><strong><em>Warm greetings from Queen&rsquo;s Chain of Indian Restaurants.<br />
          JAKARTA - BALI - AL-KHOBAR - RIYADH</em></strong>
        </div><br>
        <b>Fullname</b> : <?php echo $fullname; ?><br />
        <b>Company</b> : <?php echo $mailto; ?><br />
        <b>Email</b> : <?php echo $content; ?><br />
        <b>Phone Number</b> : <?php echo $subject; ?><br />
        <b>Fax Number</b>: <?php echo $nm_outlet; ?><br />
        <br />
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <thead>
          <tr>
            <th width="42%">&nbsp;</th>
            <th width="58%" align="left"><strong>Preference</strong></th>
            </tr>
          </thead>
          <tbody>
          <tr class="danger">
            <td><strong>FOOD</strong></td>
            <td>&nbsp;</td>
            </tr>
          <tr>
            <td><p>&nbsp;&nbsp;&rarr; Presentation</p></td>
            <td><?=$presentation;?></td>
            </tr>
          <tr>
            <td><p>&nbsp;&nbsp;&rarr; Taste</p></td>
            <td><?=$taste;?></td>
            </tr>
          <tr>
            <td><p>&nbsp;&nbsp;&rarr; Temperature</p></td>
            <td><?=$temperature_1;?></td>
            </tr>
          <tr>
            <td><p>&nbsp;&nbsp;&rarr; Portion Size</p></td>
            <td><?=$portion;?></td>
            </tr>
          <tr>
            <td><p>&nbsp;&nbsp;&rarr; Quality</p></td>
            <td><?=$quality;?></td>
            </tr>
          <tr>
            <td><p>&nbsp;&nbsp;&rarr; Menu Selection</p></td>
            <td><?=$menu;?></td>
            </tr>
          <tr class="danger">
            <td><strong>SERVICES</strong></td>
            <td>&nbsp;</td>
            </tr>
          <tr>
            <td><p>&nbsp;&nbsp;&rarr; Greeting</p></td>
            <td><?=$greeting;?></td>
            </tr>
          <tr>
            <td><p>&nbsp;&nbsp;&rarr; Politeness</p></td>
            <td><?=$politeness;?></td>
            </tr>
          <tr>
            <td><p>&nbsp;&nbsp;&rarr; Promptness</p></td>
            <td><?=$promptness;?></td>
            </tr>
          <tr>
            <td><p>&nbsp;&nbsp;&rarr; Attire</p></td>
            <td><?=$attire;?></td>
            </tr>
          <tr class="danger">
            <td> <strong>ATMOSPHERE</strong></td>
            <td>&nbsp;</td>
            </tr>
          <tr>
            <td><p>&nbsp;&nbsp;&rarr; Comfortable</p></td>
            <td><?=$comfortable;?></td>
            </tr>
          <tr>
            <td><p>&nbsp;&nbsp;&rarr; Cleanliness</p></td>
            <td><?=$cleanliness;?></td>
            </tr>
          <tr>
            <td><p>&nbsp;&nbsp;&rarr; Music</p></td>
            <td><?=$music;?></td>
            </tr>
          <tr>
            <td><p>&nbsp;&nbsp;&rarr; Temperature</p></td>
            <td><?=$temperature_2;?></td>
            </tr>
          <tr class="danger">
            <td><strong>OVERALL RATING</strong></td>
            <td><?=$rating;?></td>
            </tr>
          </tbody>
        </table><br />
        <strong>1. How did you hear about us ? : </strong><?=$quis_1;?><br>
        <strong>2. How often do you come to Queen's Tandoor ? : </strong><?=$quis_2;?><br>
		<strong>3. What items did you order ? : </strong><?=$quis_3;?><br>
		<strong>4. What did you like the best about our service ? : </strong><?=$quis_4;?><br>
		<strong>5. If we are not open today, which restaurant will you go to ? : </strong><?=$quis_5;?><br>
		<strong>6. Other comments or suggestions ? : </strong><?=$quis_6;?>
        <br /><hr />
        With Regards<br />
        Management of Queen's Chain of Indian Restaurants in Bali
        <br><br>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:12px;">
          <tr>
            <td align="center">
            <hr />
            Follow us @: queensbali: <img src="http://accounts.queenstandoor.com/images/icon-media.jpg" alt="" width="158" height="22" align="absmiddle"> |  http://bali.queenstandoor.com |  HOTLINE: +62 81 249 249 249 </td>
          </tr>
          <tr>
            <td align="center"><strong>QUEEN'S TANDOOR</strong>, Jl. Raya <strong>Seminyak</strong> No. 1/73A, Kuta |  T/F: (62-361) 732770/732771  |  e: bali@queenstandoor.com</td>
          </tr>
          <tr>
            <td align="center"><strong>QUEEN'S OF INDIA</strong>, Bali Dynasty Resort, Jl. Kartika Plaza, Tuban - <strong>Kuta</strong> | T/F: (62-361) 765988/761099  |  e: bali2@queenstandoor.com</td>
          </tr>
          <tr>
            <td align="center"><strong>QUEEN'S OF INDIA</strong>, Jl. Pratama No. 65B, Tanjung Benoa - <strong>Nusa Dua</strong> |  T/F: (62-361) 771344/774 648  |  e: bali3@queenstandoor.com</td>
          </tr>
          <tr>
            <td align="center"><strong>QUEEN'S OF INDIA</strong>, Jl. Suweta No. 1, Opp. Palace ( Puri Saren Ubud ), <strong>Ubud</strong> | T/F: ( 62-361 ) 977399/977400 | e: bali4@queenstandoor.com</td>
          </tr>
        </table>
		</td>
	  </tr>
	</table>
    <table width="700" border="0" cellspacing="0" cellpadding="0" align="center" style="margin-top:10px; color:#FFF;">
      <tr>
        <td align="center">
        Copyright &copy; 2010 - 2012 <a href="http://bali.queenstandoor.com/" target="_blank" title="Bali Indian Restaurant" style="color:#FFF;">bali.queenstandoor.com</a>. All Rights Reserved</td>
      </tr>
    </table>
	</td>
  </tr>
</table>