<table width="100%" border="0" cellspacing="0" cellpadding="70" bgcolor="#FF9900">
  <tr>
    <td>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="color:#FFF; margin-bottom:10px; font-size:12px;">
      <tr>
        <td align="left" valign="top">
        Membership Form, <?php echo date('F Y'); ?>
        </td>
        <td align="right" valign="top">
        <a href="http://twitter.com/queensbali" target="_blank" style="color:#FFF;">follow on Twitter</a> | <a href="http://facebook.com/queensbali" target="_blank" style="color:#FFF;">friend on Facebook</a> 
        </td>
      </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="50" bgcolor="#FFFFFF" align="center">
      <tr>
        <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
          <tr>
          	<td width="100%" align="center">
            <img src="http://accounts.queenstandoor.com/images/queens-head.jpg" width="300" height="121"><hr>
            </td>
          </tr>
          <tr>
            <td align="right">
            Date : <strong><?php echo date('l, d F Y');?></strong>
            </td>
          </tr>
        </table>
        Dear <?php echo $title.' '.$fname.' '.$lname; ?><br /><br />
        <div><strong><em>Warm greetings from Queen&rsquo;s Chain of Indian Restaurants.<br />
          JAKARTA - BALI - AL-KHOBAR - RIYADH</em></strong>
        </div>
        <br />
        Thank you for your submission to the VIP Membership programme at Queen&rsquo;s Bali, we will process your enquiry and respond in due course.<br />
        
        Soon our sales & marketing team will contact you to follow up your VIP Membership request.<br />
        
        Membership eligibility is decided exclusively by the Queen&rsquo;s management and Owner(s) depending on the frequency of dining and value of your patronage.
        
        
        
        Thanks again.
        <br />
        <br />
        <strong>MEMBERSHIP FORM SUBMITTED</strong><br /><br />
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="3"><strong>PERSONAL DETAILS</strong></td>
          </tr>
          <tr>
            <td width="19%">Fullname</td>
            <td width="4%">:</td>
            <td width="77%"><?php echo $title.' '.$fname.' '.$lname; ?>
            </td>
          </tr>
          <tr>
            <td>Gender</td>
            <td>:</td>
            <td><?php echo $sex; ?></td>
          </tr>
          <tr>
            <td>Nationality</td>
            <td>:</td>
            <td><?php echo $nationality; ?></td>
          </tr>
          <tr>
            <td>Religion</td>
            <td>:</td>
            <td><?php echo $religion; ?></td>
          </tr>
          <tr>
            <td>Home Address</td>
            <td>:</td>
            <td><?php echo $haddress; ?></td>
          </tr>
          <tr>
            <td>City</td>
            <td>:</td>
            <td><?php echo $hcity; ?></td>
          </tr>
          <tr>
            <td>Postal Code</td>
            <td>:</td>
            <td><?php echo $hpost; ?></td>
          </tr>
          <tr>
            <td>Area</td>
            <td>:</td>
            <td><?php echo $harea; ?></td>
          </tr>
          <tr>
            <td>Country</td>
            <td>:</td>
            <td><?php echo $hcountry; ?></td>
          </tr>
          <tr>
            <td>Phone</td>
            <td>:</td>
            <td><?php echo $hphone;?></td>
          </tr>
          <tr>
            <td>Fax</td>
            <td>:</td>
            <td><?php echo $hfax; ?></td>
          </tr>
          <tr>
            <td>Mobile / HP</td>
            <td>:</td>
            <td><?php echo $hmobile; ?></td>
          </tr>
          <tr>
            <td>Email</td>
            <td>:</td>
            <td><?php echo $hmail; ?></td>
          </tr>
          <tr>
            <td>Website</td>
            <td>:</td>
            <td><?php echo $web; ?></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="3"><strong>PRIVATE DETAILS</strong></td>
          </tr>
          <tr>
            <td>Birth Place</td>
            <td>:</td>
            <td><?php echo $birthplace; ?></td>
          </tr>
          <tr>
            <td>Birth Date</td>
            <td>:</td>
            <td><?php echo $birthdate; ?></td>
          </tr>
          <tr>
            <td>Marriage Date</td>
            <td>:</td>
            <td><?php echo $marriage; ?></td>
          </tr>
          <tr>
            <td>Hobby</td>
            <td>:</td>
            <td><?php echo $hobby; ?></td>
          </tr>
          <tr>
            <td>Remark</td>
            <td>:</td>
            <td><?php echo $remark; ?></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="3"><strong>COMPANY DETAILS</strong></td>
          </tr>
          <tr>
            <td>Company Name</td>
            <td>:</td>
            <td><?php echo $cname; ?></td>
          </tr>
          <tr>
            <td>Company Address</td>
            <td>:</td>
            <td><?php echo $caddress; ?></td>
          </tr>
          <tr>
            <td>City</td>
            <td>:</td>
            <td><?php echo $ccity; ?></td>
          </tr>
          <tr>
            <td>Postal Code</td>
            <td>:</td>
            <td><?php echo $cpost; ?></td>
          </tr>
          <tr>
            <td>Area</td>
            <td>:</td>
            <td><?php echo $carea; ?></td>
          </tr>
          <tr>
            <td>Country</td>
            <td>:</td>
            <td><?php echo $ccountry; ?></td>
          </tr>
          <tr>
            <td>Phone</td>
            <td>:</td>
            <td><?php echo $cphone; ?></td>
          </tr>
          <tr>
            <td>Fax</td>
            <td>:</td>
            <td><?php echo $cfax; ?></td>
          </tr>
          <tr>
            <td>Position</td>
            <td>:</td>
            <td><?php echo $cposition; ?></td>
          </tr>
          <tr>
            <td>Email</td>
            <td>:</td>
            <td><?php echo $cmail; ?></td>
          </tr>
        </table><br><hr>
        Auto Reply<br />
        Management of Queen&rsquo;s Chain of Indian Restaurants in Indonesia (Bali Branch)<br /><br />
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:12px;">
          <tr>
            <td align="center">
            <hr />
            Follow us @: queensbali: <img src="http://accounts.queenstandoor.com/images/icon-media.jpg" alt="" width="158" height="22" align="absmiddle"> |  http://bali.queenstandoor.com |  HOTLINE: +62 81 249 249 249 </td>
          </tr>
          <tr>
            <td align="center"><strong>QUEEN'S TANDOOR</strong>, Jl. Raya <strong>Seminyak</strong> No. 1/73A, Kuta |  T/F: (62-361) 732770/732771  |  e: bali@queenstandoor.com</td>
          </tr>
          <tr>
            <td align="center"><strong>QUEEN'S OF INDIA</strong>, Bali Dynasty Resort, Jl. Kartika Plaza, Tuban - <strong>Kuta</strong> | T/F: (62-361) 765988/761099  |  e: bali2@queenstandoor.com</td>
          </tr>
          <tr>
            <td align="center"><strong>QUEEN'S OF INDIA</strong>, Jl. Pratama No. 65B, Tanjung Benoa - <strong>Nusa Dua</strong> |  T/F: (62-361) 771344/774 648  |  e: bali3@queenstandoor.com</td>
          </tr>
          <tr>
            <td align="center"><strong>QUEEN'S OF INDIA</strong>, Jl. Suweta No. 1, Opp. Palace ( Puri Saren Ubud ), <strong>Ubud</strong> | T/F: ( 62-361 ) 977399/977400 | e: bali4@queenstandoor.com</td>
          </tr>
        </table>
		</td>
	  </tr>
	</table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="margin-top:10px; color:#FFF;">
      <tr>
        <td align="center">
        Copyright © 2010 - 2012 <a href="http://bali.queenstandoor.com/" target="_blank" title="Bali Indian Restaurant" style="color:#FFF;">bali.queenstandoor.com</a>. All Rights Reserved</td>
      </tr>
    </table>
	</td>
  </tr>
</table>