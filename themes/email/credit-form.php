<table width="100%" border="0" cellspacing="0" cellpadding="70" bgcolor="#FF9900">
  <tr>
    <td>
    <table width="700" border="0" cellspacing="0" cellpadding="0" style="color:#FFF; margin-bottom:10px; font-size:12px;" align="center">
      <tr>
        <td align="left" valign="top">
        Credit Request Form, <?php echo date('F Y'); ?>
        </td>
        <td align="right" valign="top">
        <a href="http://twitter.com/queensbali" target="_blank" style="color:#FFF;">follow on Twitter</a> | <a href="http://facebook.com/queensbali" target="_blank" style="color:#FFF;">friend on Facebook</a> 
        </td>
      </tr>
    </table>
    <table width="700" border="0" cellspacing="0" cellpadding="50" bgcolor="#FFFFFF" align="center">
      <tr>
        <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
          <tr>
          	<td width="100%" align="center">
            <img src="http://accounts.queenstandoor.com/images/queens-head.jpg" width="300" height="121"><hr>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="27" align="center" bgcolor="#000" style="color:#FFF">
            TRAVEL AGENT CREDIT REQUEST APPLICATION
            </td>
          </tr>
        </table><br />
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="59%">&nbsp;</td>
            <td width="15%">Date</td>
            <td width="2%">:</td>
            <td width="24%" style="border-bottom:dotted 1px #000">
            <strong><?php echo date('l, d F Y');?></strong>
            </td>
          </tr>
        </table><br />    
        <table width="100%" border="0" cellspacing="5" cellpadding="5" style="border:solid 1px #000">
          <tr>
            <td>
            <table width="100%" border="0" cellspacing="5" cellpadding="0">
              <tr>
                <td width="7%">Company</td>
                <td width="2%">:</td>
                <td colspan="7" style="border-bottom:dotted 1px #000">
                <?php echo $company; ?>&nbsp;
                </td>
              </tr>
              <tr>
                <td>Address</td>
                <td>:</td>
                <td colspan="7" style="border-bottom:dotted 1px #000">
                <?php echo $caddress; ?>&nbsp;
                </td>
              </tr>
              <tr>
                <td>City</td>
                <td>:</td>
                <td width="22%" style="border-bottom:dotted 1px #000">
                <?php echo $city; ?>&nbsp;
                </td>
                <td width="3%">State</td>
                <td colspan="2" style="border-bottom:dotted 1px #000">
                : <?php echo $state; ?>&nbsp;</td>
                <td width="2%">Code</td>
                <td width="1%">:</td>
                <td width="29%" style="border-bottom:dotted 1px #000">
                <?php echo $postal; ?>&nbsp;
                </td>
              </tr>
              <tr>
                <td>Telephone</td>
                <td>:</td>
                <td colspan="2" style="border-bottom:dotted 1px #000">
                <?php echo $cphone; ?>&nbsp;
                </td>
                <td width="3%">Fax :</td>
                <td colspan="4" style="border-bottom:dotted 1px #000">
                <?php echo $cfax; ?>&nbsp;
                </td>
              </tr>
            </table>
            </td>
          </tr>
        </table><br />
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="145">Name<font color="#FFFFFF">_</font>of<font color="#FFFFFF">_</font>accounts<font color="#FFFFFF">_</font>payable<font color="#FFFFFF">_</font>contact </td>
            <td width="3">:</td>
            <td width="337" style="border-bottom:dotted 1px #000">
            <?php echo $aname; ?>&nbsp;
            </td>
            <td width="23">Title</td>
            <td width="3">:</td>
            <td width="263" style="border-bottom:dotted 1px #000">
            <?php echo $atitle; ?>&nbsp;
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td width="15%">Company<font color="#FFFFFF">_</font>Registration<font color="#FFFFFF">_</font>No.</td>
            <td width="1%">:</td>
            <td width="18%" style="border-bottom:dotted 1px #000">
            <?php echo $aregistration; ?>&nbsp;
            </td>
            <td width="13%">ASITA<font color="#FFFFFF">_</font>Member<font color="#FFFFFF">_</font>No. </td>
            <td width="1%">:</td>
            <td width="19%" style="border-bottom:dotted 1px #000">
			<?php echo $asitano; ?>&nbsp;</td>
            <td width="13%">PATA<font color="#FFFFFF">_</font>Registration<font color="#FFFFFF">_</font>No.</td>
            <td width="1%">:</td>
            <td width="19%" style="border-bottom:dotted 1px #000">
			<?php echo $apatano; ?>&nbsp;</td>
          </tr>
        </table><br />
        <table width="100%" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td width="15%">OWNERSHIP</td>
            <td width="2%">:</td>
            <td width="12%">Name </td>
            <td width="2%">:</td>
            <td width="27%" style="border-bottom:dotted 1px #000">
			<?php echo $oname1; ?>&nbsp;</td>
            <td width="8%">Telephone</td>
            <td width="2%">:</td>
            <td width="32%" style="border-bottom:dotted 1px #000">
			<?php echo $ophone1; ?>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Title </td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $otitle1; ?>&nbsp;</td>
            <td>Email </td>
            <td>&nbsp;</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $omail1; ?>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Address</td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $oaddress1; ?>&nbsp;</td>
            <td>No. Hp.</td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $ophone1; ?>&nbsp;</td>
          </tr>
          </table><br />
        <table width="100%" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td width="15%">OWNERSHIP</td>
            <td width="2%">:</td>
            <td width="12%">Name </td>
            <td width="2%">:</td>
            <td width="27%" style="border-bottom:dotted 1px #000">
			<?php echo $oname2; ?>&nbsp;</td>
            <td width="8%">Telephone</td>
            <td width="2%">:</td>
            <td width="32%" style="border-bottom:dotted 1px #000">
			<?php echo $ophone2; ?>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Title </td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $otitle2; ?>&nbsp;</td>
            <td>Email </td>
            <td>&nbsp;</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $omail2; ?>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Address </td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $oaddress2; ?>&nbsp;</td>
            <td>No.Hp. </td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $ophone2; ?>&nbsp;</td>
          </tr>
        </table><br />
        <table width="100%" border="0" cellspacing="5" cellpadding="0" style="border-top:solid 1px #000">
          <tr>
            <td width="15%">GENERAL</td>
            <td width="2%">:</td>
            <td width="12%">Nama</td>
            <td width="2%">:</td>
            <td width="27%" style="border-bottom:dotted 1px #000">
			<?php echo $mtitle; ?>&nbsp;</td>
            <td width="8%">Telephone</td>
            <td width="2%">:</td>
            <td width="32%" style="border-bottom:dotted 1px #000">
			<?php echo $mphone; ?>&nbsp;</td>
          </tr>
          <tr>
            <td>MANAGER</td>
            <td>&nbsp;</td>
            <td>Title</td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $mname; ?>&nbsp;</td>
            <td>Email</td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $mmail; ?>&nbsp;</td>
          </tr>
          <tr>
            <td>/OPERATION</td>
            <td>&nbsp;</td>
            <td>Address</td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $maddress; ?>&nbsp;</td>
            <td>No.Hp.</td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $mphone; ?>&nbsp;</td>
          </tr>
          <tr>
            <td>MANAGER</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="5" cellpadding="0" style="border-top:solid 1px #000000">
          <tr>
            <td width="15%">ACCOUNTANT</td>
            <td width="2%">:</td>
            <td width="12%">Name </td>
            <td width="2%">:</td>
            <td width="27%" style="border-bottom:dotted 1px #000">
			<?php echo $accname1; ?>&nbsp;</td>
            <td width="8%">Telephone</td>
            <td width="2%">:</td>
            <td width="32%" style="border-bottom:dotted 1px #000">
			<?php echo $accphone1; ?>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Title </td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $acccontact1; ?>&nbsp;</td>
            <td>Email</td>
            <td>&nbsp;</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $accmail1; ?>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Address </td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $accaddress1; ?>&nbsp;</td>
            <td>No. Hp</td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $accphone1; ?>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Name </td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $accname2; ?>&nbsp;</td>
            <td>Telephone</td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $accphone2; ?>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Title</td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $acccontact2; ?>&nbsp;</td>
            <td>Email</td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $accmail2; ?>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Address </td>
            <td>&nbsp;</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $accaddress2; ?>&nbsp;</td>
            <td>No. Hp</td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $accphone2; ?>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td >&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Name</td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $accname3; ?>&nbsp;</td>
            <td>Telephone</td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $accphone3; ?>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Title</td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $acccontact3; ?>&nbsp;</td>
            <td>Email</td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $accmail3; ?>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Address </td>
            <td>&nbsp;</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $accaddress3; ?>&nbsp;</td>
            <td>No. Hp</td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $accphone3; ?>&nbsp;</td>
          </tr>
          <tr>
            <td height="12">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td height="12">PAYMENT</td>
            <td>:</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="5" cellpadding="0" style="border-top:solid 1px #000000;">
          <tr>
            <td width="15%">BANK</td>
            <td width="2%">:</td>
            <td width="12%">Name</td>
            <td width="2%">:</td>
            <td width="27%" style="border-bottom:dotted 1px #000">
			<?php echo $bname; ?>&nbsp;</td>
            <td width="8%">Address</td>
            <td width="2%">:</td>
            <td width="32%" style="border-bottom:dotted 1px #000">
			<?php echo $baddress; ?>&nbsp;</td>
          </tr>
          <tr>
            <td>REFERENCES</td>
            <td>&nbsp;</td>
            <td>Office</td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $boffice; ?>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td style="border-bottom:dotted 1px #000">&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td style="border-bottom:dotted 1px #000">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td style="border-bottom:dotted 1px #000">&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Account<font color="#FFFFFF">_</font>No.</td>
            <td>:</td>
            <td style="border-bottom:dotted 1px #000">
			<?php echo $baccno; ?>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:12px;">
          <tr>
            <td align="center">
            <hr />
            Follow us @: queensbali: <img src="http://accounts.queenstandoor.com/images/icon-media.jpg" alt="" width="158" height="22" align="absmiddle"> |  http://bali.queenstandoor.com |  HOTLINE: +62 81 249 249 249 </td>
          </tr>
          <tr>
            <td align="center"><strong>QUEEN'S TANDOOR</strong>, Jl. Raya <strong>Seminyak</strong> No. 1/73A, Kuta |  T/F: (62-361) 732770/732771  |  e: bali@queenstandoor.com</td>
          </tr>
          <tr>
            <td align="center"><strong>QUEEN'S OF INDIA</strong>, Bali Dynasty Resort, Jl. Kartika Plaza, Tuban - <strong>Kuta</strong> | T/F: (62-361) 765988/761099  |  e: bali2@queenstandoor.com</td>
          </tr>
          <tr>
            <td align="center"><strong>QUEEN'S OF INDIA</strong>, Jl. Pratama No. 65B, Tanjung Benoa - <strong>Nusa Dua</strong> |  T/F: (62-361) 771344/774 648  |  e: bali3@queenstandoor.com</td>
          </tr>
          <tr>
            <td align="center"><strong>QUEEN'S OF INDIA</strong>, Jl. Suweta No. 1, Opp. Palace ( Puri Saren Ubud ), <strong>Ubud</strong> | T/F: ( 62-361 ) 977399/977400 | e: bali4@queenstandoor.com</td>
          </tr>
        </table>
    	</td>
      </tr>
    </table>
    <table width="700" border="0" cellspacing="0" cellpadding="0" align="center" style="margin-top:10px; color:#FFF;">
      <tr>
        <td align="center">
        Copyright © 2010 - 2012 <a href="http://bali.queenstandoor.com/" target="_blank" title="Bali Indian Restaurant" style="color:#FFF;">bali.queenstandoor.com</a>. All Rights Reserved</td>
      </tr>
    </table>
	</td>
  </tr>
</table>