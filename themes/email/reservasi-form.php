<table width="100%" border="0" cellspacing="0" cellpadding="70" bgcolor="#FF9900">
  <tr>
    <td>
    <table width="700" border="0" cellspacing="0" cellpadding="0" align="center" style="color:#FFF; margin-bottom:10px; font-size:12px;">
      <tr>
        <td align="left" valign="top">
        Reservation Form, <?php echo date('F Y'); ?>
        </td>
        <td align="right" valign="top">
        <a href="http://twitter.com/queensbali" target="_blank" style="color:#FFF;">follow on Twitter</a> | <a href="http://facebook.com/queensbali" target="_blank" style="color:#FFF;">friend on Facebook</a> 
        </td>
      </tr>
    </table>
    <table width="700" border="0" cellspacing="0" cellpadding="50" bgcolor="#FFFFFF" align="center">
      <tr>
        <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
          <tr>
          	<td width="100%" align="center">
            <img src="http://accounts.queenstandoor.com/images/queens-head.jpg" width="300" height="121"><hr>
            </td>
          </tr>
          <tr>
            <td align="right">
            Date : <strong><?php echo date('l, d F Y');?></strong>
            </td>
          </tr>
        </table>
        Dear <?php echo $fullname; ?><br /><br />
        <div><strong><em>Warm greetings from Queen's Chain of Indian Restaurants.<br />
          JAKARTA - BALI - AL-KHOBAR - RIYADH</em></strong>
        </div>
        <br />
        Thank you for your reservation enquiry.<br />
        <br />
        Your enquiry has been sent to our sales &amp; marketing team and will be processed and responded to within 24 hours.
        <br />
        <br />
        <strong>RESERVATION FORM SUBMITTED</strong><br /> <br />
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="20%">Fullname</td>
            <td width="4%">:</td>
            <td width="76%"><?php echo $fullname; ?></td>
          </tr>
          <tr>
            <td>Home Address</td>
            <td>:</td>
            <td><?php echo $haddress; ?></td>
          </tr>
          <tr>
            <td>City</td>
            <td>:</td>
            <td><?php echo $city; ?></td>
          </tr>
          <tr>
            <td>Postal Code</td>
            <td>:</td>
            <td><?php echo $postal; ?></td>
          </tr>
          <tr>
            <td>Country</td>
            <td>:</td>
            <td><?php echo $country; ?></td>
          </tr>
          <tr>
            <td>Phone Number</td>
            <td>:</td>
            <td><?php echo $phone; ?></td>
          </tr>
          <tr>
            <td>Mobile Number</td>
            <td>:</td>
            <td><?php echo $mobile; ?></td>
          </tr>
          <tr>
            <td>Mail Address</td>
            <td>:</td>
            <td><?php echo $mailto; ?></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>Outlet</td>
            <td>:</td>
            <td><?php echo $selcab['name'].' - '.$selcab['state']; ?></td>
          </tr>
          <tr>
            <td>Date For Visit</td>
            <td>:</td>
            <td><?php echo $datevisit.' Time : '.$time; ?></td>
          </tr>
          <tr>
            <td>Number Of</td>
            <td>:</td>
            <td>Adult : <?php echo $adult; ?> | Child : <?php echo $child; ?>
            </td>
          </tr>
          <tr>
            <td>Subject</td>
            <td>:</td>
            <td>Online Reservation</td>
          </tr>
          <tr>
            <td>Note</td>
            <td>:</td>
            <td><?php echo $specrec; ?></td>
          </tr>
        </table>
        <br /><hr>
        Auto Reply<br />
        Management of Queen&rsquo;s Chain of Indian Restaurants, Bali<br /><br />
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:12px;">
          <tr>
            <td align="center">
            <hr />
            Follow us @: queensbali: <img src="http://accounts.queenstandoor.com/images/icon-media.jpg" alt="" width="158" height="22" align="absmiddle"> |  http://bali.queenstandoor.com |  HOTLINE: +62 81 249 249 249 
            </td>
          </tr>
          <tr>
            <td align="center"><strong>QUEEN'S TANDOOR</strong>, Jl. Raya <strong>Seminyak</strong> No. 1/73A, Kuta |  T/F: (62-361) 732770/732771  |  e: bali@queenstandoor.com
            </td>
          </tr>
          <tr>
            <td align="center"><strong>QUEEN'S OF INDIA</strong>, Bali Dynasty Resort, Jl. Kartika Plaza, Tuban - <strong>Kuta</strong> | T/F: (62-361) 765988/761099  |  e: bali2@queenstandoor.com
            </td>
          </tr>
          <tr>
            <td align="center"><strong>QUEEN'S OF INDIA</strong>, Jl. Pratama No. 65B, Tanjung Benoa - <strong>Nusa Dua</strong> |  T/F: (62-361) 771344/774 648  |  e: bali3@queenstandoor.com
            </td>
          </tr>
          <tr>
            <td align="center"><strong>QUEEN'S OF INDIA</strong>, Jl. Suweta No. 1, Opp. Palace ( Puri Saren Ubud ), <strong>Ubud</strong> | T/F: ( 62-361 ) 977399/977400 | e: bali4@queenstandoor.com
            </td>
          </tr>
        </table>
		</td>
	  </tr>
	</table>
    <table width="700" border="0" cellspacing="0" cellpadding="0" align="center" style="margin-top:10px; color:#FFF;">
      <tr>
        <td align="center">
        Copyright &copy; 2010 - 2012 <a href="http://bali.queenstandoor.com/" target="_blank" title="Bali Indian Restaurant" style="color:#FFF;">bali.queenstandoor.com</a>. All Rights Reserved</td>
      </tr>
    </table>
	</td>
  </tr>
</table>