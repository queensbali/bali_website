<table width="100%" border="0" cellspacing="0" cellpadding="70" bgcolor="#FF9900">
  <tr>
    <td>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="color:#FFF; margin-bottom:10px; font-size:12px;">
      <tr>
        <td align="left" valign="top">
        Booking Agent Form, <?php echo date('F Y'); ?>
        </td>
        <td align="right" valign="top">
        <a href="http://twitter.com/queensbali" target="_blank" style="color:#FFF;">follow on Twitter</a> | <a href="http://facebook.com/queensbali" target="_blank" style="color:#FFF;">friend on Facebook</a> 
        </td>
      </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="50" bgcolor="#FFFFFF" align="center">
      <tr>
        <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
          <tr>
          	<td width="100%" align="center">
            <img src="http://accounts.queenstandoor.com/images/queens-head.jpg" width="300" height="121"><hr>
            </td>
          </tr>
          <tr>
            <td align="right">
            Date : <strong><?php echo date('l, d F Y');?></strong>
            </td>
          </tr>
        </table>
        Dear, <strong><?php echo $company; ?></strong><br /><br />
        <div><strong><em>Warm greetings from Queen&rsquo;s Chain of Indian Restaurants.<br />
          JAKARTA - BALI - AL-KHOBAR - RIYADH</em></strong>
        </div>
        <br />
        Thank you for your booking with Queen's.<br>
Your booking has been sent to our   sales &amp; marketing team and will be processed and responded within the next hour. <br /><br>
		<strong>BOOKING REQUEST FORM SUBMITTED</strong><br><br>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="justify">
            <table width="100%" border="0" cellspacing="5" cellpadding="0">
              <tr>
                <td height="25" colspan="3" bgcolor="#EEEEEE">
                BOOKING DETAILS
                </td>
              </tr>
              <tr>
                <td width="27%">Guest Name</td>
                <td width="5%">:</td>
                <td width="68%" style="border-bottom:dotted 1px #000">
				<?php echo $guestname; ?></td>
              </tr>
              <tr>
                <td>Group Name</td>
                <td>:</td>
                <td style="border-bottom:dotted 1px #000">
				<?php echo $groupname; ?></td>
              </tr>
              <tr>
                <td>Nationality</td>
                <td>:</td>
                <td style="border-bottom:dotted 1px #000">
				<?php echo $nationality; ?></td>
              </tr>
              <tr>
                <td>No Of Pax</td>
                <td>:</td>
                <td style="border-bottom:dotted 1px #000">
				<?php echo $nopax; ?></td>
              </tr>
              <tr>
                <td>Date For Visit</td>
                <td>:</td>
                <td style="border-bottom:dotted 1px #000">
				<?php echo $datevisit; ?></td>
              </tr>
              <tr>
                <td>Expected Timings</td>
                <td>:</td>
                <td style="border-bottom:dotted 1px #000">
				<?php echo $timevisit; ?></td>
              </tr>
              <tr>
                <td>Outlet</td>
                <td>:</td>
                <td style="border-bottom:dotted 1px #000">
				<?php echo $nmoutlet; ?></td>
              </tr>
              <tr>
                <td>Venue</td>
                <td>:</td>
                <td style="border-bottom:dotted 1px #000">
				<?php echo $venue; ?></td>
              </tr>
              <tr>
                <td>Package / Rate</td>
                <td>:</td>
                <td style="border-bottom:dotted 1px #000">
				<?php echo $package; ?></td>
              </tr>
              <tr>
                <td>Service</td>
                <td>:</td>
                <td style="border-bottom:dotted 1px #000">
				<?php echo $service; ?></td>
              </tr>
              <tr>
                <td height="25" colspan="3" bgcolor="#EEEEEE">
                OTHER DETAILS
                </td>
              </tr>
              <tr>
                <td>Book By</td>
                <td>:</td>
                <td style="border-bottom:dotted 1px #000">
				<?php echo $bookby; ?></td>
              </tr>
              <tr>
                <td>Company Name</td>
                <td>:</td>
                <td style="border-bottom:dotted 1px #000">
				<?php echo $company; ?></td>
              </tr>
              <tr>
                <td>Email Address</td>
                <td>:</td>
                <td style="border-bottom:dotted 1px #000">
				<?php echo $mail; ?></td>
              </tr>
              <tr>
                <td>Guide Name</td>
                <td>:</td>
                <td style="border-bottom:dotted 1px #000">
				<?php echo $guidename; ?></td>
              </tr>
              <tr>
                <td>Guide Contact</td>
                <td>:</td>
                <td style="border-bottom:dotted 1px #000">
				<?php echo $guidephone; ?></td>
              </tr>
              <tr>
                <td height="25" colspan="3" bgcolor="#EEEEEE">
                PAYMENT MODE</td>
              </tr>
              <tr>
                <td>Payment Mode</td>
                <td>:</td>
                <td style="border-bottom:dotted 1px #000">
				<?php echo $payment; ?></td>
              </tr>
            </table><br>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:12px;">
              <tr>
                <td align="center">
                <hr />
                Follow us @: queensbali: <img src="http://accounts.queenstandoor.com/images/icon-media.jpg" alt="" width="158" height="22" align="absmiddle"> |  http://bali.queenstandoor.com |  HOTLINE: +62 81 249 249 249 </td>
              </tr>
              <tr>
                <td align="center"><strong>QUEEN'S TANDOOR</strong>, Jl. Raya <strong>Seminyak</strong> No. 1/73A, Kuta |  T/F: (62-361) 732770/732771  |  e: bali@queenstandoor.com</td>
              </tr>
              <tr>
                <td align="center"><strong>QUEEN'S OF INDIA</strong>, Bali Dynasty Resort, Jl. Kartika Plaza, Tuban - <strong>Kuta</strong> | T/F: (62-361) 765988/761099  |  e: bali2@queenstandoor.com</td>
              </tr>
              <tr>
                <td align="center"><strong>QUEEN'S OF INDIA</strong>, Jl. Pratama No. 65B, Tanjung Benoa - <strong>Nusa Dua</strong> |  T/F: (62-361) 771344/774 648  |  e: bali3@queenstandoor.com</td>
              </tr>
              <tr>
                <td align="center"><strong>QUEEN'S OF INDIA</strong>, Jl. Suweta No. 1, Opp. Palace ( Puri Saren Ubud ), <strong>Ubud</strong> | T/F: ( 62-361 ) 977399/977400 | e: bali4@queenstandoor.com</td>
              </tr>
            </table>
            </td>
          </tr>
        </table>
    	</td>
      </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="margin-top:10px; color:#FFF;">
      <tr>
        <td align="center">
        Copyright © 2010 - 2012 <a href="http://bali.queenstandoor.com/" target="_blank" title="Bali Indian Restaurant" style="color:#FFF;">bali.queenstandoor.com</a>. All Rights Reserved</td>
      </tr>
    </table>
	</td>
  </tr>
</table>