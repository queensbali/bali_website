<style type="text/css">
ol {
	margin-top:0px;
}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="70" bgcolor="#FF9900">
  <tr>
    <td>
    <table width="700" border="0" cellspacing="0" cellpadding="0" style="color:#FFF; margin-bottom:10px; font-size:12px;" align="center">
      <tr>
        <td align="left" valign="top">
        Credit Request Form, <?php echo date('F Y'); ?>
        </td>
        <td align="right" valign="top">
        <a href="http://twitter.com/queensbali" target="_blank" style="color:#FFF;">follow on Twitter</a> | <a href="http://facebook.com/queensbali" target="_blank" style="color:#FFF;">friend on Facebook</a> 
        </td>
      </tr>
    </table>
    <table width="700" border="0" cellspacing="0" cellpadding="50" bgcolor="#FFFFFF" align="center">
      <tr>
        <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
          <tr>
          	<td width="100%" align="center">
            <img src="http://accounts.queenstandoor.com/images/queens-head.jpg" width="300" height="121"><hr>
            </td>
          </tr>
          <tr>
            <td align="right">
            Date : <strong><?php echo date('l, d F Y');?></strong>
            </td>
          </tr>
        </table>
        Dear <strong><?php echo $company; ?>,</strong><br /><br />
        <div><strong><em>Warm greetings from Queen&rsquo;s Chain of Indian Restaurants.<br />
          JAKARTA - BALI - AL-KHOBAR - RIYADH</em></strong>
        </div>
        <br />
        Thank you for submitting &quot;CREDIT REQUEST FORM&quot; &amp; applying for credit terms with <strong>Queen's Bali</strong>.<br /><br />
        Thank you for taking the first step into the &quot;CREDIT PAYMENT&quot; with <strong>Queen's Bali</strong>.<br /><br />
		We really appreciate & honor your business, 
		whether you are new to <strong>Queen's</strong> partner list or already in our business list,<br />
		<br />
		This &quot;CREDIT REQUEST FORM&quot; will be send to Financial Controller for analysis, and our team will send confirmation within 24 hours.<br />
		<br />
		<u>Criteria for credit terms with <strong>Queen's Bali<br />
		</strong></u>
        <ol>
        	<li><strong>Queen's Bali</strong> must be able to recieve complete form on your company and / or the personal data of management.</li>
        	<li>
        	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        	    <tr>
        	      <td width="24%"><strong>Payment History</strong></td>
                  <td width="3%">:</td>
        	      <td width="73%">Should be smooth, clear &amp; on time</td>
      	      </tr>
      	    </table>
        	</li>
        	<li>
        	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        	    <tr>
        	      <td width="24%"><strong>Accounting Communication</strong></td>
                  <td width="3%">:</td>
        	      <td width="73%">Should be responsible &amp; corporative for all due payment</td>
      	      </tr>
      	    </table>
        	</li>
        	<li>
        	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        	    <tr>
        	      <td width="24%"><strong>Legality</strong></td>
        	      <td width="3%">:</td>
        	      <td width="73%">Should be a registered company with having membership in ASITA</td>
      	      </tr>
      	    </table>
        	</li>
        	<li>
        	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        	    <tr>
        	      <td width="24%"><strong>Relationship With Sales</strong></td>
        	      <td width="3%">:</td>
        	      <td width="73%">Should be fully supportive for FIT &amp; MICE business</td>
      	      </tr>
      	    </table>
        	</li>
        	<li>
        	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        	    <tr>
        	      <td width="24%"><strong>References</strong></td>
        	      <td width="3%">:</td>
        	      <td width="73%">Should have enough data &amp; information about owners, general manager &amp; accountant</td>
      	      </tr>
      	    </table>
        	</li>
        </ol>
		<u>Please read  the credit payment policy.</u>
		<ol>
        	<li>Payment is jointly, severally and unconditionally guaranteed within 30 days of date of booking (before 15th of every month)
            </li> 			
            <li>title to all work shall remain with the creditor until all invoices and additional charges have been paid in full
            </li>
		    <li>all claims, requests for adjustments, or notification of errors   must be made within thirty days, or charges are considered accepted
            </li>
            <li>this agreement shall apply to all current and future charges unless revocation is received by registered mail
            </li>
		    <li>credit privileges may be withdrawn at any time without invalidating the terms of this agreement
            </li>
        </ol>
		For full Terms &amp; Conditions, plase click <a href="http://bali.queenstandoor.com/terms-condition-proposal-agentL.html" target="_blank" title="Terms & Conditions">here</a> to read<br />
		If you need further assistant, please contact us<br />
		<br />
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
		    <td width="25%">
            <strong>Mr. Enoch F. Tualena</strong><br />
			(Sales Manager)<br />
           	bali@queenstandoor.com<br />
			+6281 23845598
            </td>
		    <td width="26%">
            <strong>Mrs. Nuriani</strong><br />
			(Sales Admin)<br />
           	bali2@queenstandoor.com<br />
			+6287 860041023
            </td>
		    <td width="25%">
            <strong>Mr. Mahfuj Shaikh</strong><br />
			(Financial Controller)<br />
           	ca1.bali@queenstandoor.com<br />
			+6281 23845598
            </td>
		    <td width="24%">
            <strong>Katon</strong><br />
			(Account Receivable)<br />
           	ar1.bali@queenstandoor.com<br />
			+6287 760054312</td>
		   </tr>
		</table><br />
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
		    <td width="34%">
		      <strong>Mr. Puneet Malhotra</strong><br />
		      (Business Development Advisor)<br />
		      puneet@queenstandoor.com<br />
		      +6281 558584669
		      </td>
		    <td width="66%">
            <strong>Mrs. Neeta Shamdasani Malhotra</strong><br />
			(Queen's Bali Owner)<br />
           	neeta@queenstandoor.com<br />
			+6281 558027770
            </td>
		   </tr>
		</table>
		<br />
		We look forward to providing you with quality products and a mutually beneficial relationship.<br />
		Thank you again for your business. We look forward a mutually advantageous and successful relationship.<br />
<br />
		Best Regards,<br />
		<br />
		<br />
		<strong>Queen's Bali </strong><br />
		<br />
			<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:12px;">
              <tr>
                <td align="center">
                <hr />
                Follow us @: queensbali: <img src="http://accounts.queenstandoor.com/images/icon-media.jpg" alt="" width="158" height="22" align="absmiddle"> |  http://bali.queenstandoor.com |  HOTLINE: +62 81 249 249 249 </td>
              </tr>
              <tr>
                <td align="center"><strong>QUEEN'S TANDOOR</strong>, Jl. Raya <strong>Seminyak</strong> No. 1/73A, Kuta |  T/F: (62-361) 732770/732771  |  e: bali@queenstandoor.com</td>
              </tr>
              <tr>
                <td align="center"><strong>QUEEN'S OF INDIA</strong>, Bali Dynasty Resort, Jl. Kartika Plaza, Tuban - <strong>Kuta</strong> | T/F: (62-361) 765988/761099  |  e: bali2@queenstandoor.com</td>
              </tr>
              <tr>
                <td align="center"><strong>QUEEN'S OF INDIA</strong>, Jl. Pratama No. 65B, Tanjung Benoa - <strong>Nusa Dua</strong> |  T/F: (62-361) 771344/774 648  |  e: bali3@queenstandoor.com</td>
              </tr>
              <tr>
                <td align="center"><strong>QUEEN'S OF INDIA</strong>, Jl. Suweta No. 1, Opp. Palace ( Puri Saren Ubud ), <strong>Ubud</strong> | T/F: ( 62-361 ) 977399/977400 | e: bali4@queenstandoor.com</td>
              </tr>
            </table>
            </td>
          </tr>
        </table>
        <table width="700" border="0" cellspacing="0" cellpadding="0" align="center" style="margin-top:10px; color:#FFF;">
          <tr>
            <td align="center">
            Copyright &copy; 2010 - 2012 <a href="http://bali.queenstandoor.com/" title="Bali Indian Restaurant" style="color:#FFF;">bali.queenstandoor.com</a>. All Rights Reserved</td>
          </tr>
        </table>
    	</td>
      </tr>
    </table>
	</td>
  </tr>
</table>